/* player.c
   Player window for jukebox file playing
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "player.h"
#include "jukebox.h"
#include "util.h"

typedef struct {
  guint16 effect;
  gint16 min;
  gint16 max;
} eax_adjust_props_t;

static gboolean playing = FALSE;
static GSList *exclusive_patches = NULL;
// Previous (since last callback) position in the file as percent
static guint prevpos;

/* option values */
static void patch_changed (gpointer combo,
			   gpointer data)
{
  GSList *tmplist;
  gboolean was_exclusive = FALSE;
  guint effect = GPOINTER_TO_UINT(data);
  guint patch = gtk_combo_box_get_active(GTK_COMBO_BOX(combo));

  // See if it is exclusive
  tmplist = exclusive_patches;
  while (tmplist != NULL) {
    if (combo == tmplist->data) {
      was_exclusive = TRUE;
    }
    tmplist = g_slist_next(tmplist);
  }
  // OK it was exclusive, reset all other exclusive patches...
  if (was_exclusive) {
    tmplist = exclusive_patches;
    // Reset all
    while (tmplist != NULL) {
      if (combo != tmplist->data) {
	// Reset it
	gtk_combo_box_set_active(GTK_COMBO_BOX(tmplist->data), 0);
      }
      tmplist = g_slist_next(tmplist);
    }
    // Then make sure it is set
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo), patch);
  }

  if (playing) {
    // Wire scalevalue to 0
    // g_print("Selecting patch %d for effect %04X\n", patch, effect);
    jukebox_adjust_eax(effect, patch, 0);
  }
}


/* options = name -> value -> name -> value -> ... */
/* This code for GTK+ 2.3 / 2.4 and up */
static GtkWidget *create_patch_menu(guint16 effect,
				    GSList *patches,
				    GCallback callback_function,
				    guint default_patch)
{
  GtkWidget *combo_box;
  GSList *tmplist = patches;
  guint mr_effect = (guint) effect;

  // Create a combo box
  combo_box = gtk_combo_box_new_text();
  // Then use gtk_combo_box_get_active() for numbering
  // Add rows to the combo box
  while (tmplist) {
    gtk_combo_box_append_text(GTK_COMBO_BOX(combo_box), tmplist->data);
    tmplist = tmplist->next;
  }
  gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box), default_patch);
  g_signal_connect(GTK_OBJECT(combo_box),
		   "changed",
		   G_CALLBACK(callback_function),
		   GUINT_TO_POINTER(mr_effect));
  gtk_widget_show(combo_box);

  // Free memory used by list
  tmplist = patches;
  while (tmplist != NULL) {
    g_free(tmplist->data);
    tmplist = tmplist->next;
  }
  g_slist_free(patches);
  return combo_box;
}

void prevbutton_click ( GtkButton *button,
			gpointer data )
{
  if (playing)
    jukebox_previous();
}

void nextbutton_click ( GtkButton *button,
			gpointer data )
{
  if (playing)
    jukebox_next(FALSE);
}

void stop_playing ( GtkButton *button,
		    gpointer data )
{
  if (playing) {
    cancel_jukebox_operation = TRUE;
    playing = FALSE;
  }
}

static void adjust_scalevalue (GtkAdjustment *adj,
			       gpointer data)
{
  eax_adjust_props_t *adjprops = (eax_adjust_props_t *) data;
  // gint16 scalevalue = adjprops->max+1 - ((gint16) adj->value);
  gint16 scalevalue = (gint16) adj->value;

  if (playing) {
    //g_print("Setting scalevalue for effect %04X to %d\n",
    //  adjprops->effect, scalevalue);
    jukebox_adjust_eax(adjprops->effect, 0, scalevalue);
  }
}

static void close_scalevalue (gpointer data,
			      GClosure *closure)
{
  eax_adjust_props_t *adjprops = (eax_adjust_props_t *) data;

  //g_print("Destroying adjprops for %04X\n", adjprops->effect);
  g_free(adjprops);
}

static GtkWidget *create_adjustment_control(guint16 effect,
					    gint16 min,
					    gint16 max,
					    gint16 current)
{
  GtkObject *adjustment;
  GtkWidget *vscale;
  eax_adjust_props_t *adjprops;

  adjprops = (eax_adjust_props_t *) g_malloc(sizeof(eax_adjust_props_t));
  adjprops->effect = effect;
  adjprops->min = min;
  adjprops->max = max;

  /*
  adjustment = gtk_adjustment_new ((gfloat) max+1 - current,
				   (gfloat) min,
				   (gfloat) max+1,
				   1.0, 1.0, 1.0);
  */

  adjustment = gtk_adjustment_new ((gfloat) current,
				   (gfloat) min,
				   (gfloat) max+1,
				   1.0, 1.0, 1.0);

  vscale = gtk_vscale_new (GTK_ADJUSTMENT (adjustment));
  g_signal_connect_data(GTK_OBJECT(adjustment),
			"value_changed",
			G_CALLBACK(adjust_scalevalue),
			adjprops,
			close_scalevalue,
			0);
  gtk_widget_set_usize (GTK_WIDGET (vscale), 20, 120);
  gtk_range_set_update_policy (GTK_RANGE (vscale),
			       GTK_UPDATE_CONTINUOUS);
  gtk_range_set_inverted (GTK_RANGE(vscale), TRUE);
  gtk_scale_set_draw_value (GTK_SCALE(vscale), TRUE);
  gtk_scale_set_digits (GTK_SCALE(vscale), 0);
  return vscale;
}


static void toggle_onoff (GtkCheckButton *toggle,
			  gpointer data)
{
  gboolean togglestatus;
  guint16 onoff;
  guint effect = GPOINTER_TO_UINT(data);

  togglestatus = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(toggle));
  if (togglestatus)
    onoff = 1;
  else
    onoff = 0;
  if (playing) {
    // Wire scalevalue to 0
    jukebox_adjust_eax(effect, onoff, 0);
  }
}


static GtkWidget *get_eax_controls(void)
{
  njb_eax_t *eax;
  GtkWidget *hbox;
  GtkWidget *group_box = NULL;
  GtkWidget *label;
  GtkWidget *checkbox;
  GtkWidget *vbox;
  guint16 lastgroup = 0xFFFF;
#define HORIZONTAL 0x00
#define VERTICAL   0x01
  u_int8_t last_direction = HORIZONTAL;

  // Clear old exclusive list...
  if (exclusive_patches != NULL) {
    g_slist_free(exclusive_patches);
    exclusive_patches = NULL;
  }

  // Stuff all EAX stuff into this frame
  hbox = gtk_hbox_new(FALSE, 5);

  jukebox_reset_get_eax();

  while ((eax = jukebox_get_eax()) != NULL) {
    // FIXME: reset all exclusive effects when an exclusive is selected.
    // Eax group has its own box
    if (lastgroup != eax->group) {
      if (group_box != NULL) {
	// pack previous frame
	gtk_box_pack_start(GTK_BOX(hbox), group_box, TRUE, TRUE, 0);
	gtk_widget_show(group_box);
      }
      // If this groups consists of scalevalues (we are guessing
      // that the first group member speaks for the rest), then create
      // a hbox, else (for patches etc) create a hbox.
      if (eax->type == NJB_EAX_SLIDER_CONTROL) {
	group_box = gtk_hbox_new(FALSE, 0);
	last_direction = HORIZONTAL;
      } else {
	group_box = gtk_vbox_new(FALSE, 0);
	last_direction = VERTICAL;
      }
    }
    // Then each effect with it's label is stuffed in a vbox
    vbox = gtk_vbox_new(FALSE, 0);
    // onoff values
    if (eax->type == NJB_EAX_FIXED_OPTION_CONTROL &&
	eax->min_value == 0x00 &&
	eax->max_value == 0x01) {
      guint effect = (guint) eax->number;

      checkbox = gtk_check_button_new_with_label(eax->name);
      if (eax->current_value != 0x00) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), TRUE);
      }
      g_signal_connect(GTK_OBJECT(checkbox),
		       "toggled",
		       G_CALLBACK(toggle_onoff),
		       GUINT_TO_POINTER(effect));
      gtk_box_pack_start(GTK_BOX(vbox), checkbox, TRUE, TRUE, 0);
    } else {
      // All non-onoff effects need a label too.
      label = gtk_label_new(eax->name);
      gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.0f);
      gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
      // Patch value combobox
      if (eax->type == NJB_EAX_FIXED_OPTION_CONTROL) {
	guint i;
	GSList *names = NULL;
	GtkWidget *patches;

	for(i = 0; i < eax->max_value - eax->min_value + 1; i++) {
	  names = g_slist_append(names, g_strdup(eax->option_names[i]));
	}
	patches = create_patch_menu(eax->number,
				    names,
				    G_CALLBACK(patch_changed),
				    eax->current_value);
	gtk_box_pack_start(GTK_BOX(vbox), patches, TRUE, TRUE, 0);
	if (eax->exclusive) {
	  // Add to list of exclusive stuff
	  exclusive_patches = g_slist_append(exclusive_patches, (gpointer) patches);
	}
      }
      // Scale value slider
      if (eax->type == NJB_EAX_SLIDER_CONTROL) {
	GtkWidget *slider;

	// g_print("Scalevalue %s, min %d, max %d, current %d\n",
	//	eax->name, eax->min_scalevalue, eax->max_scalevalue,
	//	eax->current_scalevalue);
	slider = create_adjustment_control(eax->number,
					   eax->min_value,
					   eax->max_value,
					   eax->current_value);
	gtk_box_pack_start(GTK_BOX(vbox), slider, TRUE, TRUE, 0);
	gtk_widget_show(slider);
      }
    }
    gtk_box_pack_start(GTK_BOX(group_box), vbox, TRUE, TRUE, 0);
    gtk_widget_show(vbox);
    lastgroup = eax->group;
    jukebox_destroy_eax(eax);
  }

  // Pack the last group box
  gtk_box_pack_start(GTK_BOX(hbox), group_box, TRUE, TRUE, 0);
  gtk_widget_show(group_box);

  return hbox;
}

static void adjust_position (GtkAdjustment *adj,
			     gpointer data)
{
  // eax_adjust_props_t *adjprops = (eax_adjust_props_t *) data;
  // gint16 scalevalue = adjprops->max+1 - ((gint16) adj->value);
  gint16 pos = (gint16) adj->value;
  // Calculate the byte position offset
  guint delta = 0;
  guint skipto = 0;

  // If we just passed a track boundary there is no delta,
  // else we find out how much the position has changed.
  if ((prevpos == 100 || prevpos == 99) && (pos == 0  || pos == 1)) {
    delta = 0;
  } else if (pos >= prevpos) {
    delta = pos - prevpos;
  } else if (pos < prevpos) {
    delta = prevpos - pos;
  }

  // If someone has dragged the progress bar, delta will be more
  // than 2 percent (let's say).
  if (delta > 2) {
    skipto = pos;
  }

  // If we're skipping backward or forward, execute this.
  if (playing && skipto > 0) {
    metadata_t *meta = jukebox_get_current_playing_metadata();
    guint seconds = mmss_to_seconds(meta->length);
    guint mspos = (seconds * 1000 * skipto) / 100;
    g_print("Skipping to millisecond offset %d in track\n", mspos);
    // Call NJB
    jukebox_skip_songposition(mspos);
  }
  prevpos = pos;
}


/* Creates a complicated player window */
void create_player_window(GList *metalist)
{
  static play_thread_arg_t play_thread_args;
  GtkWidget *label1, *label2, *label3, *dialog, *button, *separator;
  GtkWidget *hscale, *eax_controls;
  GtkWidget *hbox, *hbox2, *vbox;
  GtkAdjustment *adj;

  // Playback on MTP devices not supported. (Yet.)
  if (jukebox_is_mtp()) {
    create_error_dialog(_("Playback is not supported on MTP devices"));
    return;
  }
  play_thread_args.metalist = metalist;
  dialog = gtk_dialog_new();
  gtk_window_set_title(GTK_WINDOW (dialog), (_("Playing tracks on the jukebox")));
  gtk_box_set_spacing(GTK_BOX (GTK_DIALOG (dialog)->vbox), 5);
  gtk_box_set_homogeneous(GTK_BOX (GTK_DIALOG (dialog)->action_area), TRUE);
  gtk_window_set_position(GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  label1 = gtk_label_new(_("Playing:"));
  label2 = gtk_label_new("");
  /* value, lower, upper, step_increment, page_increment, page_size */
  /* Note that the page_size value only makes a difference for
   * scrollbar widgets, and the highest value you'll get is actually
   * (upper - page_size). */
  adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 101.0, 0.1, 1.0, 1.0));
  hscale = gtk_hscale_new(adj);
  // To make it possible to skip into the song
  // gtk_widget_set_sensitive(hscale, FALSE);
  g_signal_connect_data(GTK_OBJECT(adj),
			"value_changed",
			G_CALLBACK(adjust_position),
			NULL, // data
			NULL, // destructor
			0);
  gtk_widget_set_usize (GTK_WIDGET (hscale), 200, 30);
  gtk_range_set_update_policy (GTK_RANGE (hscale),
			       GTK_UPDATE_CONTINUOUS);
  gtk_scale_set_digits (GTK_SCALE(hscale), 1);
  gtk_scale_set_value_pos (GTK_SCALE(hscale), GTK_POS_TOP);
  gtk_scale_set_draw_value (GTK_SCALE(hscale), FALSE);
  gtk_widget_show (hscale);
  button =  gtk_button_new_from_stock(GTK_STOCK_MEDIA_STOP);
  gtk_widget_set_can_default(button, TRUE);
  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area), button,
		     FALSE, FALSE, 0);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(stop_playing),
			  NULL,
			  0);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "delete_event",
			  G_CALLBACK(stop_playing),
			  NULL, // GTK_OBJECT(dialog) ???
			  0);
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox, TRUE, TRUE, 0);
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
  separator = gtk_hseparator_new ();
  gtk_box_pack_start(GTK_BOX(vbox), separator, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), label1, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), label2, TRUE, TRUE, 0);

  /* Manouver panel */
  hbox2 = gtk_hbox_new(FALSE, 0);

  button =  gtk_button_new_from_stock(GTK_STOCK_MEDIA_PREVIOUS);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(prevbutton_click),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(hbox2), button, FALSE, TRUE, 0);
  gtk_widget_set_can_default(button, TRUE);
  label3 = gtk_label_new("00:00:00");
  gtk_box_pack_start(GTK_BOX(hbox2), label3, TRUE, TRUE, 0);
  button =  gtk_button_new_from_stock(GTK_STOCK_MEDIA_NEXT);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(nextbutton_click),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(hbox2), button, FALSE, TRUE, 0);
  gtk_widget_set_can_default(button, TRUE);
  gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, TRUE, 0);

  separator = gtk_hseparator_new ();
  gtk_box_pack_start(GTK_BOX(vbox), separator, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hscale, TRUE, TRUE, 0);
  eax_controls = get_eax_controls();
  gtk_box_pack_start(GTK_BOX(hbox), eax_controls, TRUE, TRUE, 0);

  /* Get all settings from jukebox */
  gtk_widget_show_all(dialog);

  play_thread_args.songlabel = label2;
  play_thread_args.timelabel = label3;
  play_thread_args.adj = adj;
  play_thread_args.dialog = dialog;

  cancel_jukebox_operation = FALSE;
  g_thread_create(play_thread,(gpointer) &play_thread_args, FALSE, NULL);
  playing = TRUE;
}

/* Plays a certain playlist with ID plid */
void play_playlist(guint plid)
{
  GList *playlist;

  /* Get the playlist from the jukebox */
  playlist = jukebox_get_playlist_for_play(plid);
  if (playlist != NULL)
    create_player_window(playlist);
}
