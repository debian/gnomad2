/* filesystem.c
   Interface to the posix (well...) file system
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "metadata.h"
#include "filesystem.h"
#include "filenaming.h"
#include "prefs.h"
#include "util.h"
#include "mp3file.h"
#include "id3read.h"
#include "wmaread.h"
#include "riffile.h"
#include "xfer.h"
#ifdef HAVE_TAGLIB
#include "tagfile.h"
#endif
#include <glib.h>
#include <glib/gstdio.h>

/* Extra includes used by this file only */

#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef G_OS_WIN32
#include <pwd.h>
#endif

/* Globally imported variables */
extern GtkWidget *main_window;

/* Local variables */
static gint preffd;

/*
 * Types and function to send drawing down to idle thread
 */

typedef struct {
  GtkWidget *label;
  gchar *text;
} draw_label_args_t;

typedef struct {
  GtkWidget *progress;
  gfloat fraction;
} fraction_set_args_t;

typedef struct {
  listtype_t listtype;
  GList *metalist;
} metadata_set_args_t;

static gboolean draw_label(gpointer p) {
  draw_label_args_t *args = (draw_label_args_t *) p;

  if (args != NULL) {
    if (args->label != NULL && args->text != NULL) {
      gtk_label_set_text(GTK_LABEL(args->label), args->text);
    }
    if (args->text != NULL)
      g_free(args->text);
    g_free(args);
  }
  return FALSE;
}

static gboolean update_fraction(gpointer p) {
  fraction_set_args_t *args = (fraction_set_args_t *) p;

  if (args != NULL) {
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(args->progress), args->fraction);
    g_free(args);
  }
  return FALSE;
}

static gboolean update_metadata(gpointer p) {
  metadata_set_args_t *metarg = (metadata_set_args_t *) p;
  GList *tmp;

  if (metarg != NULL) {
    recreate_list_store(metarg->listtype);
    tmp = metarg->metalist;
    while (tmp) {
      metadata_t *meta;

      meta = (metadata_t *) tmp->data;
      // g_print("Adding metadata for \"%s\" to model\n", meta->path);
      add_metadata_to_model(meta, metarg->listtype);
      tmp = tmp->next;
      destroy_metadata_t(meta);
    }
    g_list_free(metarg->metalist);
    view_and_sort_list_store(metarg->listtype);
    g_free(metarg);
  }
  return FALSE;
}

/* Filecopy routine loosely based on
 * http://www.snippets.org/snippets/portable/WB_FCOPY+C.php3 */
gboolean copy_file(gchar *source, gchar *dest)
{
  gint f1, f2;

  f1 = (gint) g_open(source, READONLY_FLAGS, 0);
  if (f1 < 0)
    return FALSE;

#if !GLIB_CHECK_VERSION(2,8,0)
  f2 = (gint) creat(dest, (mode_t) CREATE_FILEMODE);
#else
  f2 = (gint) g_creat(dest, (mode_t) CREATE_FILEMODE);
#endif
  if (f2 >= 0) {
    guint bufsiz;
    gboolean retval = FALSE;
    gchar *buffer;

    for (bufsiz = 0x8000; bufsiz >= 128; bufsiz >>= 1) {
      buffer = (gchar *) g_malloc(bufsiz);
    }

    if (buffer == NULL) {
      return FALSE;
    }

    while (1) {
      register gint n;

      n = read(f1,buffer,bufsiz);
      if (n == -1) {
	break;
      }
      if (n == 0) {
	retval = TRUE;
	break;
      }
      if (n != write(f2,buffer,n)) {
	break;
      }
    }

    g_free(buffer);
    close(f1);
    close(f2);
    if (!retval) {
      remove(dest); // Delete partial file
    }
    return retval;
  }
  close(f1);
  return FALSE;
}

/**
 * Get the 64bit file size for a file. Return 0 if failed.
 * Does not currently expect path to be UTF-8 but assumes
 * local filesystem encoding.
 */
guint64 get_64bit_file_size(const gchar * const path)
{
  struct stat sb;

  // Get the size that we actually have now...
  if ( stat(path, &sb) == -1 ) {
    g_print("Error getting file size for \"%s\"!!\n", path);
    return 0;
  }
  // TODO: fix this to actually find the 64bit size!
  return (guint64) sb.st_size;
}

/* Unicode conversion wrappers that enables
 * forcing the local charset to something disregarding
 * the locale setting and such stuff. */
gchar *filename_toutf8(const gchar * const instr)
{
#ifndef G_OS_WIN32
  gchar *tmp;

  if (get_prefs_force_8859()) {
    tmp = g_convert(instr,-1,"UTF-8","ISO-8859-1",NULL,NULL,NULL);
  } else {
    tmp = g_filename_to_utf8(instr,-1,NULL,NULL,NULL);
    /* If this conversion fails, fall back to ISO 8859-1 */
    if (tmp == NULL) {
      /* Something is wrong with the locales */
      gchar *lang = getenv("LANG");
      g_print("Seems like you're using a filesystem which still contains ISO 8859-1 characters\n");
      g_print("though your locale is \"%s\". The right thing to do is to convert\n", lang);
      g_print("all file- and directorynames in your filesystem to UTF-8 format.\n");
      g_print("Otherwise, consider checking \"force to 8859-1\" option for a workaround.\n\n");
      tmp = g_convert(instr,-1,"UTF-8","ISO-8859-1",NULL,NULL,NULL);
    }
  }
  return tmp;
#else
  // Windows will always use UTF-8 everywhere.
  return g_strdup(instr);
#endif
}

gchar *filename_fromutf8(const gchar * const instr)
{
#ifndef G_OS_WIN32
  gchar *tmp;

  if (get_prefs_force_8859()) {
    /* FIXME utf8 conversion
	 * when the system fails to convert the fname, ask the user
	 * but until the dialog box is created, don't crash, use
	 * "?" for fname characters that can't be converted instead of:
	 * tmp = g_convert(instr,-1,"ISO-8859-1","UTF-8",NULL,NULL,NULL);
	 */
	tmp = g_convert_with_fallback(instr,-1,"ISO-8859-1","UTF-8","?",NULL,NULL,NULL);
  } else {
    tmp = g_filename_from_utf8(instr,-1,NULL,NULL,NULL);
  }
  return tmp;
#else
  // Windows will always use UTF-8 everywhere.
  return g_strdup(instr);
#endif
}

gboolean is_directory(gchar *dir) {
  struct stat st;
  gchar *tmp;

  tmp = filename_fromutf8(dir);
  stat(tmp, &st);
  g_free(tmp);
  return S_ISDIR(st.st_mode);
}

static GList *get_directory_listing(gchar* base_path, gboolean anyfile)
{
  GList *filelist;
  DIR * dirp;
  struct dirent * dir_entry;
  gchar *tmppath;

  /* Returns a listing of applicable files */
  /* returned GSList contains file / size pairs. */
  /* Directories: no full path, just dirname; size 0 */
  filelist = NULL;
  tmppath = filename_fromutf8(base_path);
  if (gnomad_debug != 0) {
    g_print("Getting directory listing for: %s\n", base_path);
  }
  dirp = opendir(tmppath);
  if (!dirp){
    if (gnomad_debug != 0) {
      g_print("Unable to open directory %s.\n", tmppath);
    }
    return NULL;
  }
  while ((dir_entry = readdir(dirp)) != NULL)
    {
      struct stat entry_status;
      gchar path_buffer[512];
      gchar *filename;
      int size;
      int is_dir;
      int is_file;

      strncpy(path_buffer, tmppath, sizeof(path_buffer)-1);
      path_buffer[sizeof(path_buffer)-1] = '\0';
      strncat(path_buffer, G_DIR_SEPARATOR_S, sizeof(path_buffer)-1-strlen(path_buffer));
      strncat(path_buffer, dir_entry->d_name, sizeof(path_buffer)-1-strlen(path_buffer));
      if (gnomad_debug != 0) {
	g_print("Examining dir path: %s\n", path_buffer);
      }
      if (stat(path_buffer, &entry_status)) {
	if (gnomad_debug != 0) {
	  g_print("Failed to read file status for %s.\n", path_buffer);
	}
        continue;
      }
      size = (int) entry_status.st_size;
      is_dir = S_ISDIR(entry_status.st_mode);
      is_file = S_ISREG(entry_status.st_mode);
      if (is_dir) {
	/* We don't want this "dir" */
	if (!strcmp(dir_entry->d_name,".")) {
	  continue;
	}
	/* If the prefs are selected not to display the hidden
	 * directories, we hide them. */
#ifndef G_OS_WIN32
	if (strlen(dir_entry->d_name) >= 2) {
	  if (dir_entry->d_name[0] == '.' &&
	      dir_entry->d_name[1] != '.' &&
	      !get_prefs_display_dotdirs())
	    continue;
	}
#endif
      } else if (is_file) {
	char tmp_codec[5];
	char *tmp;

	if (!anyfile) {
	  /* Must be more atleast one byte */
	  if (size == 0)
	    continue;
	  /* Must have sane filename */
	  if (strlen(dir_entry->d_name) < 4)
	    continue;

	  strcpy(tmp_codec, dir_entry->d_name+strlen(dir_entry->d_name)-4);
	  tmp = g_utf8_strdown(tmp_codec, -1);
	  /* Only display valid files */
          if (g_ascii_strcasecmp(tmp+1,"mp3") &&
              g_ascii_strcasecmp(tmp+1,"wav") &&
              g_ascii_strcasecmp(tmp+1,"avi") &&
              g_ascii_strcasecmp(tmp+1,"wmv") &&
              g_ascii_strcasecmp(tmp+1,"wma")
#ifdef HAVE_LIBMTP
	      // Additional music codecs supported by libmtp
	      // TODO: base this on the list of filetypes actually
	      // supported by a particular device, if one is connected.
	      &&
	      g_ascii_strcasecmp(tmp+1,"ogg") &&
	      g_ascii_strcasecmp(tmp,"flac") &&
	      g_ascii_strcasecmp(tmp+1,"mp2") &&
	      g_ascii_strcasecmp(tmp+1,"m4a") &&
	      g_ascii_strcasecmp(tmp+1,"aac") &&
	      g_ascii_strcasecmp(tmp+1,"mp4")
#endif
	      ) {
	    g_free(tmp);
	    continue;
	  }
	  g_free(tmp);
	}
      }
      /* only directories and valid files come here, the
       * list is NOT in UTF-8, that conversion will be
       * done by the caller of this routine. */
      if (is_dir) {
	/* For directories, set size to 0 */
	size = 0;
      }
#ifndef G_OS_WIN32
      filename = g_strdup(path_buffer);
      // g_printf("strdup(%s)=%s\n", path_buffer, filename);
#else
      // On Windows we need to convert into UTF-8 since
      // this does not use the stdio wrappers properly.
      filename = g_locale_to_utf8(path_buffer,-1,NULL,NULL,NULL);
#endif
      if (gnomad_debug != 0) {
	g_print("Adding dir entry: %s, size %d\n", filename, size);
      }
      filelist = g_list_append(filelist, filename);
      filelist = g_list_append(filelist, GUINT_TO_POINTER(size));
    }
  closedir(dirp);
  g_free(tmppath);
  return filelist;
}

static GList *get_metadata_for_dirlist(GList *inlist,
				       listtype_t listtype,
				       dirfill_thread_arg_t *args)
{
  GList *metalist = NULL;
  GList *filelist;
  guint nofiles = 0;  /* initialize to shut up GCC */
  guint currentfile;
  gboolean use_dialog = FALSE;
  gboolean cancelled = FALSE;

  /* Count the number of files */
  if (args != NULL) {
    use_dialog = TRUE;
    filelist = inlist;
    nofiles = 0;
    while (filelist) {
      nofiles ++;
      filelist = filelist->next;
      filelist = filelist->next;
    }
    currentfile = 0;
  }

  filelist = inlist;
  while ((filelist != NULL) && !cancelled) {
    metadata_t *meta;
    gchar *path;
    gchar *tmp;
    gchar fileext[5];

    meta = new_metadata_t();
    /* Full path -> ID column */
    path = filelist->data;
    meta->path = filename_toutf8(path);
    if (meta->path == NULL) {
      g_print("%s created a NULL value in conversion to UTF-8!\n", path);
    }
    /* If filename is ISO-8859-1 convert it by force! */
    filelist = filelist->next;
    meta->size = GPOINTER_TO_UINT(filelist->data);
    filelist = filelist->next;
    tmp = g_path_get_basename(path);
    if (tmp == NULL) {
      meta->filename = NULL;
      g_print("Basename was NULL\n");
    } else {
      meta->filename = filename_toutf8(tmp);
      g_free(tmp);
    }
    if (use_dialog) {
      draw_label_args_t *da;

      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->label;
      da->text = g_strdup_printf("File %d of %d", currentfile, nofiles);
      g_idle_add(draw_label, da);

      currentfile ++;
      if (gnomad_debug != 0) {
	g_print("Getting metadata for: %s (%d/%d)\n", meta->filename, currentfile, nofiles);
      }
    }
    /* Find codec/file extension first */
    fileext[0] = 0;
    if (strlen(meta->path) > 5) {
      gchar *tmp = meta->path;

      // Accept 3 or 4 character extensions
      if (tmp[strlen(tmp)-4] == '.') {
	strcpy(fileext, tmp+strlen(tmp)-3);
      } else if (tmp[strlen(tmp)-5] == '.') {
	strcpy(fileext, tmp+strlen(tmp)-4);
      }
      // Uppercase it.
      g_strup(fileext);
      meta->codec = g_strdup(fileext);
    }
    if (listtype == HDDATA_LIST) {
      /*
       * For data files, provide basic data nothing additional is needed.
       * The file extension will be used to map the apropriate transfer
       * type later.
       */
    } else {
      /* For music files, fill in row by using libid3tag and friends */
      if (meta->size != 0) {
	if (!strcmp(meta->codec,"MP3")) {
	  /* Gets the ID3 tag if any */
	  if (gnomad_debug != 0) {
	    g_print("Getting ID3 info for: %s\n", meta->filename);
	  }
#if defined(HAVE_TAGLIB) && defined(TAGLIB_ONLY)
	  get_tag_for_file(meta);
#else
	  get_tag_for_mp3file(meta);
	  /* FIXME: use taglib to do this, if it is available. */
	  /* Scans the mp3 frames to calculate play length */
	  if ((meta->length == NULL) && (meta->path != NULL)) {
	    // g_print("Calling length_from_file()...\n");
	    gchar *tmppath = filename_fromutf8(meta->path);
	    meta->length = length_from_file(tmppath, meta->size);
	    g_free(tmppath);
	  }
#endif
	}
	if (!strcmp(meta->codec,"WAV")) {
	  /* FIXME: learn how to read metadata from WAV files */
	  get_tag_for_wavfile(meta);
	  if (meta->length == NULL) {
	    meta->length = strdup("0:01");
	  }
	}
	if (!strcmp(meta->codec,"WMA")) {
	  get_tag_for_wmafile(meta);
	}
#ifdef HAVE_LIBMTP
	if (!strcmp(meta->codec,"OGG")) {
#ifdef HAVE_TAGLIB
           get_tag_for_file(meta);
#endif
	}
	if (!strcmp(meta->codec,"FLAC")) {
#ifdef HAVE_TAGLIB
           get_tag_for_file(meta);
#endif
	}
	if (!strcmp(meta->codec,"AVI")) {
           get_tag_for_avifile(meta);
        }
	// Implement getting track metadata for the other libmtp
	// supported filetypes here...
	if (!strcmp(meta->codec,"MP2")) {
	}
	if (!strcmp(meta->codec,"M4A")) {
	}
	if (!strcmp(meta->codec,"AAC")) {
	}
	if (!strcmp(meta->codec,"MP4")) {
	}
#endif
	/*
	 * Gather additional information from the path and file name
	 * if this preference is set.
	 */
	if (get_prefs_detect_metadata()) {
	  info_from_path(meta, TRUE);
	} else {
	  info_from_path(meta, FALSE);
	}

	/* At last pad any empty fields to be <unknown>
	 * (the way playcenter does it) */

	if (meta->artist == NULL)
	  meta->artist = g_strdup("<Unknown>");
	if (meta->title == NULL)
	  meta->title = g_strdup("<Unknown>");
	if (meta->album == NULL)
	  meta->album = g_strdup("<Unknown>");
	if (meta->codec == NULL)
	  meta->codec = g_strdup("<Unknown>");
	if (meta->genre == NULL)
	  meta->genre = g_strdup("<Unknown>");
	if (meta->length == NULL)
	  meta->length = g_strdup("<Unknown>");
	if (meta->filename == NULL) {
	  tmp = g_path_get_basename(path);
	  meta->filename = filename_toutf8(tmp);
	  g_free(tmp);
	}
      } else {
	if (meta->path == NULL) {
	  meta->artist = g_strdup("<Unknown>");
	} else {
	  /* For directories: copy dirname to Artist field */
	  gchar **tmpv;
	  gint i;

	  tmpv = g_strsplit(meta->path, G_DIR_SEPARATOR_S, 0);
	  /* Find last component */
	  for (i = 0; tmpv[i] != NULL; i++) {};
	  if (i >= 1)
	    meta->artist = g_strdup(tmpv[i-1]);
	  g_strfreev(tmpv);
	}
      }
    }
    /* Add this to the metadata list */
    metalist = g_list_append(metalist, meta);
    /* Update progress bar */
    if (use_dialog) {
      fraction_set_args_t *fsa;

      gfloat fraction = ((gdouble) currentfile / (gdouble) nofiles);

      fsa = (fraction_set_args_t *) g_malloc(sizeof(fraction_set_args_t));
      fsa->progress = args->progress;
      fsa->fraction = fraction;
      g_idle_add(update_fraction, fsa);
      cancelled = args->cancelled;
    }
  }
  return metalist;
}


/**
 * @param args if this and the following parameter is null the call is assumed to be
 *        non-threaded, else it is updated and handled as if called from a thread.
 *        Else it is assumed to be called from a single-threaded context and will not
 *        display update information.
 */
GList *get_metadata_dir(listtype_t listtype,
			gchar *path,
			dirfill_thread_arg_t *args)
{
  GList *metalist;
  GList *filelist;
  GList *tmp;

  /* If it is for the data view, get for ALL files */
  if (args != NULL) {
    draw_label_args_t *da;

    da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
    da->label = args->label;
    da->text = g_strdup(_("Getting file list from filesystem"));
    g_idle_add(draw_label, da);
  }
  filelist = get_directory_listing(path, (listtype == HDDATA_LIST));
  /* This is the heavy part */
  metalist = get_metadata_for_dirlist(filelist, listtype, args);
  /* Free the filelist, not used any more */
  tmp = filelist;
  while (tmp) {
    g_free(tmp->data); /* Free all paths */
    tmp = tmp->next;
    tmp = tmp->next;
  }
  g_list_free(filelist);
  return metalist;
}

static gboolean call_destroy_args(gpointer data)
{
  dirfill_thread_arg_t *args = data;
  GtkWidget *w = (GtkWidget *) args->dialog;

  if (args == NULL)
    return FALSE;

  if (args->dialog != NULL)
    gtk_widget_destroy(GTK_WIDGET(args->dialog));

  if (args->path != NULL)
    g_free(args->path);
  g_free(args);
  return FALSE;
}

static gpointer dirfill_thread(gpointer thread_args)
{
  dirfill_thread_arg_t *args = (dirfill_thread_arg_t *) thread_args;
  GList *metalist;
  draw_label_args_t *da;
  metadata_set_args_t *metarg;

  /* This is time consuming */
  metalist = get_metadata_dir(args->listtype, args->path, args);

  da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
  da->label = args->label;
  da->text = g_strdup(_("Adding metadata to view"));
  g_idle_add(draw_label, da);

  metarg = (metadata_set_args_t *) g_malloc(sizeof(metadata_set_args_t));
  metarg->listtype = args->listtype;
  metarg->metalist = metalist;
  g_idle_add(update_metadata,metarg);

  g_idle_add(call_destroy_args, args);
  return NULL;
}

/**
 * This callback function will cancel the ongoing
 * fill-in of metadata for a certain thread.
 */
void swapped_cancel_fillin_callback(gpointer *data)
{
  dirfill_thread_arg_t *args = (dirfill_thread_arg_t *) data;

  g_print("Cancelled scan of %s!\n", args->path);
  args->cancelled = TRUE;
}

/**
 * This will fire up a separate thread which retrieves
 * the metadata for a certain host-side directory. Both
 * tracks and common data files.
 * @param listtype the list pane to retrieve. Must be one
 *        of the harddisk-refering panes.
 * @param path the path to retrieve metadata from.
 */
void fill_in_dir(listtype_t listtype, gchar *path)
{
  dirfill_thread_arg_t *dirfill_thread_args;
  GtkWidget *label1;
  GtkWidget *dialog, *scanlabel, *metadata_progress_bar, *separator;

  dirfill_thread_args = (dirfill_thread_arg_t *) g_malloc(sizeof(dirfill_thread_arg_t));

  // Create a dialog...
  dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
				   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_INFO,
				   GTK_BUTTONS_CLOSE,
				   (listtype == HDDATA_LIST) ? _("Getting file metadata") : _("Getting track metadata"));
  gtk_window_set_title (GTK_WINDOW(dialog), (listtype == HDDATA_LIST) ? _("Getting file metadata") : _("Getting track metadata"));
  gtk_box_set_spacing(GTK_BOX(GTK_DIALOG(dialog)->vbox), 5);
  gtk_box_set_homogeneous(GTK_BOX(GTK_DIALOG(dialog)->action_area), TRUE);
  g_signal_connect_swapped(GTK_OBJECT(dialog),
		   "delete_event",
		   G_CALLBACK(swapped_cancel_fillin_callback),
		   (gpointer) dirfill_thread_args);
  g_signal_connect_swapped(GTK_OBJECT(dialog),
		   "response",
		   G_CALLBACK(swapped_cancel_fillin_callback),
		   (gpointer) dirfill_thread_args);
  label1 = gtk_label_new(path);
  scanlabel = gtk_label_new("");
  metadata_progress_bar = gtk_progress_bar_new();
  separator = gtk_hseparator_new ();
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label1, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), scanlabel, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), separator, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), metadata_progress_bar, TRUE, TRUE, 0);
  gtk_progress_bar_update(GTK_PROGRESS_BAR(metadata_progress_bar), (gfloat) 0);
  gtk_widget_show_all(dialog);

  dirfill_thread_args->dialog = dialog;
  dirfill_thread_args->label = scanlabel;
  dirfill_thread_args->progress = metadata_progress_bar;
  dirfill_thread_args->listtype = listtype;
  dirfill_thread_args->path = g_strdup(path);
  dirfill_thread_args->cancelled = FALSE;

  g_thread_create(dirfill_thread,(gpointer) dirfill_thread_args, FALSE, NULL);
}

gchar *get_current_dir(void)
{
  gchar *tmp, *path;

  /* Retrieve the current working directory somehow */
  tmp = g_get_current_dir();
  path = filename_toutf8(tmp);
  g_free(tmp);
  return path;
}

void change_directory(gchar *dir)
{
  gchar *tmp;

  tmp = filename_fromutf8(dir);
#if GLIB_CHECK_VERSION(2,8,0)
  g_chdir(tmp);
#else
  chdir(tmp);
#endif
  g_free(tmp);
}

gint remove_dir_contents(gchar *dir)
{
  GDir * current_dir;
  const gchar * file = NULL;
  gchar * current_file = NULL;
  gint c = 0;

  if ((current_dir = g_dir_open(dir,0,NULL)) == NULL) return -1;

  while ((file = g_dir_read_name(current_dir)) != NULL)
    {
      current_file = g_build_filename (dir, file, NULL);

      if (g_file_test(current_file, G_FILE_TEST_IS_DIR))
	{
	  c += remove_dir_contents(current_file);
	}

      c += g_remove(current_file);
      g_free(current_file);
    }

  g_dir_close(current_dir);
  return c;
}

gint delete_files(GList *filelist)
{
  GList *tmplist = g_list_first(filelist);
  gchar *tmp;
  guint c = 0;

  while (tmplist != NULL) {
    metadata_t *meta = (metadata_t *) tmplist->data;
    /* g_print("Deleting: %s\n", meta->path); */
    tmp = filename_fromutf8(meta->path);

    /*
     * Skip past current and parent directory if selected.
     * Be sure to avoid also /foo/bar/.. and /foo/bar/.
     */
    if (
	(strlen(tmp) >= 1 && !strcmp(tmp+strlen(tmp)-1, "."))
	||
	(strlen(tmp) >= 2 && !strcmp(tmp+strlen(tmp)-2, ".."))
	) {
      g_print("Skipping deletion of: %s\n", meta->path);
      g_free(tmp);
      tmplist = tmplist->next;
      continue;
    }

    if (g_file_test(tmp, G_FILE_TEST_IS_DIR)) {
      remove_dir_contents(tmp);
    }

    c += g_remove(tmp);

    g_free(tmp);
    tmplist = tmplist->next;
  }
  return c;
}

/* Copy a file and remove the tag */
gboolean clone_and_strip_tag(const gchar * const source,
			     const gchar * const dest,
			     const gchar * const codec)
{
  gchar *tmpsource, *tmpdest;

  tmpsource = filename_fromutf8(source);
  tmpdest = filename_fromutf8(dest);
  if (copy_file(tmpsource, tmpdest)) {
    /* Next strip the tag */
    /* g_print("Stripping tag from %s\n", dest); */
    if (!strcmp(codec, "MP3")) {
#if defined(HAVE_TAGLIB) && defined(TAGLIB_ONLY)
      remove_tag_from_file(tmpdest);
#else
      remove_tag_from_mp3file(tmpdest);
#endif
    }
#ifdef HAVE_TAGLIB
    else if (!strcmp(codec, "OGG") || !strcmp(codec, "FLAC")) {
      remove_tag_from_file(tmpdest);
    }
#endif
    g_free(tmpsource);
    g_free(tmpdest);
    return TRUE;
  }
  g_free(tmpsource);
  g_free(tmpdest);
  return FALSE;
}

/* Get a file descriptor for the preference file */
gint get_prefsfile_fd(gboolean write)
{
  static gchar filename[] = "." PACKAGE "rc";
  static gchar *path = NULL;

  if (path == NULL) {
    gchar *tmp;

    tmp = g_utf8_strdown(filename, -1);
    path = g_strdup(g_get_home_dir());
    path = stringcat(path, G_DIR_SEPARATOR_S);
    path = stringcat(path, tmp);
    g_free(tmp);
  }

  if (write) {
    preffd = (gint) g_open(path, WRITEONLY_FLAGS, (mode_t) CREATE_FILEMODE);
    if (preffd < 0)
#if !GLIB_CHECK_VERSION(2,8,0)
      preffd = (gint) creat(path, (mode_t) CREATE_FILEMODE);
#else
      preffd = (gint) g_creat(path, (mode_t) CREATE_FILEMODE);
#endif
  } else {
    preffd = (gint) g_open(path, READONLY_FLAGS, 0);
  }

  if (preffd < 0 && write) {
    create_error_dialog(_("Could not write preference file"));
  }
  return preffd;
}

/* Close a preference file */
void close_prefsfile(void)
{
  close(preffd);
  preffd = 0;
}

/* Close a preference file */
void rewrite_prefsfile(gchar *prefs)
{
  get_prefsfile_fd(TRUE);
  write(preffd, prefs, strlen(prefs));
  close_prefsfile();
}

/* Function from gftp lib/misc.c */
char *expand_path (char *src)
{
// The following is very POSIX
#ifndef G_OS_WIN32
  char *str, *pos, *endpos, *prevpos, *newstr, *tempstr, tempchar;
  struct passwd *pw;

  pw = NULL;

  str = filename_fromutf8(src);

  if (*str == '~') {
    if (*(str + 1) == G_DIR_SEPARATOR || (pos = strchr (str, G_DIR_SEPARATOR)) == NULL) {
      pw = getpwuid (geteuid ());
    } else {
      *pos = '\0';
      pw = getpwnam (str + 1);
      *pos = G_DIR_SEPARATOR;
    }
  }

  endpos = str;
  newstr = NULL;
  while ((pos = strchr (endpos, G_DIR_SEPARATOR)) != NULL)
    {
      pos++;
      while (*pos == G_DIR_SEPARATOR)
	pos++;
      if ((endpos = strchr (pos, G_DIR_SEPARATOR)) == NULL)
	endpos = strchr (pos, '\0');
      tempchar = *endpos;
      *endpos = '\0';
      if (strcmp (pos, "..") == 0)
	{
	  *(pos - 1) = '\0';
	  if (newstr != NULL && (prevpos = strrchr (newstr, G_DIR_SEPARATOR)) != NULL)
	    *prevpos = '\0';
	}
      else if (strcmp (pos, ".") != 0)
	{
	  if (newstr == NULL)
	    {
	      newstr = g_malloc (strlen (pos - 1) + 1);
	      strcpy (newstr, pos - 1);
	    }
	  else
	    {
	      tempstr = g_strconcat (newstr, pos - 1, NULL);
	      g_free (newstr);
	      newstr = tempstr;
	    }
	}
      *endpos = tempchar;
      if (*endpos == '\0')
	break;
      endpos = pos + 1;
    }

  if (newstr == NULL || *newstr == '\0')
    {
      if (newstr != NULL)
	g_free (newstr);
      newstr = g_malloc (2);
      *newstr = G_DIR_SEPARATOR;
      *(newstr + 1) = '\0';
    }

  if (newstr != NULL)
    {
      g_free (str);
      str = newstr;
    }

  if (pw != NULL)
    {
      if ((pos = strchr (str, G_DIR_SEPARATOR)) == NULL)
	{
	  newstr = g_malloc (strlen (pw->pw_dir) + 1);
	  strcpy (newstr, pw->pw_dir);
	}
      else
	newstr = g_strconcat (pw->pw_dir, pos, NULL);
      if (str)
	g_free (str);
      return (newstr);
    }
  else
    {
      newstr = g_malloc (strlen (str) + 1);
      strcpy (newstr, str);
    }
  g_free (str);
  str = newstr;
  newstr = filename_toutf8(str);
  g_free(str);
  return (newstr);
#else
  return g_strdup(src);
#endif
}

/* Test if a file is writeable or not */
gboolean test_writeable(gchar *path)
{
  FILE *file;

  /* Test to know if we can write into the file */
  file = (FILE *) g_fopen(path,READPLUS_FOPEN_STRING);
  if (file == NULL) {
    return FALSE;
  }
  fclose(file);
  return TRUE;
}

gboolean create_directory(gchar *path)
{
  if (g_mkdir(path, CREATE_DIRMODE) == -1) {
    return FALSE;
  } else {
    return TRUE;
  }
}

void create_dirs(gchar *filename) {
  struct stat st;
  gchar **tmpv;
  gchar *path;
  gint i, dirs;

  tmpv = g_strsplit(filename, G_DIR_SEPARATOR_S, 0);
  for (dirs=0; tmpv[dirs]; ++dirs);
  if (--dirs < 1)
    goto out;

  path = g_strdup(tmpv[0]);
  for (i=0;;) {
    gchar *tmp;
    gint ret = 0;

    ret = stat(path, &st);
    if (ret < 0 || !S_ISDIR(st.st_mode))
      create_directory(path);
    if (++i >= dirs)
      break;
    tmp = path;
    path = g_strjoin(G_DIR_SEPARATOR_S, path, tmpv[i], NULL);
    g_free(tmp);
  }
  g_free(path);
out:
  g_strfreev(tmpv);
}

void export_playlistfile(gchar *filename, gchar *formatstring, GList *playlist, gboolean m3ufile, gboolean plsfile)
{
  gchar *tmpname = filename_toutf8(filename);
  gint fd;
  gchar m3uheader[] = "#EXTM3U\n";
  gchar plsheader[] = "[playlist]\n";
  GList *tmp;
  guint i;

  fd = (gint) g_open(tmpname, WRITEONLY_FLAGS, (mode_t) CREATE_FILEMODE);
  if (fd < 0)
#if !GLIB_CHECK_VERSION(2,8,0)
    fd = (gint) creat(tmpname, (mode_t) CREATE_FILEMODE);
#else
    fd = (gint) g_creat(tmpname, (mode_t) CREATE_FILEMODE);
#endif
  if (fd < 0) {
    create_error_dialog(_("Could not write exported playlist"));
    return;
  }

  if (m3ufile) {
    write(fd, m3uheader, strlen(m3uheader));
  } else if (plsfile) {
    write(fd, plsheader, strlen(plsheader));
  }

  tmp = g_list_first(playlist);
  i = 1;
  while (tmp != NULL) {
    gchar *song;
    gchar *row;
    metadata_t *meta;

    meta = (metadata_t *) tmp->data;
    tmp = g_list_next(tmp);
    song = compose_metadata_string(meta, formatstring, FALSE);
    if (m3ufile || plsfile) {
      guint tmplen = mmss_to_seconds(meta->length);
      gchar *tmpfname = compose_filename(meta);
      if (m3ufile) {
	row = g_strdup_printf("#EXTINF:%u,%s\n%s\n", tmplen, song, tmpfname);
      } else {
	row = g_strdup_printf("File%u=%s\nTitle%u=%s\nLength%u=%u\n", i, tmpfname,
			      i, song, i, tmplen);
      }
      g_free(tmpfname);
    } else {
      row = g_strdup_printf("%d. %s\n", i, song);
    }
    write(fd, row, strlen(row));
    g_free(song);
    g_free(row);
    i++;
  }

  if (plsfile) {
    gchar *footer = g_strdup_printf("NumberOfEntries=%u\nVersion=2\n", i-1);
    write(fd, footer, strlen(footer));
    g_free(footer);
  }

  close(fd);
}
