/* playlists.c
   Playlist GUI and call handling
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "jukebox.h"
#include "xfer.h"
#include "player.h"
#include "util.h"
#include "prefs.h"
#include "filesystem.h"

/* Local variables */
static GtkWidget *new_pl_dialog;
static GtkWidget *new_pl_entry;
static GtkWidget *edit_pl_dialog;
static GtkWidget *edit_pl_entry;
static GtkWidget *export_pl_dialog;
static GtkWidget *export_pl_entry;
static GtkWidget *export_pl_format_entry;
static GtkWidget *select_playlist_dialog;
static GtkWidget *m3ulist;
static GtkWidget *plslist;

/***************************************************************************************
 * UTILITY FUNCTIONS
 ***************************************************************************************/

/* Text you get from here must be freed afterwards */
static gchar *get_text_from_selection_column(gint column)
{
  GtkTreeSelection *selection;
  GtkTreeIter iter;
  gchar *text = NULL;

  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(playlist_widgets.pltreeview));
  if (gtk_tree_selection_get_selected (selection, (GtkTreeModel **) &playlist_widgets.pltreestore, &iter)) {
    gtk_tree_model_get(GTK_TREE_MODEL(playlist_widgets.pltreestore), &iter, column, &text, -1);
  }
  return text;
}

static guint32 get_playlistid_from_selection(void)
{
  gchar *text = NULL;
  guint32 plid = 0;

  text = get_text_from_selection_column(PLIST_PLID_COLUMN);
  if (text != NULL) {
    plid = string_to_guint32(text);
    g_free(text);
  }
  return plid;
}

static guint32 get_trackid_from_selection(void)
{
  gchar *text = NULL;
  guint32 trackid = 0;

  text = get_text_from_selection_column(PLIST_SONGID_COLUMN);
  if (text != NULL) {
    trackid = string_to_guint32(text);
    g_free(text);
  }
  return trackid;
}


static void remove_selected_row(void)
{
  GtkTreeSelection *selection;
  GtkTreeIter iter;

  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(playlist_widgets.pltreeview));
  if (gtk_tree_selection_get_selected (selection, (GtkTreeModel **) &playlist_widgets.pltreestore, &iter)) {
    gtk_tree_store_remove(playlist_widgets.pltreestore, &iter);
  }
}

static gboolean foreach_renumber(GtkTreeModel *model,
				 GtkTreePath *path,
				 GtkTreeIter *iter,
				 gpointer data)
{
  gchar *text;
  gchar **swap = (gchar **) data;

  gtk_tree_model_get(model, iter, PLIST_PLID_COLUMN, &text, -1);
  /* g_print("Found playlist %s\n", text); */
  if (!strcmp(text, swap[0])) {
    /* g_print("Renumbering to %s\n", swap[1]); */
    gtk_tree_store_set(GTK_TREE_STORE(model), iter,
		       PLIST_PLID_COLUMN, swap[1], -1);
  }
  g_free(text);
  return FALSE;
}

/* Change playlist ID on all occurences in the list store */
static void update_playlistid_in_store(guint32 old_plid, guint32 new_plid)
{
  gchar *swap[2];

  swap[0] = g_strdup_printf("%" PRIguint32, old_plid);
  swap[1] = g_strdup_printf("%" PRIguint32, new_plid);
  /* g_print("Trying to renumber playlist %s to %s...\n", swap[0], swap[1]); */
  gtk_tree_model_foreach(GTK_TREE_MODEL(playlist_widgets.pltreestore),
			 (GtkTreeModelForeachFunc) foreach_renumber,
			 (gpointer) swap);
  g_free(swap[0]);
  g_free(swap[1]);
}

/***************************************************************************************
 * CALLBACK FUNCTIONS FOR GUI
 ***************************************************************************************/

/* Deletes the dialog windows, passed as data */
GCallback dispose_of_dialog_window(GtkButton * button, gpointer data)
{
	GtkWidget * tmpwid=GTK_WIDGET(data);
	gtk_widget_destroy(tmpwid);
	return NULL;
}

static GCallback create_new_playlist_ok_button (GtkButton *button,
						gpointer data)
{
  gchar *plname;
  guint32 plid;

  plname = g_strdup(gtk_entry_get_text(GTK_ENTRY(new_pl_entry)));
  gtk_widget_destroy(new_pl_dialog);
  plid = jukebox_create_playlist(plname, playlist_widgets.pltreestore);
  g_free(plname);
  return NULL;
}


/* Response to the New playlist popup menu function */
static GCallback playlist_new_response(gpointer data)
{
  GtkWidget *label, *button;

  new_pl_dialog=gtk_dialog_new_with_buttons(_("Create a new playlist"),
		  NULL,
		  0,
		  NULL);

  button = gtk_dialog_add_button(GTK_DIALOG(new_pl_dialog),
		  GTK_STOCK_CANCEL,
		  0);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(dispose_of_dialog_window),
		  GTK_OBJECT(new_pl_dialog));

  button = gtk_dialog_add_button(GTK_DIALOG(new_pl_dialog),
		  GTK_STOCK_OK,
		  1);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(create_new_playlist_ok_button),
		  NULL);

  gtk_window_set_position (GTK_WINDOW(new_pl_dialog), GTK_WIN_POS_MOUSE);

  label = gtk_label_new(_("Choose a name for the new playlist:"));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_pl_dialog)->vbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  new_pl_entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_pl_dialog)->vbox), new_pl_entry, TRUE, TRUE, 0);
  gtk_widget_show(new_pl_entry);

  g_signal_connect(G_OBJECT (new_pl_dialog),
		   "delete_event",
		   G_CALLBACK(gtk_widget_destroy),
		   G_OBJECT (new_pl_dialog));

  gtk_widget_show_all(new_pl_dialog);
  return NULL;
}

/* Response to the Play playlist popup menu function */
static GCallback playlist_play_response(gpointer data)
{
  guint32 plid;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }
  /* Get the ID of the playlist */
  plid = get_playlistid_from_selection();
  if (plid > 0)
    play_playlist(plid);
  return NULL;
}

static GCallback edit_playlist_ok_button (GtkButton *button,
				     gpointer data)
{
  guint plid;
  gchar *plname;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }

  plid = get_playlistid_from_selection();
  plname = g_strdup(gtk_entry_get_text(GTK_ENTRY(edit_pl_entry)));
  jukebox_rename_playlist(plid, plname, playlist_widgets.pltreestore);
  gtk_widget_destroy(edit_pl_dialog);

  g_free(plname);
  return NULL;
}

/* Response to the Edit playlist popup menu function */
static GCallback playlist_edit_response(gpointer data)
{
  GtkWidget *label, *button;
  gchar *text;

  edit_pl_dialog=gtk_dialog_new_with_buttons(_("Edit playlist"),
		  NULL,
		  0,
		  NULL);

  button = gtk_dialog_add_button(GTK_DIALOG(edit_pl_dialog),
		  GTK_STOCK_CANCEL,
		  0);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(dispose_of_dialog_window),
		  GTK_OBJECT(edit_pl_dialog));

  button = gtk_dialog_add_button(GTK_DIALOG(edit_pl_dialog),
		  GTK_STOCK_OK,
		  1);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(edit_playlist_ok_button),
		  NULL);
  gtk_window_set_position (GTK_WINDOW(edit_pl_dialog), GTK_WIN_POS_MOUSE);

  label = gtk_label_new(_("Edit the name for this playlist:"));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(edit_pl_dialog)->vbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  edit_pl_entry = gtk_entry_new();
  // Get the old name of the playlist
  text = get_text_from_selection_column(PLIST_PLAYLISTNAME_COLUMN);
  if (text != NULL) {
    gtk_entry_set_text(GTK_ENTRY(edit_pl_entry),
		       text);
    g_free(text);
  }
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(edit_pl_dialog)->vbox), edit_pl_entry, TRUE, TRUE, 0);
  gtk_widget_show(edit_pl_entry);
  g_signal_connect_object(GTK_OBJECT (edit_pl_dialog),
			  "delete_event",
			  G_CALLBACK(gtk_widget_destroy),
			  GTK_OBJECT (edit_pl_dialog),
			  0);
  gtk_widget_show_all(edit_pl_dialog);
  return NULL;
}

static void toggle_m3ulist (GtkCheckButton *toggle,
			    gpointer data)
{
  gboolean state;
  gchar *text;
  guint len;

  if (m3ulist == NULL)
    return;

  text = g_strdup(gtk_entry_get_text(GTK_ENTRY(export_pl_entry)));

  if (text == NULL)
    return;

  len = strlen(text);

  state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m3ulist));

  if (state == TRUE) {
    // We just selected to use an M3U list
    // First deselect the PLS option
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(plslist), FALSE);
    if (text != NULL) {
      if (!strcmp(text+len-4, ".txt") ||
	  !strcmp(text+len-4, ".pls")) {
	// Replace .txt or .pls for .m3u
	text[len-3] = 'm';
	text[len-2] = '3';
	text[len-1] = 'u';
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry), text);
      }
    }
  }

  else {
    // We just deselected the use of an m3u list
    if (text != NULL) {
      if (!strcmp(text+len-4, ".m3u")) {
	// Replace .m3u for .txt
	text[len-3] = 't';
	text[len-2] = 'x';
	text[len-1] = 't';
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry), text);
      }
    }
  }

  g_free(text);
}


static void toggle_plslist (GtkCheckButton *toggle,
			    gpointer data)
{
  gboolean state;
  gchar *text;
  guint len;

  if (plslist == NULL)
    return;

  text = g_strdup(gtk_entry_get_text(GTK_ENTRY(export_pl_entry)));

  if (text == NULL)
    return;

  len = strlen(text);

  state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(plslist));

  if (state == TRUE) {
    // We just selected to use an PLS list
    // First deselect the M3U option
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m3ulist), FALSE);
    if (text != NULL) {
      if (!strcmp(text+len-4, ".txt") ||
	  !strcmp(text+len-4, ".m3u")) {
	// Replace .txt or .m3u for .pls
	text[len-3] = 'p';
	text[len-2] = 'l';
	text[len-1] = 's';
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry), text);
      }
    }
  }

  else {
    // We just deselected the use of an m3u list
    if (text != NULL) {
      if (!strcmp(text+len-4, ".pls")) {
	// Replace .pls for .txt
	text[len-3] = 't';
	text[len-2] = 'x';
	text[len-1] = 't';
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry), text);
      }
    }
  }

  g_free(text);
}

static GCallback export_playlist_ok_button (GtkButton *button,
				     gpointer data)
{
  gchar *filename;
  gchar *formatstring;
  GList *playlist = (GList *) data;
  gboolean m3ufile = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m3ulist));
  gboolean plsfile = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(plslist));

  filename = g_strdup(gtk_entry_get_text(GTK_ENTRY(export_pl_entry)));
  formatstring = g_strdup(gtk_entry_get_text(GTK_ENTRY(export_pl_format_entry)));
  gtk_widget_destroy(export_pl_dialog);

  export_playlistfile(filename, formatstring, playlist, m3ufile, plsfile);

  destroy_metalist(playlist);
  g_free(filename);
  g_free(formatstring);
  return NULL;
}

/* Response to the Export playlist popup menu function */
static GCallback playlist_export_response(gpointer data)
{
  guint plid;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }
  /* Get the ID of the playlist */
  plid = get_playlistid_from_selection();
  if (plid > 0) {
    GList *playlist = jukebox_get_playlist_for_play(plid);
    GtkWidget *label;
    gchar *text;

    if (playlist != NULL) {
      GtkWidget * button;
      export_pl_dialog=gtk_dialog_new_with_buttons(_("Export playlist"),
		  NULL,
		  0,
		  NULL);

      button = gtk_dialog_add_button(GTK_DIALOG(export_pl_dialog),
		  GTK_STOCK_CANCEL,
		  0);
      g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(dispose_of_dialog_window),
		  GTK_OBJECT(export_pl_dialog));

      button = gtk_dialog_add_button(GTK_DIALOG(export_pl_dialog),
		  GTK_STOCK_OK,
		  1);
      g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(export_playlist_ok_button),
		  playlist);
      gtk_window_set_position (GTK_WINDOW(export_pl_dialog), GTK_WIN_POS_MOUSE);

      label = gtk_label_new(_("Export playlist to this file:"));
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), label, TRUE, TRUE, 0);
      gtk_widget_show(label);
      export_pl_entry = gtk_entry_new();
      // Get the old name of the playlist
      text = get_text_from_selection_column(PLIST_PLAYLISTNAME_COLUMN);
      if (text != NULL) {
	text = stringcat(text, ".txt");
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry),
			   text);
	g_free(text);
      } else {
	gtk_entry_set_text(GTK_ENTRY(export_pl_entry),
			   _("Unnamed playlist.txt"));
      }
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), export_pl_entry, TRUE, TRUE, 0);
      gtk_widget_show(export_pl_entry);

      label = gtk_label_new(_("Format the playlist according to this string:"));
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), label, TRUE, TRUE, 0);
      gtk_widget_show(label);

      export_pl_format_entry = gtk_entry_new();
      text = get_prefs_filenameformat();
      gtk_entry_set_text(GTK_ENTRY(export_pl_format_entry), text);
      g_free(text);
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), export_pl_format_entry, TRUE, TRUE, 0);
      gtk_widget_show(export_pl_format_entry);

      /* Don't translate the magic %-things! */
      label = gtk_label_new(_("%a = Artist, %t = Title, %b = Album, %g = Genre, %n = Track number, %y = Year"));
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), label, TRUE, TRUE, 0);
      gtk_widget_show(label);

      m3ulist = gtk_check_button_new_with_label(_("Create an M3U playlist (assuming the format filter sets " \
						  "the correct filenames)"));
      g_signal_connect_object(GTK_OBJECT(m3ulist),
			      "toggled",
			      G_CALLBACK(toggle_m3ulist),
			      NULL,
			      0);
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), m3ulist, TRUE, TRUE, 0);
      gtk_widget_show(m3ulist);

      plslist = gtk_check_button_new_with_label(_("Create an PLS playlist (assuming the format filter sets " \
						  "the correct filenames)"));
      g_signal_connect_object(GTK_OBJECT(plslist),
			      "toggled",
			      G_CALLBACK(toggle_plslist),
			      NULL,
			      0);
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(export_pl_dialog)->vbox), plslist, TRUE, TRUE, 0);
      gtk_widget_show(plslist);

      g_signal_connect_object(GTK_OBJECT (export_pl_dialog),
			      "delete_event",
			      G_CALLBACK(gtk_widget_destroy),
			      GTK_OBJECT (export_pl_dialog),
			      0);
      gtk_widget_show_all(export_pl_dialog);
    }
  }
  return NULL;
}

/* Response to the Delete playlist popup menu function */
static GCallback playlist_delete_response(gpointer data)
{
  guint plid;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }
  plid = get_playlistid_from_selection();
  if (plid) {
      if (request_confirmation_dialog(_("Really delete selected playlist?"))) {
	jukebox_delete_playlist(plid);
	/* g_print("Deleting playlist %lu...\n", plid); */
	remove_selected_row();
      }
    }
  return NULL;
}

/* Response to the Shuffle playlist popup menu function */
static GCallback playlist_shuffle_response(gpointer data)
{
  guint plid;
  guint newplaylist;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }
  plid = get_playlistid_from_selection();
  newplaylist = jukebox_randomize_playlist(plid, playlist_widgets.pltreestore);
  /* g_print("Shuffeling playlist %lu...\n", plid); */
  return NULL;
}

/* Response to the Delete track popup menu function */
static GCallback node_delete_response(gpointer data)
{
  guint32 plid;
  guint32 track;
  guint32 newplaylist;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return NULL;
  }

  if (request_confirmation_dialog(_("Really delete selected tracks?"))) {
    plid = get_playlistid_from_selection();
    track = get_trackid_from_selection();
    /* g_print("Trying to delete track %lu from playlist %lu\n", track, plid); */
    newplaylist = jukebox_delete_track_from_playlist(track, plid, playlist_widgets.pltreestore);
    remove_selected_row();
    /* Change this in all affected places in the liststore */
    // This should work now.
    update_playlistid_in_store(plid, newplaylist);
  }
  return NULL;
}


static GCallback mouseevent (GtkWidget      *clist,
			     GdkEventButton *event,
			     gpointer        data )
{
  if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    GdkEventButton *bevent = (GdkEventButton *) event;
    guint trackid = 0;

    /* Get the trackid of what is under the mouse cursor */
    trackid = get_trackid_from_selection();

    if (trackid == 0) {
      gtk_menu_popup (GTK_MENU (playlist_widgets.playlistpopupmenu),
		      NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    } else {
      gtk_menu_popup (GTK_MENU (playlist_widgets.nodepopupmenu),
		      NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    }

    return (gpointer) 1;
  }
  return data;
}

static GtkWidget *create_option_menu(void)
{
  GtkWidget *combo;
  GList *tmplist = g_list_first(jukebox_playlist);

  /* Create the option menu */
#if GTK_CHECK_VERSION(2,24,0)
  // Use the new combo box type in 2.24 and later
  combo = gtk_combo_box_text_new();
  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), "(None)");
#else
  combo = gtk_combo_box_new_text();
  gtk_combo_box_append_text(GTK_COMBO_BOX(combo), "(None)");
#endif

  /* Add a row to the menu */
  while (tmplist) {
#if GTK_CHECK_VERSION(2,24,0)
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), tmplist->data);
#else
    gtk_combo_box_append_text(GTK_COMBO_BOX(combo), tmplist->data);
#endif
    tmplist = tmplist->next;
    tmplist = tmplist->next;
  }

  gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);

  return combo;
}

/* Pre-declare this function */
static void add_pl_selection(GtkBox *box, guint number);

static GCallback toggle_and(gpointer a, gpointer b)
{
  GtkToggleButton *tbutton = GTK_TOGGLE_BUTTON(a);
  guint next = GPOINTER_TO_UINT(b)+1;

  /* Force the toggle button to active and grey it out */
  gtk_toggle_button_set_active(tbutton, TRUE);
  gtk_widget_set_sensitive(GTK_WIDGET(tbutton), FALSE);
  /* If this was not previously selected, it's time to create a new one */
  add_pl_selection(GTK_BOX(GTK_DIALOG(select_playlist_dialog)->vbox), next);
  return NULL;
}

/*
 * box = box to add this one to
 * number = menu number (successive created) first is 0.
 */
static void add_pl_selection(GtkBox *box, guint number)
{
  GtkWidget *and = NULL;
  GtkWidget *option_menu, *hbox;

  hbox = gtk_hbox_new(FALSE, 0);
  option_menu = create_option_menu();
  selected_target_playlists = g_list_append(selected_target_playlists, option_menu);
  gtk_box_pack_start(GTK_BOX(hbox), option_menu, TRUE, TRUE, 0);
  gtk_widget_show(option_menu);
  /* and .. */
  and = gtk_check_button_new_with_label(_("and"));
  g_signal_connect(GTK_OBJECT(and),
		   "clicked",
		   G_CALLBACK(toggle_and),
		   GUINT_TO_POINTER(number));
  gtk_box_pack_start(GTK_BOX(hbox), and, TRUE, TRUE, 0);
  gtk_widget_show(and);
  gtk_box_pack_start(GTK_BOX(box), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);
}

static void playlist_selection_dialog(gchar *title, gchar *prompt, GCallback ok_signal)
{
  GtkWidget *label, *button;

  /* Make sure this is cleared */
  if (selected_target_playlists != NULL) {
    g_list_free(selected_target_playlists);
    selected_target_playlists = NULL;
  }
  select_playlist_dialog=gtk_dialog_new_with_buttons(title,
		  NULL,
		  0,
		  NULL);

  button = gtk_dialog_add_button(GTK_DIALOG(select_playlist_dialog),
		  GTK_STOCK_CANCEL,
		  0);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(dispose_of_dialog_window),
		  GTK_OBJECT(select_playlist_dialog));

  button = gtk_dialog_add_button(GTK_DIALOG(select_playlist_dialog),
		  GTK_STOCK_OK,
		  1);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(ok_signal),
		  NULL);
  gtk_window_set_position (GTK_WINDOW(select_playlist_dialog), GTK_WIN_POS_MOUSE);
  g_signal_connect_object(GTK_OBJECT (select_playlist_dialog),
			  "delete_event",
			  G_CALLBACK(gtk_widget_destroy),
			  GTK_OBJECT(select_playlist_dialog),
			  0);
  /* Add a label and the option menu */
  label = gtk_label_new(prompt);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(select_playlist_dialog)->vbox), label, TRUE, TRUE, 0);
  /* Initially add one selector */
  add_pl_selection(GTK_BOX(GTK_DIALOG(select_playlist_dialog)->vbox), 0);
  gtk_widget_show(select_playlist_dialog);
}

/* Find the playlists selected for addition */
static GList *get_selected_playlists(void)
{
  GList *templist;
  GList *retlist = NULL;

  templist = g_list_first(selected_target_playlists);
  while (templist != NULL) {
    guint selection;

    selection = gtk_combo_box_get_active(GTK_COMBO_BOX(templist->data));
    if (selection != 0) {
      /* Find out playlist ID of this entry */
      GList *tmp = g_list_nth(jukebox_playlist, (selection-1)*2+1);
      retlist = g_list_append(retlist, tmp->data);
    }
    templist = g_list_next(templist);
  }
  return retlist;
}

static void add_to_playlist_ok_button (GtkButton *button,
				       gpointer data)
{
  GList *metalist = get_all_metadata_from_selection(JB_LIST);
  GList *listlist = get_selected_playlists(); /* Playlists to add these to. */

  /* If no songs were selected, abort this */
  if (!metalist) {
    gtk_widget_destroy(select_playlist_dialog);
    g_list_free(selected_target_playlists);
    return;
  }
  /* If no playlist was selected, abort this. */
  if (listlist == NULL) {
    gtk_widget_destroy(select_playlist_dialog);
    g_list_free(selected_target_playlists);
    destroy_metalist(metalist);
    return;
  }
  /* See if the jukebox is locked. */
  if (jukebox_locked) {
    gtk_widget_destroy(select_playlist_dialog);
    g_list_free(listlist);
    g_list_free(selected_target_playlists);
    destroy_metalist(metalist);
    create_error_dialog(_("Jukebox busy"));
    return;
  }
  /* g_print("Calling add_tracks_to_playlists()\n"); */
  add_tracks_to_playlists(listlist, metalist);
  build_playlist_tree();
  /* Free any used memory */
  g_list_free(listlist);
  g_list_free(selected_target_playlists);
  selected_target_playlists = NULL;
  gtk_widget_destroy(select_playlist_dialog);
  destroy_metalist(metalist);
}

static GCallback transfer_from_hd_ok_button (GtkButton *button,
                                             gpointer data)
{
  GList *listlist = get_selected_playlists(); /* Playlists to add these to. */
  gtk_widget_destroy(select_playlist_dialog);
  /* Set it as argument */
  transfer_from_hd_to_jukebox(listlist);
  return NULL;
}

void transfer_from_hd_dialog (GtkButton *inbutton,
			      gpointer data)
{
  /* If the user does not want to select playlist, s/he shan't have to */
  if (get_prefs_ask_playlist() == FALSE){
    transfer_from_hd_to_jukebox(NULL);
    return;
  }
  playlist_selection_dialog(_("Transfer to jukebox library"),
			    _("Add tracks to playlist(s):"),
			    G_CALLBACK(transfer_from_hd_ok_button));
}

void jbmenu_add_playlist_response(gpointer data)
{
  playlist_selection_dialog(_("Add to playlist"),
			    _("Add tracks to playlist(s):"),
			    G_CALLBACK(add_to_playlist_ok_button));
}

static GtkWidget *create_treeview(GtkTreeStore *treestore)
{
  GtkWidget *treeview;
  GtkCellRenderer *textrenderer;
  GtkTreeViewColumn *view;
  GtkTreeSelection *select;

  treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(treestore));
  gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(treeview), TRUE);

  textrenderer = gtk_cell_renderer_text_new ();

  view = gtk_tree_view_column_new_with_attributes (_("Playlist"),
						   textrenderer,
						   "text", PLIST_PLAYLISTNAME_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sort_column_id (view, PLIST_PLAYLISTNAME_COLUMN);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 200);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_PLAYLISTNAME_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Artist"),
						   textrenderer,
						   "text", PLIST_ARTIST_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_ARTIST_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Title"),
						   textrenderer,
						   "text", PLIST_TITLE_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_TITLE_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Length"),
						   textrenderer,
						   "text", PLIST_LENGTH_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 40);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_LENGTH_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);


  view = gtk_tree_view_column_new_with_attributes ("PLID",
						   textrenderer,
						   "text", PLIST_PLID_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 100);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  /* Hide it */
  gtk_tree_view_column_set_visible(view, FALSE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_PLID_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);

  view = gtk_tree_view_column_new_with_attributes ("SONGID",
						   textrenderer,
						   "text", PLIST_SONGID_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 100);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  /* Hide it */
  gtk_tree_view_column_set_visible(view, FALSE);
  gtk_tree_view_column_set_sort_column_id(view, PLIST_SONGID_COLUMN);
  gtk_tree_view_column_set_clickable(view, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), view);

  /* Configure selection mode */
  select = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
  gtk_tree_selection_set_mode(select,GTK_SELECTION_SINGLE);

  return treeview;
}


void create_playlist_widgets(GtkWidget *box)
{
  GtkWidget *scrolled_window, *pltreeview, *playlistpopupmenu,
    *blankpopupmenu, *nodepopupmenu, *popupmenu_item;

  selected_target_playlists = NULL;

  /* The popup menu for the playlists */
  blankpopupmenu = gtk_menu_new();
  popupmenu_item  = gtk_menu_item_new_with_label(_("New playlist"));
  gtk_menu_shell_append (GTK_MENU_SHELL(blankpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_new_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);

  playlistpopupmenu = gtk_menu_new();
  popupmenu_item  = gtk_menu_item_new_with_label(_("New playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_new_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Shuffle playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_shuffle_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Play playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_play_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Edit playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_edit_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Export playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_export_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Delete playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(playlistpopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);

  nodepopupmenu = gtk_menu_new ();
  popupmenu_item  = gtk_menu_item_new_with_label(_("New playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(nodepopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(playlist_new_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label(_("Delete track from playlist"));
  gtk_menu_shell_append(GTK_MENU_SHELL(nodepopupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT (popupmenu_item),
			  "activate",
			  G_CALLBACK(node_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);

  playlist_widgets.blankpopupmenu = blankpopupmenu;
  playlist_widgets.playlistpopupmenu = playlistpopupmenu;
  playlist_widgets.nodepopupmenu = nodepopupmenu;

  /* Create a scrolled window for the playlists */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  pltreeview = create_treeview(playlist_widgets.pltreestore);
  g_signal_connect_object((gpointer) pltreeview,
			  "button_press_event",
			  G_CALLBACK(mouseevent),
			  NULL,
			  0);
  playlist_widgets.pltreeview = pltreeview;

  gtk_container_add(GTK_CONTAINER(scrolled_window), pltreeview);
  gtk_widget_show (pltreeview);
  gtk_widget_show (scrolled_window);
  gtk_box_pack_start(GTK_BOX(box), scrolled_window, TRUE, TRUE, 0);
}
