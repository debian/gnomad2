/**
 * \file jukebox.c
 *  Functions related to the communication with jukebox hardware
 *  using the libnjb library.
 *  Copyright (C) 2001-2011 Linus Walleij
 *
 * This file is part of the GNOMAD package.
 *
 * GNOMAD is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNOMAD; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <time.h>
#include "common.h"
#include "jukebox.h"
#include "xfer.h"
#include "prefs.h"
#include "filesystem.h"
#include "filenaming.h"
#include "util.h"
#include "id3read.h"
#include <glib.h>
#include <glib/gprintf.h>

extern GtkWidget *main_window;

/* Local variables */
static gboolean use_mtp = FALSE;
static njb_t pde_devices[NJB_MAX_DEVICES];
static njb_t *pdedevice = NULL;
static int PDE_device_count = 0;
static int MTP_device_count = 0;
static gboolean device_connected = FALSE;
#ifdef HAVE_LIBMTP
static LIBMTP_mtpdevice_t *mtpdevice;
static LIBMTP_folder_t * mtp_folders=NULL;
static uint32_t mtp_current_filter_id=0;
static uint32_t mtp_last_folder_id=0;
static gchar * mtp_last_fullpath=NULL;
#endif
/* Jukebox properties variables */
static gchar *jukebox_ownerstring = NULL;
static gchar *jukebox_timestring = NULL;
static gchar *jukebox_firmware = NULL;
static gchar *jukebox_hardware = NULL;
static u_int64_t jukebox_totalbytes = 0;
static u_int64_t jukebox_freebytes = 0;
static u_int64_t jukebox_usedbytes = 0;
static guint32 jukebox_songs = 0;
static guint32 jukebox_playlists = 0;
static guint32 jukebox_datafiles = 0;
/* Variables used for playing tracks */
static GList *playlist;
static GList *playlistitr;
static GList *playlistlast;
static GtkWidget *songnamelabel;
static gboolean created_play_mutex = FALSE;
static GMutex *play_thread_mutex = NULL;
static gboolean passed_first_zero = FALSE;
/* After initial scan, this hash table always contains
 * all the songs arranged by song ID */
static GHashTable *songhash = NULL;
/* This is used temporarily for storing a list of
 * playlist entries (gnomadplaylist_entry_t), which
 * in turn stores the playlists as GSLists with track
 * IDs.
 *
 * playlistlist -> list-> list -> list -> ... -> NULL
 *                 |      |       |
 *                 entry  entry   entry
 *                 |
 *                 +-> name
 *                 +-> plid
 *                 +-> tracklist
 *                     |
 *                     +-> track -> track -> ... -> NULL
 */
static GSList *playlistlist = NULL;
/*
 * This is a file list with folder information, filtered
 * to present a folder view
 */
static GSList *datafilelist = NULL;

#ifdef HAVE_LIBMTP
/*
 * MTP FILETYPE <-> INTERNAL CODEC STRING relation
 */
typedef struct {
  LIBMTP_filetype_t  filetype;
  const gchar       *codec;
} mtp_filetype_description_t;

static const mtp_filetype_description_t mtp_filetype_descriptions[] = {
  { LIBMTP_FILETYPE_WAV,     "WAV" },
  { LIBMTP_FILETYPE_MP3,     "MP3" },
  { LIBMTP_FILETYPE_WMA,     "WMA" },
  { LIBMTP_FILETYPE_OGG,     "OGG" },
  { LIBMTP_FILETYPE_MP4,     "MP4" },
  { LIBMTP_FILETYPE_WMV,     "WMV" },
  { LIBMTP_FILETYPE_AVI,     "AVI" },
  { LIBMTP_FILETYPE_MPEG,    "MPG" },
  { LIBMTP_FILETYPE_MPEG,    "MPEG" },
  { LIBMTP_FILETYPE_ASF,     "ASF" },
  { LIBMTP_FILETYPE_QT,      "MOV" },
  { LIBMTP_FILETYPE_JPEG,    "JPG" },
  { LIBMTP_FILETYPE_JPEG,    "JPEG" },
  { LIBMTP_FILETYPE_JFIF,    "JFIF" },
  { LIBMTP_FILETYPE_TIFF,    "TIF" },
  { LIBMTP_FILETYPE_TIFF,    "TIFF" },
  { LIBMTP_FILETYPE_BMP,     "BMP" },
  { LIBMTP_FILETYPE_GIF,     "GIF" },
  { LIBMTP_FILETYPE_PICT,    "PIC" },
  { LIBMTP_FILETYPE_PICT,    "PICT" },
  { LIBMTP_FILETYPE_PNG,     "PNG" },
  { LIBMTP_FILETYPE_WINDOWSIMAGEFORMAT, "WMF" },
  { LIBMTP_FILETYPE_VCALENDAR2, "ICS" },
  { LIBMTP_FILETYPE_VCARD3,  "VCF" },
  { LIBMTP_FILETYPE_WINEXEC, "EXE" },
  { LIBMTP_FILETYPE_WINEXEC, "COM" },
  { LIBMTP_FILETYPE_WINEXEC, "BAT" },
  { LIBMTP_FILETYPE_WINEXEC, "DLL" },
  { LIBMTP_FILETYPE_WINEXEC, "SYS" },
  { LIBMTP_FILETYPE_TEXT,    "TXT" },
  { LIBMTP_FILETYPE_HTML,    "HTML" },
  { LIBMTP_FILETYPE_AAC,     "AAC" },
  { LIBMTP_FILETYPE_FLAC,    "FLAC" },
  { LIBMTP_FILETYPE_MP2,     "MP2" },
  { LIBMTP_FILETYPE_M4A,     "M4A" },
  { LIBMTP_FILETYPE_DOC,     "DOC" },
  { LIBMTP_FILETYPE_XML,     "XML" },
  { LIBMTP_FILETYPE_XLS,     "XLS" },
  { LIBMTP_FILETYPE_PPT,     "PPT" },
  { LIBMTP_FILETYPE_MHT,     "MHT" },
  { LIBMTP_FILETYPE_JP2,     "JP2" },
  { LIBMTP_FILETYPE_JPX,     "JPX" },
  { LIBMTP_FILETYPE_UNKNOWN, "N/A" }
};
#endif

/* Forward declarations where necessary */
static void build_tree_widget(void);
static void destroy_datafile_list(void);
static void destroy_tree_structure(void);

/* Local static functions */
#ifdef HAVE_LIBMTP
static const mtp_filetype_description_t*
get_mtp_filetype_description(LIBMTP_filetype_t type) {
  const mtp_filetype_description_t *current;

  current = &mtp_filetype_descriptions[0];
  while (current->filetype != LIBMTP_FILETYPE_UNKNOWN) {
    if (current->filetype == type) return current;
    ++current;
  }
  return current; /* I.E. type == LIBMTP_FILETYPE_UNKNOWN */
}

static const mtp_filetype_description_t*
get_mtp_filetype_description_by_codec(const gchar *name) {
  const mtp_filetype_description_t *current;

  g_assert(name != NULL);
  current = &mtp_filetype_descriptions[0];
  while (current->filetype != LIBMTP_FILETYPE_UNKNOWN) {
    if (strcmp(current->codec, name) == 0)
      return current;
    ++current;
  }
  return current; /* I.E. type == LIBMTP_FILETYPE_UNKNOWN */
}
#endif

/* Return disk usage */
static void jukebox_getusage(guint64 *total, guint64 *free, guint64 *used,
			     guint32 *songs, guint32 *playlists, guint32 *datafiles)
{
  *total = jukebox_totalbytes;
  *free = jukebox_freebytes;
  *used = jukebox_usedbytes;
  *songs = jukebox_songs;
  *playlists = jukebox_playlists;
  *datafiles = jukebox_datafiles;
}

/*
 * Public and private functions
 */

static void display_njberror(njb_t *njb, gchar *template)
{
  gboolean found_njb_error = FALSE;
  const gchar *sp;
  GString *error_string = g_string_new(template);

  if (njb != NULL) {
    NJB_Error_Reset_Geterror(njb);
    while ((sp = NJB_Error_Geterror(njb)) != NULL) {
      found_njb_error = TRUE;
      g_string_append_printf(error_string, "%s\n", sp);
    }
  } else {
    found_njb_error = TRUE;
    g_string_append_printf(error_string, _("Could not open the device on the USB bus"));
  }

  if (!found_njb_error)
    g_string_append_printf(error_string, _("(unknown cause)"));

  create_error_dialog(error_string->str);
  g_string_free(error_string, TRUE);
}

static void refresh_id(void)
{
  if (!use_mtp && pdedevice != NULL) {
    NJB_Ping(pdedevice);
  }
}

/* Returns the unique jukebox ID */
static gchar *jukebox_get_idstring(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    return LIBMTP_Get_Serialnumber(mtpdevice);
#else
    return NULL;
#endif
  } else {
    if (pdedevice != NULL) {
      u_int8_t sdmiid[16];

      if (NJB_Get_SDMI_ID(pdedevice, (u_int8_t *) &sdmiid) == 0) {
	return g_strdup_printf("%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			       sdmiid[0], sdmiid[1], sdmiid[2], sdmiid[3], sdmiid[4],
			       sdmiid[5], sdmiid[6], sdmiid[7], sdmiid[8], sdmiid[9],
			       sdmiid[10], sdmiid[11], sdmiid[12], sdmiid[13], sdmiid[14],
			       sdmiid[15]);
      } else {
	return NULL;
      }
    } else {
      return NULL;
    }
  }
}

/* Returns a string representing the firmware revision */
static gchar *jukebox_get_firmware(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    return LIBMTP_Get_Deviceversion(mtpdevice);
#else
    return NULL;
#endif
  } else {
    if (pdedevice != NULL) {
      if (jukebox_firmware == NULL) {
	u_int8_t major, minor, release;

	if (NJB_Get_Firmware_Revision(pdedevice, &major, &minor, &release) == -1) {
	  return NULL;
	}
	if (pdedevice->device_type == NJB_DEVICE_NJB1) {
	  jukebox_firmware = g_strdup_printf("%u.%u", major, minor);
	} else {
	  jukebox_firmware = g_strdup_printf("%u.%u.%u", major, minor, release);
	}
      }
      return jukebox_firmware;
    } else {
      return NULL;
    }
  }
}

/* Returns a string representing the hardware revision */
static gchar *jukebox_get_hardware(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    return LIBMTP_Get_Deviceversion(mtpdevice);
#else
    return NULL;
#endif
  } else {
    if (pdedevice != NULL) {
      if (jukebox_hardware == NULL) {
	u_int8_t major, minor, release;

	if (NJB_Get_Hardware_Revision(pdedevice, &major, &minor, &release) == -1) {
	  return NULL;
	}
	jukebox_hardware = g_strdup_printf("%u.%u.%u", major, minor, release);
      }
      return jukebox_hardware;
    } else {
      return NULL;
    }
  }
}

/* Returns the product name */
static gchar *jukebox_get_prodname(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    return LIBMTP_Get_Modelname(mtpdevice);
#else
    return NULL;
#endif
  } else {
    if (pdedevice != NULL) {
      return (gchar *) NJB_Get_Device_Name(pdedevice, 1);
    } else {
      return NULL;
    }
  }
}

/* Returns data about if the power cord is connected */
static gboolean jukebox_get_power(void)
{
  if (use_mtp) {
    // Not supported yet.
    return FALSE;
  } else {
    if (pdedevice != NULL) {
      int powerstat = NJB_Get_Auxpower(pdedevice);
      if (powerstat == 1) {
	return TRUE;
      } else {
	return FALSE;
      }
    } else {
      return FALSE;
    }
  }
}

/* Returns data about if the power cord is connected */
static gboolean jukebox_get_charging(void)
{
  if (use_mtp) {
    // Not supported yet.
    return TRUE;
  } else {
    if (pdedevice != NULL) {
      int chargestat = NJB_Get_Battery_Charging(pdedevice);
      if (chargestat == 1) {
	return TRUE;
      } else {
	return FALSE;
      }
    } else {
      return FALSE;
    }
  }
}

/* Returns the battery level in percent */
static guint jukebox_get_battery_level(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    uint8_t max, current;
    gint ret;

    ret = LIBMTP_Get_Batterylevel(mtpdevice, &max, &current);
    if (ret == 0) {
      // Adjust to a scale between 0 and 100, linear interpolation
      gfloat percentage = ((gfloat) current) / ((gfloat) max);
      return (guint) (percentage * (gfloat) 100);
    } else {
      return 0;
    }
#else
    return 0;
#endif
  } else {
    if (pdedevice != NULL) {
      return NJB_Get_Battery_Level(pdedevice);
    } else {
      return 0;
    }
  }
}

static void add_to_dialog(GtkWidget *dialog, GtkWidget *thing)
{
  gtk_box_pack_start (GTK_BOX(GTK_DIALOG(dialog)->vbox), thing, TRUE, TRUE, 0);
}

/* This creates a device information dialog and returns it. */
GtkWidget *jukebox_get_deviceinfo_dialog(void)
{
  GtkWidget *label, *dialog, *hbox;
  gchar tmp[50];
  guint64 total, free, used;
  guint32 songs, playlists, datafiles;

  if (use_mtp) {
#ifdef HAVE_LIBMTP
    if (mtpdevice == NULL) {
      return NULL;
    }
#else
    return NULL;
#endif
  } else {
    if (pdedevice == NULL) {
      return NULL;
    }
  }

  // Make sure our information is up-to-date
  refresh_id();

  dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
				   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_INFO,
				   GTK_BUTTONS_CLOSE,
				   _("Jukebox information"));
  // dialog = gtk_dialog_new();
  gtk_window_set_title (GTK_WINDOW (dialog), (_("Jukebox information")));
  gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 5);
  gtk_box_set_homogeneous (GTK_BOX (GTK_DIALOG (dialog)->action_area), TRUE);
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "delete_event",
			  G_CALLBACK(gtk_widget_destroy),
			  NULL,
			  0);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "response",
			  G_CALLBACK(gtk_widget_destroy),
			  NULL,
			  0);
  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Unique device ID:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new(jukebox_get_idstring());
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Firmware revision:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new(jukebox_get_firmware());
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  if (!use_mtp && (pdedevice->device_type != NJB_DEVICE_NJB1)) {
    hbox = gtk_hbox_new(FALSE, 0);
    label = gtk_label_new (_("Hardware revision:"));
    gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox (hbox);
    label = gtk_label_new(jukebox_get_hardware());
    gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show(label);
    add_to_dialog(dialog, hbox);
    gtk_widget_show (hbox);
  }

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Product name:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new(jukebox_get_prodname());
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Auxilary power (AC or USB) connected:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new((jukebox_get_power()) ? _("Yes") : _("No"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Battery is charging:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new((jukebox_get_charging()) ? _("Yes") : _("No"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  // The pdedevice struct will not be dereferenced unless
  // PDE is used.
  if (use_mtp || (pdedevice != NULL && pdedevice->device_type != NJB_DEVICE_NJB1)) {
    hbox = gtk_hbox_new(FALSE, 0);
    label = gtk_label_new (_("Battery level:"));
    gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox (hbox);
    sprintf(tmp, "%d%%", jukebox_get_battery_level());
    label = gtk_label_new(tmp);
    gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show(label);
    add_to_dialog(dialog, hbox);
    gtk_widget_show (hbox);
  }

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Jukebox owner:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%s", jukebox_get_ownerstring());
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Time on jukebox:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  label = gtk_label_new(jukebox_get_time());
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  jukebox_getusage(&total, &free, &used, &songs, &playlists, &datafiles);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Total bytes on disk:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint64 " (%" PRIguint64 " MB)",
	  total, (guint64) total/G_GINT64_CONSTANT(1048576));
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Total free bytes available:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint64 " (%" PRIguint64 " MB)",
	  free, (guint64) free/G_GINT64_CONSTANT(1048576));
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Total bytes used:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint64 " (%" PRIguint64 " MB)",
	  used, (guint64) used/G_GINT64_CONSTANT(1048576));
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Number of songs:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint32, songs);
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Number of playlists:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint32, playlists);
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new (_("Number of datafiles:"));
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox (hbox);
  sprintf(tmp, "%" PRIguint32, datafiles);
  label = gtk_label_new(tmp);
  gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  add_to_dialog(dialog, hbox);
  gtk_widget_show (hbox);
  return dialog;
}

gchar *jukebox_get_time(void)
{
  njb_time_t *time;

  if (use_mtp) {
    // Not implemented
    return g_strdup("Not implemented");
  } else {
    if (pdedevice != NULL) {
      time = NJB_Get_Time(pdedevice);
      if (time != NULL) {
	if (jukebox_timestring != NULL) {
	  g_free(jukebox_timestring);
	  jukebox_timestring = NULL;
	}
	jukebox_timestring = g_strdup_printf("%u-%.2u-%.2u %.2u:%.2u:%.2u",
					     time->year, time->month, time->day,
					     time->hours, time->minutes,
					     time->seconds);
	free(time);
      }
    } else {
      if (jukebox_timestring != NULL) {
	g_free(jukebox_timestring);
      }
      jukebox_timestring = NULL;
    }
    return jukebox_timestring;
  }
}


/* Functions for manipulating the song hash */
static void destroy_hash (gpointer key,
			  gpointer value,
			  gpointer user_data)
{
  metadata_t *meta = (metadata_t *) value;
  destroy_metadata_t(meta);
}


static void destroy_hashes(void)
{
  /* Destroy the song hash */
  if (songhash != NULL) {
    g_hash_table_foreach(songhash,
			 (GHFunc) destroy_hash,
			 NULL);
    g_hash_table_destroy(songhash);
    songhash = NULL;
  }
}


static void add_row_from_songhash (gpointer key,
				 gpointer value,
				 gpointer user_data)
{
  metadata_t *meta = (metadata_t *) value;
  add_metadata_to_model(meta, JB_LIST);
  //g_print("Added %s\n", meta->title);
}


static void jblist_from_songhash(void)
{
  /* Little trick I learned from Sven Neumann, recreate the store each time,
   * because otherwise you cannot remove the sorting attribute from the store,
   * and that in turn will result in O(n^2) addition times for this store.
   * So we add rows in O(n) time to a fresh store and then connect it to the
   * view and sort it afterwards.
   */
  recreate_list_store(JB_LIST);
  /* This does not need to be threaded! */
  if (songhash != NULL) {
    g_hash_table_foreach(songhash,
			 (GHFunc) add_row_from_songhash,
			 NULL);
  }
  /* Then sort everything */
  view_and_sort_list_store(JB_LIST);
}


/* This callback handle all "cancel" buttons during
 * jukebox operations */
void cancel_jukebox_operation_click ( GtkButton *button,
				      gpointer data )
{
  cancel_jukebox_operation = TRUE;
}


/*
 * Discover devices on the USB bus and return an array of
 * descriptions that may be used to select the jukebox with
 * index from 0 upwards.
 */
gchar **jukebox_discover(void) {
  int device_count;
  gchar **retarray;

  /*
   * Intercept USB communications - invoke gnomad2
   * with gnomad2 -D7 to activate a lot of USB debugging
   * information
   */
  if (gnomad_debug != 0) {
    NJB_Set_Debug(gnomad_debug);
  }

  /* Select UTF8 because we are running Gnome 2 */
  /* Also test with NJB_UC_8859 */
  NJB_Set_Unicode(NJB_UC_UTF8);

  if ( NJB_Discover(pde_devices, 0, &PDE_device_count) == -1 ) {
    create_error_dialog(_("Could not try to locate jukeboxes\nUSB error?"));
    return NULL;
  }

#ifdef HAVE_LIBMTP
  LIBMTP_Init();
  mtpdevice = LIBMTP_Get_First_Device();
  if (mtpdevice != NULL) {
    MTP_device_count = 1;
  }
#endif

  device_count = PDE_device_count + MTP_device_count;

  if ( device_count == 0 ) {
    create_error_dialog(_("No jukeboxes found on USB bus"));
    return NULL;
  }

  // Add a nice selection dialog here njb->device_type tells the type,
  // and owner string is useful too.
  if ( device_count >= 1) {
    int i;

    // Space for jukebox strings.
    retarray = g_malloc((device_count+1) * sizeof(gchar *));
    for (i = 0; i < PDE_device_count; i++) {
      pdedevice = &pde_devices[i];
      // This function may be called prior to opening the jukebox
      // if parameterized with type 0.
      retarray[i] = g_strdup(NJB_Get_Device_Name(pdedevice, 0));
    }
    pdedevice = NULL;

    // Replace with for() statement once libmtp support multiple devices
    if (MTP_device_count > 0) {
      i++;
#ifdef HAVE_LIBMTP
      retarray[i] = g_strdup(LIBMTP_Get_Modelname(mtpdevice));
      g_print("Queried %s\n", retarray[i]);
#else
      // Should not happen but to be certain ...
      retarray[i] = NULL;
#endif
    }
    retarray[device_count] = NULL;
    return retarray;
  }

  return NULL;
}

// This selects a certain jukebox for use and sets the
// global njb variable to point to thus jukebox.
gboolean jukebox_select(gint i) {

  if (i < PDE_device_count) {
    g_print("This is a PDE device\n");
    pdedevice = &pde_devices[i];

    if (NJB_Open(pdedevice) == -1) {
      display_njberror(pdedevice, _("Could not open jukebox:\n"));
      return FALSE;
    }

    /*
     * Causing problems. Removing for now. 2004-04-12
     if ((njbid = NJB_Ping(njb)) == NULL) {
     create_error_dialog(_("Could not ping jukebox"));
     NJB_Error_Dump(stderr);
     NJB_Close(njb);
     return FALSE;
     }
    */

    if (NJB_Capture(pdedevice) == -1) {
      display_njberror(pdedevice, _("Could not capture jukebox:\n"));
      NJB_Close(pdedevice);
      return FALSE;
    }

    /* Set turbo mode according to preferences */
    if (get_prefs_turbo_mode()) {
      NJB_Set_Turbo_Mode(pdedevice, NJB_TURBO_ON);
    } else {
      NJB_Set_Turbo_Mode(pdedevice, NJB_TURBO_OFF);
    }

    use_mtp = FALSE;
#ifdef HAVE_LIBMTP
    mtpdevice = NULL;
#endif
  }

  // Else try to connect to the MTP device
  else if (MTP_device_count > 0) {
    // g_print("This is an MTP device\n");
    use_mtp = TRUE;
    // This is implicitly connected to in the first library call.
    // Set njb to NULL to be certain this is the only device used.
    pdedevice = NULL;
  }

  // Else we have no devices
  else {
    g_print("We have no devices!\n");
    pdedevice = NULL;
    use_mtp = FALSE;
#ifdef HAVE_LIBMTP
    mtpdevice = NULL;
#endif
    return FALSE;
  }

  device_connected = TRUE;
  return TRUE;
}

// Check if a jukebox has been selected.
gboolean jukebox_selected(void)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    if (mtpdevice == NULL) {
      g_print("MTP device NULL.\n");
      return FALSE;
    }
#else
    return FALSE;
#endif
  } else {
    if (pdedevice == NULL) {
      g_print("PDE device NULL.\n");
      return FALSE;
    }
  }

  if (!device_connected) {
    g_print("Device was not connected.\n");
    return FALSE;
  }
  return TRUE;
}

gboolean jukebox_is_mtp(void)
{
  return use_mtp;
}

/* Called at end of session to release jukebox */
void jukebox_release(void)
{
  // First clean up all metadata storage internally etc.
  destroy_hashes();
  destroy_datafile_list();
  jukebox_songs = 0;
  jukebox_datafiles = 0;
  jukebox_playlists = 0;
  // Redraw song and data window blank.
  jblist_from_songhash();
  recreate_list_store(JBDATA_LIST);
  view_and_sort_list_store(JBDATA_LIST);
  // Rebuild this all blank.
  destroy_tree_structure(); // Should already be blank really.
  build_tree_widget();
  // Then actually release the device connection
  if (device_connected) {
    if (use_mtp) {
#ifdef HAVE_LIBMTP
      if (mtpdevice != NULL) {
	LIBMTP_Release_Device(mtpdevice);
      }
#endif
    } else {
      if (pdedevice != NULL) {
	NJB_Ping(pdedevice);
	NJB_Release(pdedevice);
	NJB_Close(pdedevice);
      }
    }
    device_connected = FALSE;
  }
  // Just to make sure
  jukebox_locked = FALSE;
}

/* Builds the list in memory that the dialog
 * drop down is then built from */
static void build_playlist_list(void)
{
  // g_print("Called build playlist list...\n");
  // Make sure we destroyed the old playlist list
  if (playlistlist != NULL)
    g_print("Playlistlist was not NULL!\n");

  // Destroy the playlist list used for choosing
  // during transfer.
  if (jukebox_playlist != NULL) {
    GList *tmplist = jukebox_playlist;

    // g_print("Destroy jukebox_playlist...\n");
    while(tmplist) {
      if (tmplist->data != NULL)
	g_free(tmplist->data);
      tmplist = tmplist->next;
      tmplist = tmplist->next;
    }
    g_list_free(jukebox_playlist);
    jukebox_playlist = NULL;
  }

  if (use_mtp) {
#ifdef HAVE_LIBMTP
    LIBMTP_playlist_t *playlists;

    // Get playlist listing.
    playlists = LIBMTP_Get_Playlist_List(mtpdevice);
    if (playlists != NULL) {
      LIBMTP_playlist_t *pl, *tmpl;

      pl = playlists;
      while (pl != NULL) {
	gnomadplaylist_entry_t *entry;
	gchar *tmp;
	gint i;

	// List of choices when transfering tracks
	jukebox_playlists++;
	jukebox_playlist = g_list_append(jukebox_playlist, g_strdup(pl->name));
	jukebox_playlist = g_list_append(jukebox_playlist, GUINT32_TO_GPOINTER((guint32) pl->playlist_id));

	// Widget playlist tree
	entry = (gnomadplaylist_entry_t *) g_malloc(sizeof(gnomadplaylist_entry_t));
	entry->name = NULL;
	entry->plid = 0;
	entry->tracklist = NULL;
	entry->name = g_strdup(pl->name);
	tmp = g_strdup_printf("%" PRIguint32, (guint32) pl->playlist_id);
	entry->plid = tmp;

	// Then build the tree structure immediately
	for (i = 0; i < pl->no_tracks; i++) {
	  entry->tracklist = g_slist_append(entry->tracklist,
					    GUINT32_TO_GPOINTER((guint32) pl->tracks[i]));
	}
	playlistlist = g_slist_append(playlistlist, (gpointer) entry);
	if (cancel_jukebox_operation)
	  break;
	tmpl = pl;
	pl = pl->next;
	LIBMTP_destroy_playlist_t(tmpl);
      }
    }
#endif
  } else {
    njb_playlist_t *playlist;

    NJB_Reset_Get_Playlist(pdedevice);
    while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
      gnomadplaylist_entry_t *entry;
      gchar *tmp;
      njb_playlist_track_t *track;

      jukebox_playlists++;
      jukebox_playlist = g_list_append(jukebox_playlist, g_strdup(playlist->name));
      jukebox_playlist = g_list_append(jukebox_playlist, GUINT32_TO_GPOINTER((guint32) playlist->plid));

      // Widget playlist tree
      entry = (gnomadplaylist_entry_t *) g_malloc(sizeof(gnomadplaylist_entry_t));
      entry->name = NULL;
      entry->plid = 0;
      entry->tracklist = NULL;
      entry->name = g_strdup(playlist->name);
      tmp = g_strdup_printf("%" PRIguint32, (guint32) playlist->plid);
      entry->plid = tmp;

      // Then build the tree structure immediately
      NJB_Playlist_Reset_Gettrack(playlist);
      while ((track = NJB_Playlist_Gettrack(playlist)) != NULL) {
	entry->tracklist = g_slist_append(entry->tracklist,
					  GUINT32_TO_GPOINTER((guint32) track->trackid));

      }
      playlistlist = g_slist_append(playlistlist, (gpointer) entry);
      if (cancel_jukebox_operation)
	break;

      // Dangerous?
      NJB_Playlist_Destroy(playlist);
    }
  }
}

static void build_tree_widget(void)
{
  GSList *tmplist;

  clear_list_store(PL_TREE);
  tmplist = playlistlist;
  // Draw the tree...
  while (tmplist != NULL) {
    gnomadplaylist_entry_t *entry;
    GSList *tmplist2;
    GtkTreeIter treeiter;

    entry = (gnomadplaylist_entry_t *) tmplist->data;
    // g_print("Playlist: %s, ID %lu\n", entry->name, entry->plid);
    gtk_tree_store_append (GTK_TREE_STORE(playlist_widgets.pltreestore), &treeiter, NULL);
    gtk_tree_store_set(GTK_TREE_STORE(playlist_widgets.pltreestore), &treeiter,
		       PLIST_PLAYLISTNAME_COLUMN, entry->name,
		       PLIST_PLID_COLUMN, entry->plid,
		       PLIST_SONGID_COLUMN, "0",
		       -1);

    tmplist2 = entry->tracklist;
    while (tmplist2 != NULL) {
      metadata_t *meta;
      GtkTreeIter treeiter2;

      // g_print("Track: %" PRIguint32 "\n", GPOINTER_TO_GUINT32(tmplist2->data));
      meta = (metadata_t *) g_hash_table_lookup(songhash, tmplist2->data);
      /*
       * This might have caused lots of trouble to people with bad tracks
       * in their playlists... Fixed 2004-03-09 Linus Walleij
       */
      if (meta != NULL) {
	// g_print("Adding song with ID %" PRIguint32 " to playlist %" PRIguint32 "\n",
	// GPOINTER_TO_GUINT32(tmplist2->data), entry->plid);
	// dump_metadata_t(meta);
	gtk_tree_store_append (GTK_TREE_STORE(playlist_widgets.pltreestore), &treeiter2, &treeiter);
	gtk_tree_store_set(GTK_TREE_STORE(playlist_widgets.pltreestore), &treeiter2,
			   PLIST_ARTIST_COLUMN, meta->artist,
			   PLIST_TITLE_COLUMN, meta->title,
			   PLIST_LENGTH_COLUMN, meta->length,
			   PLIST_PLID_COLUMN, entry->plid,
			   PLIST_SONGID_COLUMN, meta->path,
			   -1);
      } else {
	g_print("Bad track detected in playlist, track ID: %" PRIguint32 "\n", GPOINTER_TO_GUINT32(tmplist2->data));
      }
      tmplist2 = tmplist2->next;
    }
    tmplist = tmplist->next;
  }
}

static void destroy_tree_structure(void)
{
  /* This destroys the playlists in memory again */
  GSList *tmplist;

  tmplist = playlistlist;
  while (tmplist != NULL) {
    gnomadplaylist_entry_t *entry;

    entry = (gnomadplaylist_entry_t *) tmplist->data;
    g_free(entry->name);
    g_free(entry->plid);
    g_slist_free(entry->tracklist);
    tmplist = tmplist->next;
  }
  g_slist_free(playlistlist);
  playlistlist = NULL;
}


void build_playlist_tree(void)
{
  jukebox_locked = TRUE;
  /* First build the structure for dialog boxes */
  build_playlist_list();
  /* Now we are finished with the jukebox communication and we have
   * all playlists in memory, so let's enter the GTK thread and
   * redraw the tree. */
  build_tree_widget();
  /* Destroy the memory allocated by temporary playlist
   * tree structure */
  destroy_tree_structure();
  jukebox_locked = FALSE;
}

/*
 * This function is used to push the playlist tree building
 * down to the main loop.
 */
static gboolean call_build_playlist_tree(gpointer data)
{
  GtkTreeStore *pltreestore = (GtkTreeStore *) data;

  // g_print("Called call_build_playlist_tree\n");
  build_playlist_tree();
  // g_print("returned...\n");
  return FALSE;
}

/*
 * Simple helper to destroy a dialog from the main loop.
 */
static gboolean call_widget_destroy(gpointer data)
{
  GtkWidget *dialog = (GtkWidget *) data;

  gtk_widget_destroy(GTK_WIDGET(dialog));
  return FALSE;
}

/* Update the usage information */
static void flush_usage(void)
{
  u_int64_t totalbytes = 0;
  u_int64_t freebytes = 0;

  if (use_mtp) {
#ifdef HAVE_LIBMTP
    gint ret;

    ret = LIBMTP_Get_Storage(mtpdevice, 0);
    if (mtpdevice->storage != NULL) {
      totalbytes = mtpdevice->storage->MaxCapacity;
      freebytes = mtpdevice->storage->FreeSpaceInBytes;
    }
    if (ret != 0) {
      totalbytes = 0;
      freebytes = 0;
    }
#endif
  } else {
    if (NJB_Get_Disk_Usage (pdedevice, &totalbytes, &freebytes) == -1) {
      NJB_Error_Dump(pdedevice, stderr);
      return;
    }
  }

  jukebox_totalbytes = totalbytes;
  jukebox_freebytes = freebytes;
  jukebox_usedbytes = jukebox_totalbytes - jukebox_freebytes;
}

static void destroy_datafile_list(void)
{
  GSList *tmplist = datafilelist;
  metadata_t *meta;
  while (tmplist != NULL) {
    meta = (metadata_t *) tmplist->data;
    destroy_metadata_t(meta);
    tmplist = g_slist_next(tmplist);
  }
  g_slist_free(datafilelist);
  datafilelist = NULL;
}

static void remove_from_datafile_list(gchar *path)
{
  GSList *tmplist1 = datafilelist;
  GSList *tmplist2 = NULL;
  metadata_t *meta;

  while (tmplist1 != NULL) {
    meta = (metadata_t *) tmplist1->data;
    if (!strcmp(path, meta->path)) {
      // Found it...
      destroy_metadata_t(meta);
    } else {
      // Keep it...
      tmplist2 = g_slist_append(tmplist2, meta);
    }
    tmplist1 = g_slist_next(tmplist1);
  }
  g_slist_free(datafilelist);
  datafilelist = tmplist2;
}

#ifdef HAVE_LIBMTP

static void mtp_initialize_folders() {
    if (mtp_folders != NULL) {
	    LIBMTP_destroy_folder_t(mtp_folders);
    };
    mtp_folders = LIBMTP_Get_Folder_List(mtpdevice);
    mtp_last_folder_id=0;
    if (mtp_last_fullpath!=NULL) {
	    g_free(mtp_last_fullpath);
	    mtp_last_fullpath=NULL;
    };
};

static LIBMTP_folder_t * mtp_find_name_in_sibling(LIBMTP_folder_t * folder,gchar * name) {
	LIBMTP_folder_t * f=folder;
	int found=0;
	while ((f!=NULL)&&(!found)) {
		if (strcmp(f->name,name)==0) {
			found=1;
		} else {
			f=f->sibling;
		};
	};
	return f;
};

static gchar * mtp_find_fullpath(uint32_t folder_id) {
	if (mtp_last_fullpath!=NULL) {
		if (mtp_last_folder_id==folder_id) {
   			return mtp_last_fullpath;
		};
	};
	gchar * ret;
	if (folder_id == 0) {
		ret="\\";
	} else {
		LIBMTP_folder_t * folder=LIBMTP_Find_Folder(mtp_folders,folder_id);
		if (folder==NULL) {
			ret="";
		} else {
			gchar * tmp=mtp_find_fullpath(folder->parent_id);
			ret=g_strconcat(tmp,folder->name,"\\",NULL);
			};
	};
	mtp_last_folder_id=folder_id;
	if (mtp_last_fullpath!=NULL) {
		g_free(mtp_last_fullpath);
	};
	mtp_last_fullpath=g_strdup(ret);
	return ret;
};

static uint32_t  mtp_find_folder_id(gchar * filter){
	  if (strcmp(filter,"\\")==0) {
			  return 0;
	  };
  gchar ** tmp = g_strsplit(filter, "\\", 0);
  LIBMTP_folder_t * f=mtp_folders;
  LIBMTP_folder_t * fprev=f;
  int i;
  int n=vectorlength(tmp)-1;
  for (i=1; i<n;i++){
	  f=mtp_find_name_in_sibling(f,tmp[i]);
	  fprev=f;
	  f=f->child;
  };
  g_strfreev(tmp);
  return (fprev->folder_id);
  };

static LIBMTP_folder_t * mtp_find_first_subfolder(uint32_t ID){
	  if (ID==0) {
			  return mtp_folders;
	  } else {
		  LIBMTP_folder_t * f =
			  LIBMTP_Find_Folder(mtp_folders, ID);
		  return f->child;
	  };
  };

static GSList * mtp_add_subfolders(gchar * filter,GSList * filterlist) {
  mtp_current_filter_id=mtp_find_folder_id(filter);
  LIBMTP_folder_t * the_subfolder=mtp_find_first_subfolder(mtp_current_filter_id);
  metadata_t * meta;
  while (the_subfolder!=NULL) {
	  meta=new_metadata_t();
	  meta->filename = g_strdup(the_subfolder->name);
	  meta->size = 0;
          meta->path = g_strdup_printf("%" PRIguint32, (guint32) the_subfolder->folder_id);
	  meta->folder=g_strdup(mtp_find_fullpath(the_subfolder->folder_id));
	  filterlist = g_slist_append(filterlist, meta);
	  the_subfolder=the_subfolder->sibling;
  };
  return filterlist;
};

#endif

static GSList *filter_datafile_list(gchar *filter)
{
  GSList *tmplist = datafilelist;
  GSList *filterlist = NULL;
  metadata_t *meta;
  gchar **tmp;
  int veclen;

  if (filter == NULL) {
    return NULL;
  }
  /* Create a ".." directory */
  meta = new_metadata_t();
  meta->filename = g_strdup("..");
  meta->size = 0;
  tmp = g_strsplit(filter, "\\", 0);
  veclen = vectorlength(tmp);
  if (veclen > 2) {
    int i;
    gchar *newdir = NULL;

    for (i = 0; i < veclen - 2; i++) {
      newdir = stringcat(newdir, tmp[i]);
      newdir = stringcat(newdir, "\\");
    }
    meta->folder = newdir;
  } else {
    meta->folder = g_strdup(filter);
  }
  g_strfreev(tmp);

  filterlist = g_slist_append(filterlist, meta);
#ifdef HAVE_LIBMTP
  if (use_mtp) {
  filterlist = mtp_add_subfolders(filter,filterlist);
  }
#endif
  /* Then filter the list */
  while (tmplist != NULL) {
    meta = (metadata_t *) tmplist->data;
    if (meta->folder != NULL) {
      if (!strcmp(meta->folder, filter)
	  && meta->size != 0) {
	/* First just add all regular files belonging to this directory */
	filterlist = g_slist_append(filterlist, meta);
      } else if (meta->size == 0) {
	/* Then add any subdirectories */
	gchar **tmp1 = g_strsplit(meta->folder, "\\", 0);
	gchar **tmp2 = g_strsplit(filter, "\\", 0);
	int veclen1 = vectorlength(tmp1);
	int veclen2 = vectorlength(tmp2);
	int i;

	/* It must be exactly one more component in the folder than in the filter */
	if (veclen1 == veclen2+1) {
	  gboolean match = TRUE;

	  /* We don't match the last component of the filter, it's NULL, see. */
	  for (i = 0; i < veclen2-1; i++) {
	    if (tmp1[i] != NULL && tmp2[i] != NULL && strcmp(tmp1[i],tmp2[i])) {
	      match = FALSE;
	    }
	  }
	  if (match) {
	    /* If it's matching up to the filter length, add it */
	if (!use_mtp) { filterlist = g_slist_append(filterlist, meta); };
	  }
	}
	g_strfreev(tmp1);
	g_strfreev(tmp2);
      }
    } else if (!strcmp(filter, "\\")) {
      filterlist = g_slist_append(filterlist, meta);
    }
    tmplist = g_slist_next(tmplist);
  }
  return filterlist;
}

void rebuild_datafile_list(gchar *filter) {
  /*
   * Add everything to the model by filtering out the root
   * directory files only from the datafile list.
   */
  if (datafilelist != NULL) {
    GSList *filtered;

    /* Hack to get the datafile list store built in O(n) instead of O(n^2) */
    recreate_list_store(JBDATA_LIST);
    filtered = filter_datafile_list(filter);
    if (filtered != NULL) {
      GSList *tmplist = filtered;
      metadata_t *meta;

      while (tmplist != NULL) {
	meta = (metadata_t *) tmplist->data;
	add_metadata_to_model(meta, JBDATA_LIST);
	tmplist = g_slist_next(tmplist);
      }
      g_slist_free(tmplist);
    }
    view_and_sort_list_store(JBDATA_LIST);
  }
}

/*
 * These structs and functions are only here to be able to
 * push all drawing operations up to the main GTK (idle) loop.
 */

typedef struct {
  GtkWidget *label;
  gchar *text;
} draw_label_args_t;

typedef struct {
  GtkWidget *progress;
  gfloat fraction;
} draw_progress_args_t;

static gboolean draw_label(gpointer p) {
  draw_label_args_t *args = (draw_label_args_t *) p;

  if (args != NULL) {
    if (args->label != NULL && args->text != NULL) {
      gtk_label_set_text(GTK_LABEL(args->label), args->text);
    }
    if (args->text != NULL)
      g_free(args->text);
    g_free(args);
  }
  return FALSE;
}

static gboolean draw_progress(gpointer p) {
  draw_progress_args_t *args = (draw_progress_args_t *) p;

  if (args != NULL) {
    if (args->progress != NULL) {
      gtk_progress_bar_update(GTK_PROGRESS_BAR(args->progress), args->fraction);
    }
    g_free(args);
  }
  return FALSE;
}

static gboolean call_jblist_from_songhash(gpointer data)
{
  jblist_from_songhash();
  return FALSE;
}

static gboolean call_rebuild_datafile_list(gpointer data)
{
  rebuild_datafile_list("\\");
  return FALSE;
}

/**
 * Scanning thread - reads in the Jukebox track directory, the datafile directory
 * and all playlists, along with some general information about the jukebox.
 * All the GUI related stuff is conditional and will not be used on Windows
 * since Windows don't like that more than one thread tries to write to the
 * widgets (i.e. the Win32 widget set is simply not natively thread safe!)
 */
gpointer scan_thread(gpointer thread_args)
{
  gboolean last_item = FALSE;
  njb_songid_t *songtag = NULL;  /* initialize to shut up GCC */
  njb_datafile_t *datatag = NULL;  /* initialize to shut up GCC */
#ifdef HAVE_LIBMTP
  LIBMTP_track_t *mtptracks;
  LIBMTP_track_t *mtptrack = NULL;  /* initialize to shut up GCC */
  LIBMTP_file_t *mtpfiles;
  LIBMTP_file_t *mtpfile = NULL;  /* initialize to shut up GCC */
#endif
  draw_label_args_t *da;
  scan_thread_arg_t *args = (scan_thread_arg_t *) thread_args;

  /* Destroy old song hashes */
  destroy_hashes();
  jukebox_songs = 0;
  jukebox_datafiles = 0;
  jukebox_playlists = 0;

  /* Retrieve general information about the jukebox */
  da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
  da->label = args->label;
  if (get_prefs_extended_metadata()) {
    da->text = g_strdup(_("Scanning songs:\nYou have selected extended metadata scan,\n so scanning will be very slow."));
  } else {
    da->text = g_strdup(_("Scanning songs..."));
  }
  g_idle_add(draw_label, da);

  /* If it has already been read, free the old string */
  if (jukebox_ownerstring != NULL)
    g_free(jukebox_ownerstring);
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    jukebox_ownerstring = LIBMTP_Get_Friendlyname(mtpdevice);
#else
    jukebox_ownerstring = NULL;
#endif
  } else {
    jukebox_ownerstring = g_strdup(NJB_Get_Owner_String (pdedevice));
  }
  flush_usage();

  songhash = g_hash_table_new(NULL, NULL);
  if (!use_mtp) {
    // Activate extended metadata mode if desired
    // FIXME: Modify metadata editor to reflect this.
    if (get_prefs_extended_metadata()) {
      NJB_Get_Extended_Tags(pdedevice, 1);
    } else {
      NJB_Get_Extended_Tags(pdedevice, 0);
    }
  }

  /* Next retrieve the track listing */
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    mtptracks = LIBMTP_Get_Tracklisting_With_Callback(mtpdevice,NULL,NULL);
    mtptrack = mtptracks;
    if (mtptrack == NULL) {
      last_item = TRUE;
    }
    mtp_initialize_folders();
#else
    last_item = TRUE;
#endif
  } else {
    NJB_Reset_Get_Track_Tag(pdedevice);
    songtag = NJB_Get_Track_Tag(pdedevice);
    if (songtag == NULL) {
      last_item = TRUE;
    }
  }
  /*
   * FIXME/IMPROVEMENT: If jukebox_songs != 0 start displaying progress bar?
   * series 3 devices can actually report the number of songs. However it
   * is so fast anyway, so who'd want it? Most time is spent updating the
   * widget, not communicating with the device.
   */

  while (!last_item) {
    metadata_t *meta;

    jukebox_songs++;
    /* Number of songs scanned during scan. */
    da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
    da->label = args->label;
    da->text = g_strdup_printf(_("%" PRIguint32 " songs scanned"), jukebox_songs);
    g_idle_add(draw_label, da);

    // Create a structure to hold the data in the columns
    meta = new_metadata_t();

    if (use_mtp) {
#ifdef HAVE_LIBMTP
      LIBMTP_track_t *tmp;
      gchar *tmpyear;
      const mtp_filetype_description_t *filetype_d;

      // Retrieve filetype infos.
      filetype_d = get_mtp_filetype_description(mtptrack->filetype);

      // Add to metadata holder
      meta->path = g_strdup_printf("%" PRIguint32, (guint32) mtptrack->item_id);
      meta->artist = g_strdup(mtptrack->artist);
      meta->title = g_strdup(mtptrack->title);
      meta->genre = g_strdup(mtptrack->genre);
      meta->album = g_strdup(mtptrack->album);
      meta->length = seconds_to_mmss((guint) (mtptrack->duration/1000));
      meta->size = (guint32) mtptrack->filesize;
      meta->codec = g_strdup(filetype_d->codec);
      meta->trackno = (guint) mtptrack->tracknumber;
      // TODO: Complicated. Fix later.
      tmpyear = g_strdup(mtptrack->date);
      if (tmpyear != NULL && strlen(tmpyear) > 4) {
	// Terminate after the year
	tmpyear[4] = '\0';
	// Convert to a figure.
	meta->year = string_to_guint32(tmpyear);
      }
      if (tmpyear != NULL) {
	g_free(tmpyear);
      }
      meta->filename = g_strdup(mtptrack->filename);
      // These two are not supported yet.
      meta->folder = NULL;
      meta->protect = FALSE;

      tmp = mtptrack;
      mtptrack = mtptrack->next;
      LIBMTP_destroy_track_t(tmp);
      if (mtptrack == NULL) {
	last_item = TRUE;
      }
#else
      last_item = TRUE;
#endif
    } else {
      njb_songid_frame_t *frame;

      NJB_Songid_Reset_Getframe(songtag);
      meta->path = g_strdup_printf("%" PRIguint32, (guint32) songtag->trid);

      /* Loop through the song tags */
      while((frame = NJB_Songid_Getframe(songtag)) != NULL){
	// FIXME: add progress bar for scanning?

	if (!strcmp(frame->label, FR_ARTIST)) {
	  meta->artist = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_TITLE)) {
	  meta->title = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_ALBUM)) {
	  meta->album = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_GENRE)) {
	  meta->genre = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_LENGTH)) {
	  meta->length = seconds_to_mmss((guint) frame->data.u_int16_val);
	} else if (!strcmp(frame->label, FR_SIZE)) {
	  meta->size = frame->data.u_int32_val;
	} else if (!strcmp(frame->label, FR_CODEC)) {
	  // Uppercaseify it. Work in all GTK+ >= 2.0?
	  meta->codec = g_ascii_strup(frame->data.strval, -1);
	} else if (!strcmp(frame->label, FR_TRACK)) {
	  meta->trackno = (guint) frame->data.u_int16_val;
	} else if (!strcmp(frame->label, FR_YEAR)) {
	  meta->year = (guint) frame->data.u_int16_val;
	} else if (!strcmp(frame->label, FR_FNAME)) {
	  meta->filename = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_FOLDER)) {
	  meta->folder = g_strdup(frame->data.strval);
	} else if (!strcmp(frame->label, FR_PROTECTED)) {
	  meta->protect = TRUE;
	} else {
	  g_print("Unknown frame type %s\n", frame->label);
	}
      }
      NJB_Songid_Destroy(songtag);
      songtag = NJB_Get_Track_Tag(pdedevice);
      if (songtag == NULL) {
	last_item = TRUE;
      }
    }
    // Compensate for missing tag information
    if (!meta->artist)
      meta->artist = g_strdup("<Unknown>");
    if (!meta->title)
      meta->title = g_strdup("<Unknown>");
    if (!meta->album)
      meta->album = g_strdup("<Unknown>");
    if (!meta->genre)
      meta->genre = g_strdup("<Unknown>");
    if (!meta->length)
      meta->length = g_strdup("0:00");
    // Add to song hash
    if (gnomad_debug != 0) {
      g_print("Adding song \"%s\" to songhash...\n", meta->title);
      dump_metadata_t(meta);
    }
    g_hash_table_insert(songhash,
			GUINT32_TO_GPOINTER(string_to_guint32(meta->path)),
			(gpointer) meta);
    if (cancel_jukebox_operation)
      break;
  }

  /* Build songlist from hash */
  g_idle_add(call_jblist_from_songhash, NULL);

  da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
  da->label = args->label;
  da->text = g_strdup(_("Scanning datafiles..."));
  g_idle_add(draw_label, da);

  /* Next retrieve the datafile listing */
  last_item = FALSE;
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    mtpfiles = LIBMTP_Get_Filelisting_With_Callback(mtpdevice,NULL,NULL);
    mtp_initialize_folders();
    mtpfile = mtpfiles;
    if (mtpfile == NULL) {
      last_item = TRUE;
    }
#else
    last_item = TRUE;
#endif
  } else {
    NJB_Reset_Get_Datafile_Tag(pdedevice);
    datatag = NJB_Get_Datafile_Tag(pdedevice);
    if (datatag == NULL) {
      last_item = TRUE;
    }
  }

  /* Destroy the old datafile list */
  destroy_datafile_list();

  while (!last_item) {
    metadata_t *meta;
    u_int64_t filesize;

    jukebox_datafiles++;

    da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
    da->label = args->label;
    da->text = g_strdup_printf(_("%" PRIguint32 " data files scanned"), jukebox_datafiles);
    g_idle_add(draw_label, da);

    // Create a structure to hold the data in the columns
    meta = new_metadata_t();

    if (use_mtp) {
#ifdef HAVE_LIBMTP
      LIBMTP_file_t *tmp;
      // Add to metadata holder
      meta->path = g_strdup_printf("%" PRIguint32, (guint32) mtpfile->item_id);
      meta->size = (guint32) mtpfile->filesize;
      meta->filename = g_strdup(mtpfile->filename);
      // These two are not supported yet.
      meta->folder = g_strdup(mtp_find_fullpath(mtpfile->parent_id));
      meta->protect = FALSE;

      tmp = mtpfile;
      mtpfile = mtpfile->next;
      LIBMTP_destroy_file_t(tmp);
      if (mtpfile == NULL) {
	last_item = TRUE;
      }
#else
      last_item = TRUE;
#endif
    } else {
      /* Convert filesize from 64 bit unsigned integer value */
      filesize = (u_int64_t) datatag->filesize;
      /* FIXME: here we loos all the 64-bit quality, because meta->size is 32-bit ... */
      meta->size = (guint) filesize;
      meta->folder = g_strdup(datatag->folder);
      if (datatag->filesize == 0) {
	gchar **tmp = g_strsplit(datatag->folder, "\\", 0);
	gint veclen = vectorlength(tmp);
	if (veclen > 2) {
	  gchar *basename = tmp[veclen-2];
	  meta->filename = g_strdup(basename);
	} else {
	  /* This should not happen */
	  meta->filename = g_strdup("Erroneous folder");
	}
	g_strfreev(tmp);
      } else {
	meta->filename = g_strdup(datatag->filename);
      }
      /* File ID */
      meta->path = g_strdup_printf("%" PRIguint32, (guint32) datatag->dfid);
      // destroy_metadata_t(meta);
      NJB_Datafile_Destroy (datatag);
      datatag = NJB_Get_Datafile_Tag(pdedevice);
      if (datatag == NULL) {
	last_item = TRUE;
      }
    }

    datafilelist = g_slist_append(datafilelist, (gpointer) meta);
    if (cancel_jukebox_operation)
      break;
  }

  /* Then refill and sort everything */
  da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
  da->label = args->label;
  da->text = g_strdup(_("Scanning playlists..."));
  g_idle_add(draw_label, da);

  /*
   * Tell main loop to rebuild datafile list and playlist tree, then
   * close the dialog.
   */
  g_idle_add(call_rebuild_datafile_list, NULL);
  g_idle_add(call_build_playlist_tree, (gpointer) args->pltreestore);
  g_idle_add(call_widget_destroy, (gpointer) args->dialog);

  jukebox_locked = FALSE;
  return NULL;
}

static void set_progress_fraction(guint64 const sent, guint64 const total)
{
  draw_progress_args_t *dp;

  dp = (draw_progress_args_t *) g_malloc(sizeof(draw_progress_args_t));
  dp->progress = progress_bar;
  dp->fraction = ((gdouble) sent / (gdouble) total);
  g_idle_add(draw_progress, dp);
  // g_print("Sent: %llu, total: %llu, fraction: %f\n", sent, total, fraction);
}

/*
 * Progression callback for transfer both back and forth to the
 * jukebox
 */
static NJB_Xfer_Callback pde_progress;
static int pde_progress (u_int64_t sent, u_int64_t total, const char* buf, unsigned len, void *data)
{
  set_progress_fraction(sent, total);
  /* This cancels any jukebox operation */
  if (cancel_jukebox_operation)
    return -1;
  return 0;
}

/**
 * Same thing but for MTP.
 */
#ifdef HAVE_LIBMTP
//static const LIBMTP_progressfunc_t mtp_progress;
static int mtp_progress (uint64_t const sent, uint64_t const total,
			 void const * const data)
{
  set_progress_fraction(sent, total);
  if (cancel_jukebox_operation)
    return -1;
  return 0;
}
#endif

/*
 * These functions only serves to send different drawing
 * operations to the main loop.
 */

static gboolean call_fill_in_dir(gpointer what)
{
  fill_in_dir((listtype_t)GPOINTER_TO_INT(what), get_current_dir());
  return FALSE;
}

typedef struct {
  listtype_t listtype;
  metadata_t *meta;
} metadata_add_args_t;

static gboolean call_add_metadata_to_model(gpointer data)
{
  metadata_add_args_t *args = (metadata_add_args_t *) data;

  add_metadata_to_model(args->meta, args->listtype);
  destroy_metadata_t(args->meta);
  g_free(args);
  return FALSE;
}


/***********************************************************************************/
/* Transferring music from jukebox thread                                          */
/***********************************************************************************/

gpointer jb2hd_thread(gpointer thread_args)
{
  jb2hd_thread_arg_t *args = (jb2hd_thread_arg_t *) thread_args;
  GList *tmplist = args->metalist;

  while (tmplist &&
	 !cancel_jukebox_operation)
    {
      metadata_t *jbmeta;
      metadata_t *meta;
      u_int32_t id;
      gchar *filename;
      gchar *tmpfname;
      draw_label_args_t *da;
      draw_progress_args_t *dp;
      metadata_add_args_t *metaddarg;

      /* Get title from filelist */
      jbmeta = (metadata_t *) tmplist->data;
      filename = compose_filename(jbmeta);
#ifndef G_OS_WIN32
      tmpfname = filename_fromutf8(filename);
#else
      // Windows locale version of filename, since libnjb does not
      // convert filenames into widechar unicode...
      tmpfname = g_locale_from_utf8(filename,-1,NULL,NULL,NULL);
#endif
      // Check if file exists to avoid overwriting.
      // Add an underscore just before the extension if file exists.
      while (g_file_test(tmpfname, G_FILE_TEST_EXISTS)) {
	tmpfname = add_underscore(tmpfname);
      }

      create_dirs(tmpfname);
      id = string_to_guint32(jbmeta->path);

      // Draw next file
      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->label;
      da->text = g_strdup(tmpfname);
      g_idle_add(draw_label, da);
      dp = (draw_progress_args_t *) g_malloc(sizeof(draw_progress_args_t));
      dp->progress = progress_bar;
      dp->fraction = (gfloat) 0;
      g_idle_add(draw_progress, dp);

      /* g_print("Transferring %s...\n", file); */
      /* Transfer track */
      if (use_mtp) {
#ifdef HAVE_LIBMTP
	int ret;
	ret = LIBMTP_Get_Track_To_File(mtpdevice, id, tmpfname, mtp_progress, NULL);
	if (ret != 0) {
	  g_free(filename);
	  g_free(tmpfname);
	  g_print("Error getting file \"%s\" from MTP device!\n", tmpfname);
	  LIBMTP_Dump_Errorstack(mtpdevice);
	  LIBMTP_Clear_Errorstack(mtpdevice);
	  goto clean_up_and_return;
	}
#endif
      } else {
	if ( NJB_Get_Track(pdedevice, id, jbmeta->size, tmpfname, pde_progress, NULL) == -1 ) {
	  NJB_Error_Dump(pdedevice, stderr);
	  g_free(filename);
	  g_free(tmpfname);
	  goto clean_up_and_return;
	}
      }
      /* Copy all metadata, but replace jukebox ID with path */
      meta = clone_metadata_t(jbmeta);
      g_free(meta->path);
      meta->path = g_strdup(tmpfname);
      g_free(filename);
      /* Tag file unless already present */
      if (get_prefs_usetag()) {
	if (meta->codec != NULL) {
	  if (!strcmp(meta->codec, "MP3")) {
#if defined(HAVE_TAGLIB) && defined(TAGLIB_ONLY)
	    set_tag_for_file (meta, get_prefs_tagoverride());
#else
	    set_tag_for_mp3file (meta, get_prefs_tagoverride());
#endif
	  }
#ifdef HAVE_TAGLIB
	  else if (!strcmp(meta->codec, "OGG") ||
		   !strcmp(meta->codec, "FLAC")) {
	    set_tag_for_file (meta, get_prefs_tagoverride());
	  }
#endif
	}
      }

      /* Add to disk listbox and re-sort */
      g_free(tmpfname);

      /* Send to main loop for drawing */
      metaddarg = (metadata_add_args_t *) g_malloc(sizeof(metadata_add_args_t));
      metaddarg->listtype = HD_LIST;
      metaddarg->meta = meta;
      g_idle_add(call_add_metadata_to_model,metaddarg);

      tmplist = tmplist->next;
    }

 clean_up_and_return:

  jukebox_locked = FALSE;
  g_idle_add((GSourceFunc) gtk_widget_destroy, args->dialog);
  g_idle_add(call_fill_in_dir, GINT_TO_POINTER((gint)HD_LIST));
  /* Free the memory used by the list */
  destroy_metalist(args->metalist);
  g_free(args);
  return NULL;
}


/***********************************************************************************/
/* Transferring data files from jukebox thread                                     */
/***********************************************************************************/

gpointer jb2hd_data_thread(gpointer thread_args)
{
  jb2hd_data_thread_arg_t *args = (jb2hd_data_thread_arg_t *) thread_args;
  GList *metalist = args->metalist;

  while (metalist &&
	 !cancel_jukebox_operation)
    {
      metadata_t *jbmeta;
      u_int32_t id;
      gchar *tmpfname;
      draw_label_args_t *da;
      draw_progress_args_t *dp;
      metadata_add_args_t *metaddarg;

      jbmeta = (metadata_t *) metalist->data;
      id = string_to_guint32(jbmeta->path);

      // Draw next file
      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->label;
      da->text = g_strdup(jbmeta->filename);
      g_idle_add(draw_label, da);
      dp = (draw_progress_args_t *) g_malloc(sizeof(draw_progress_args_t));
      dp->progress = progress_bar;
      dp->fraction = (gfloat) 0;
      g_idle_add(draw_progress, dp);

      /* g_print("Transferring %s\n", jbmeta->filename); */
      /* Transfer file */
#ifndef G_OS_WIN32
      tmpfname = filename_fromutf8(jbmeta->filename);
#else
      // Windows locale version of filename, since libnjb does not
      // convert filenames into unicode...
      tmpfname = g_locale_from_utf8(jbmeta->filename,-1,NULL,NULL,NULL);
#endif
      if (use_mtp) {
#ifdef HAVE_LIBMTP
	int ret;
	ret = LIBMTP_Get_File_To_File(mtpdevice, id, tmpfname, mtp_progress, NULL);
	if (ret != 0) {
	  g_free(tmpfname);
	  g_print("Error getting file \"%s\" from MTP device!\n", tmpfname);
	  LIBMTP_Dump_Errorstack(mtpdevice);
	  LIBMTP_Clear_Errorstack(mtpdevice);
	  goto data_error;
	}
#endif
      } else {
	if (NJB_Get_File(pdedevice, id, jbmeta->size, tmpfname, pde_progress, NULL) == -1) {
	  g_free(tmpfname);
	  NJB_Error_Dump(pdedevice, stderr);
	  goto data_error;
	}
      }
      g_free(tmpfname);

      /* Send to main loop for drawing */
      metaddarg = (metadata_add_args_t *) g_malloc(sizeof(metadata_add_args_t));
      metaddarg->listtype = HDDATA_LIST;
      metaddarg->meta = jbmeta;
      g_idle_add(call_add_metadata_to_model,metaddarg);

      metalist = g_list_next(metalist);
    }

 data_error:

  destroy_metalist(args->metalist);
  jukebox_locked = FALSE;
  g_idle_add((GSourceFunc) gtk_widget_destroy, args->dialog);
  g_idle_add(call_fill_in_dir, GINT_TO_POINTER((gint)HDDATA_LIST));
  g_free(args);
  return NULL;
}


/***********************************************************************************/
/* Transferring music to jukebox thread                                            */
/***********************************************************************************/

gpointer hd2jb_thread(gpointer thread_args)
{
  hd2jb_thread_arg_t *args = (hd2jb_thread_arg_t *) thread_args;
  GList *templist = args->metalist;
  GList *new_metalist = NULL;
  gchar *tmpdirname = NULL;
#ifndef G_OS_WIN32
  gchar tmpdirtmpl[] = "/tmp/gnomad-XXXXXX";
#else
  // FIXME: I'm not quite sure this always works on Windows.
  gchar tmpdirtmpl[] = "C:\\gnomad-XXXXXX";
#endif

  // Create a temporary directory for tag-stripped files.
  if (get_prefs_tagremove()) {
	tmpdirname = mkdtemp(tmpdirtmpl);
    if (tmpdirname == NULL) {
      goto hd2jb_cleanup;
    }
  }

  while (templist &&
	 !cancel_jukebox_operation)
    {
      metadata_t *hdmeta;
      metadata_t *jbmeta;
      gint protectint;
      gchar *tmpfname = NULL;
      gchar *tmpfname8 = NULL;
      guint32 id;
      guint length;
      gboolean clone_and_strip = FALSE;
      draw_label_args_t *da;
      draw_progress_args_t *dp;
      metadata_add_args_t *metaddarg;

      hdmeta = (metadata_t *) templist->data;
      // g_print("Storing %s on jukebox...\n", hdmeta->filename);

      // If we want to remove tags then we fix that here
      if (tmpdirname != NULL) {
	clone_and_strip = TRUE;
	tmpfname8 = g_build_filename(tmpdirname, hdmeta->filename, NULL);
	tmpfname = filename_fromutf8(tmpfname8);
	/* Call the filesystem to clone the file and remove the tag */
	if (!clone_and_strip_tag(hdmeta->path, tmpfname8, hdmeta->codec)) {
	  goto hd2jb_cleanup;
	}
      } else {
	tmpfname = filename_fromutf8(hdmeta->path);
      }

      // Draw next file
      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->label;
      da->text = g_strdup_printf("%s - %s", hdmeta->artist, hdmeta->title);
      g_idle_add(draw_label, da);
      dp = (draw_progress_args_t *) g_malloc(sizeof(draw_progress_args_t));
      dp->progress = progress_bar;
      dp->fraction = (gfloat) 0;
      g_idle_add(draw_progress, dp);

      length = mmss_to_seconds(hdmeta->length);
      protectint = hdmeta->protect ? 1 : 0;

      if (use_mtp) {
#ifdef HAVE_LIBMTP
	LIBMTP_track_t *trackmeta = LIBMTP_new_track_t();
	int ret;
	const mtp_filetype_description_t *filetype_d;

	// Retrieve filetype infos
	filetype_d = get_mtp_filetype_description_by_codec(hdmeta->codec);

	// This will be filled in, dummy set to 0.
	trackmeta->item_id = 0;
	trackmeta->title = g_strdup(hdmeta->title);
	trackmeta->album = g_strdup(hdmeta->album);
	trackmeta->artist = g_strdup(hdmeta->artist);
	trackmeta->genre = g_strdup(hdmeta->genre);
	trackmeta->date = g_strdup_printf("%4d0101T0000.0", hdmeta->year);
	trackmeta->filename = g_strdup(hdmeta->filename);
	trackmeta->tracknumber = hdmeta->trackno;
	// This need to be in milliseconds
	trackmeta->duration = length * 1000;
	trackmeta->filesize = get_64bit_file_size(tmpfname);
	trackmeta->filetype = filetype_d->filetype;
	// TODO: Discarding return value as for now
	// g_print("Transferring MTP track...\n");
#ifdef HAVE_LIBMTP_030
	// Hardcode parent (folder) to 0 (== root directory)
	trackmeta->parent_id = 0;
	// Hardcode storage to primary
	trackmeta->storage_id = 0;
	ret = LIBMTP_Send_Track_From_File(mtpdevice, tmpfname, trackmeta,
					  mtp_progress, NULL);
#else
	ret = LIBMTP_Send_Track_From_File(mtpdevice, tmpfname, trackmeta,
					  mtp_progress, NULL, 0);
#endif
	if (ret != 0) {
	  g_print("Error sending file \"%s\" to MTP device!\n", tmpfname);
	  LIBMTP_Dump_Errorstack(mtpdevice);
	  LIBMTP_Clear_Errorstack(mtpdevice);
	  goto hd2jb_cleanup;
	}
	// Fetch new track ID.
	id = (guint32) trackmeta->item_id;
	LIBMTP_destroy_track_t(trackmeta);
#endif
      } else {
	njb_songid_t *songid;
	njb_songid_frame_t *frame;

	songid = NJB_Songid_New();
	frame = NJB_Songid_Frame_New_Codec(hdmeta->codec);
	NJB_Songid_Addframe(songid, frame);
	// libnjb will add this if not added by hand
	// frame = NJB_Songid_Frame_New_Filesize(filesize);
	// NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Title(hdmeta->title);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Album(hdmeta->album);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Artist(hdmeta->artist);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Genre(hdmeta->genre);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Year(hdmeta->year);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Tracknum(hdmeta->trackno);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Length((u_int32_t) length);
	NJB_Songid_Addframe(songid, frame);
	frame = NJB_Songid_Frame_New_Filename(hdmeta->filename);
	NJB_Songid_Addframe(songid, frame);
	if (hdmeta->protect) {
	  frame = NJB_Songid_Frame_New_Protected(1);
	  NJB_Songid_Addframe(songid, frame);
	}

	// g_print("Transferring track.\n");
	if (NJB_Send_Track (pdedevice, tmpfname, songid,
			    pde_progress, NULL,
			    (u_int32_t *) &id) == -1) {
	  NJB_Error_Dump(pdedevice, stderr);
	  goto hd2jb_cleanup;
	}
	// FIXME: do we need to destroy songid? Memleak?
	/* g_print("%s stored on jukebox with ID: %lu\n", file, id); */
      }

      jukebox_songs++;
      /* Add to disk listbox and re-sort */
      /* Add correct trackid in col 7, add the row */
      jbmeta = clone_metadata_t(hdmeta);
      g_free(jbmeta->path);
      jbmeta->path = g_strdup_printf("%" PRIguint32, id);

      /* Send to main loop for drawing */
      metaddarg = (metadata_add_args_t *) g_malloc(sizeof(metadata_add_args_t));
      metaddarg->listtype = JB_LIST;
      metaddarg->meta = clone_metadata_t(jbmeta);
      g_idle_add(call_add_metadata_to_model,metaddarg);

      g_hash_table_insert(songhash,
			  GUINT32_TO_GPOINTER(id),
			  (gpointer) clone_metadata_t(jbmeta));
      new_metalist = g_list_append (new_metalist, jbmeta);
      if (clone_and_strip) {
	/* Afterwards delete the tempfile */
	unlink(tmpfname);
	if (tmpfname8 != NULL) {
	  g_free(tmpfname8);
	}
      }
      if (tmpfname != NULL) {
	g_free(tmpfname);
      }
      templist = g_list_next(templist);
    }
  /*
   * At last add the tracks to default playlists if
   * any such are selected.
   */
  if (args->playlists != NULL) {
    add_tracks_to_playlists(args->playlists, new_metalist);
  }
  destroy_metalist(new_metalist);
  flush_usage();

 hd2jb_cleanup:

  // Remove temporary directory for tag-stripped files.
  if (tmpdirname != NULL) {
    rmdir(tmpdirname);
  }

  g_idle_add((GSourceFunc) gtk_widget_destroy, args->dialog);
  if (args->playlists != NULL) {
    g_idle_add(call_build_playlist_tree, (gpointer) args->pltreestore);
  }
  jukebox_locked = FALSE;
  g_list_free(args->playlists);
  destroy_metalist(args->metalist);
  g_free(args);
#ifdef HAVE_LIBMTP
  if (use_mtp) { mtp_initialize_folders(); }
#endif
  return NULL;
}


/***********************************************************************************/
/* Transferring data file to jukebox thread                                        */
/***********************************************************************************/

gpointer hd2jb_data_thread(gpointer thread_args)
{
  hd2jb_data_thread_arg_t *args = (hd2jb_data_thread_arg_t *) thread_args;
  GList *metalist = args->metalist;

  while (metalist &&
	 !cancel_jukebox_operation)
    {
      gchar *tmpfname;
      gchar *folder;
      gchar rootdir[] = "\\";
      guint32 id;
      metadata_t *hdmeta;
      metadata_t *jbmeta;
      draw_label_args_t *da;
      draw_progress_args_t *dp;
      metadata_add_args_t *metaddarg;

      hdmeta = (metadata_t *) metalist->data;
      // FIXME: is this getting operation really thread safe???
      folder = (gchar *) gtk_entry_get_text(GTK_ENTRY(data_widgets.jbentry));
      if (folder == NULL) {
	/* This shouldn't happen, but include it anyway. */
	folder = rootdir;
      }

      // Draw next file
      // g_print("Storing %s (extension %s) on jukebox as %s in folder %s...\n", hdmeta->path, hdmeta->codec, hdmeta->filename, folder);
      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->label;
      da->text = g_strdup(hdmeta->filename);
      g_idle_add(draw_label, da);
      dp = (draw_progress_args_t *) g_malloc(sizeof(draw_progress_args_t));
      dp->progress = progress_bar;
      dp->fraction = (gfloat) 0;
      g_idle_add(draw_progress, dp);

      tmpfname = filename_fromutf8(hdmeta->path);
      if (use_mtp) {
#ifdef HAVE_LIBMTP
	LIBMTP_file_t *filemeta = LIBMTP_new_file_t();
	const mtp_filetype_description_t *filetype_d;

	// Retrieve filetype infos.
	filetype_d = get_mtp_filetype_description_by_codec(hdmeta->codec);
	filemeta->filename = g_strdup(hdmeta->filename);
	filemeta->filesize = (uint64_t) hdmeta->size;
	filemeta->filetype = filetype_d->filetype;
#ifdef HAVE_LIBMTP_030
	filemeta->parent_id = mtp_current_filter_id;
	if (LIBMTP_Send_File_From_File(mtpdevice, tmpfname, filemeta, mtp_progress, NULL) != 0) {
#else
	if (LIBMTP_Send_File_From_File(mtpdevice, tmpfname, filemeta, mtp_progress, NULL, mtp_current_filter_id) != 0) {
#endif

	  LIBMTP_destroy_file_t(filemeta);
	  g_free(tmpfname);
	  g_print("Error sending file \"%s\" to MTP device!\n", tmpfname);
	  LIBMTP_Dump_Errorstack(mtpdevice);
	  LIBMTP_Clear_Errorstack(mtpdevice);
	  goto hd2jb_data_cleanup;
	}
	LIBMTP_destroy_file_t(filemeta);
#endif
      } else {
	if (NJB_Send_File (pdedevice, tmpfname, hdmeta->filename, folder, pde_progress, NULL, (u_int32_t *) &id) == -1) {
	  g_free(tmpfname);
	  NJB_Error_Dump(pdedevice, stderr);
	  goto hd2jb_data_cleanup;
	}
      }
      g_free(tmpfname);
      /* g_print("%s stored on jukebox with ID: %lu\n", hdmeta->filename, id); */
      jukebox_datafiles++;
      /* Add to disk listbox and re-sort */
      /* Add correct trackid in col 7, add the row
       * then restore the old contents so they will
       * be freed correctly */
      jbmeta = clone_metadata_t(hdmeta);
      g_free(jbmeta->path);
      jbmeta->path = g_strdup_printf("%" PRIguint32, id);
      if (jbmeta->folder != NULL) {
	g_free(jbmeta->folder);
      }
      jbmeta->folder = g_strdup(folder);

      /* Send to main loop for drawing */
      metaddarg = (metadata_add_args_t *) g_malloc(sizeof(metadata_add_args_t));
      metaddarg->listtype = JBDATA_LIST;
      metaddarg->meta = jbmeta;
      g_idle_add(call_add_metadata_to_model,metaddarg);

      /* Keep this in the global metadata file list! */
      datafilelist = g_slist_append(datafilelist, (gpointer) jbmeta);
      metalist = g_list_next(metalist);
    }
  flush_usage();

 hd2jb_data_cleanup:

  g_idle_add((GSourceFunc) gtk_widget_destroy, args->dialog);
  jukebox_locked = FALSE;
  destroy_metalist(args->metalist);
  g_free(args);
  return NULL;
}


#ifdef HAVE_LIBMTP
static gboolean delete_one_track_from_playlist(LIBMTP_playlist_t *pl, uint32_t trackid)
{
  guint i;
  guint instances = 0;

  // See if the track is on this playlist, remove it if it is
  for (i = 0; i < pl->no_tracks; i++) {
    if (pl->tracks[i] == trackid) {
      instances ++;
    }
  }

  // If there were instances of this track on this playlist,
  // remove them.
  if (instances != 0) {
    uint32_t *copy = g_malloc((pl->no_tracks - instances) * sizeof(uint32_t));
    guint j = 0;

    for (i = 0; i < pl->no_tracks; i++) {
      if (pl->tracks[i] != trackid) {
	copy[j] = pl->tracks[i];
	j++;
      }
    }
    g_free(pl->tracks);
    pl->tracks = copy;
    pl->no_tracks = pl->no_tracks - instances;
    if (LIBMTP_Update_Playlist(mtpdevice, pl) != 0) {
      g_printf("Failed to remove tracks from playlist during device update\n");
      return FALSE;
    }
  }
  return TRUE;
}
#endif // HAVE_LIBMTP


/* Called after file deletion to remove the deleted
 * tracks from any playlists they may occur in. UNTHREADED */
static void remove_tracks_from_playlists(GList *metalist,
					 GtkTreeStore *pltreestore)
{
  GList *tmplist;

  if (metalist == NULL) {
    return;
  }
  /* g_print("Called remove_tracks_from_playlists()\n"); */
  tmplist = metalist;

  // FIXME: this routine is O(n^2) due to two linear
  //        searches. Should be improved some day.
  while (tmplist) {
    metadata_t *meta;
    u_int32_t id;

    meta = (metadata_t *) tmplist->data;
    id = string_to_guint32(meta->path);

    if (use_mtp) {
#ifdef HAVE_LIBMTP
      LIBMTP_playlist_t *playlists;

      // Get playlist listing.
      playlists = LIBMTP_Get_Playlist_List(mtpdevice);
      if (playlists == NULL) {
	// No playlists == nothing to do
	return;
      } else {
	LIBMTP_playlist_t *pl, *tmp;
	pl = playlists;
	while (pl != NULL) {
	  if(!delete_one_track_from_playlist(pl, id)) {
	    g_print("Could not remove track %d from playlist %d\n", id, pl->playlist_id);
	  }
	  // Then iterate over next playlist
	  tmp = pl;
	  pl = pl->next;
	  LIBMTP_destroy_playlist_t(tmp);
	}
      }
#endif
    } else {
      njb_playlist_t *playlist;

      NJB_Reset_Get_Playlist(pdedevice);

      while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
	NJB_Playlist_Deltrack_TrackID(playlist, id);
	/* If the playlist changed, update it */
	if (playlist->_state == NJB_PL_CHTRACKS) {
	  if (NJB_Update_Playlist(pdedevice, playlist) == -1)
	    NJB_Error_Dump(pdedevice, stderr);
	}
	NJB_Playlist_Destroy(playlist);
      }
      if (NJB_Error_Pending(pdedevice)) {
	NJB_Error_Dump(pdedevice, stderr);
      }
    }
    tmplist = g_list_next(tmplist);
  }
  build_playlist_tree();
}

/* Delete a list of files from the jukebox */
void jukebox_delete_files(GList *metalist)
{
  GList *tmplist = metalist;

  if (!metalist)
    return;
  jukebox_locked = TRUE;
  while (tmplist) {
    guint32 id;
    metadata_t *meta;

    meta = (metadata_t *) tmplist->data;
    id = string_to_guint32(meta->path);
    /* g_print("Deleting: %" PRIguint32 " from jukebox library\n", id); */
    if (id) {
      if (use_mtp) {
#ifdef HAVE_LIBMTP
	if (LIBMTP_Delete_Object(mtpdevice, (uint32_t) id) != 0) {
	  // Error message
	  g_print("Unable to delete files.\n");
	}
#endif
      } else {
	if ( NJB_Delete_Datafile(pdedevice, (u_int32_t) id) == -1 ) {
	  NJB_Error_Dump(pdedevice, stderr);
	}
      }
      // Remove it from the datafile cache
      remove_from_datafile_list(meta->path);
      // Then decrease the counter
      if (jukebox_datafiles)
	jukebox_datafiles--;
    }
    tmplist = g_list_next(tmplist);
  }
#ifdef HAVE_LIBMTP
  if (use_mtp) { mtp_initialize_folders(); };
#endif
  flush_usage();
  jukebox_locked = FALSE;
}

/* Delete a list of tracks from the jukebox */
void jukebox_delete_tracks(GList *metalist,
			   GtkTreeStore *pltreestore)
{
  GList *tmplist = metalist;

  if (!metalist)
    return;
  jukebox_locked = TRUE;
  /* First remove tracks from any playlists they are in */
  remove_tracks_from_playlists(metalist, pltreestore);
  while (tmplist) {
    metadata_t *meta;
    u_int32_t id;

    meta = (metadata_t *) tmplist->data;
    id = string_to_guint32(meta->path);
    /* g_print("Deleting: %lu from jukebox library\n", id); */
    if (id) {
      metadata_t *tmpmeta;

      if (use_mtp) {
#ifdef HAVE_LIBMTP
	if (LIBMTP_Delete_Object(mtpdevice, id) != 0) {
	  g_print("Unable to delete tracks.\n");
	}
#endif
      } else {
	if ( NJB_Delete_Track(pdedevice, id) == -1 ) {
	  NJB_Error_Dump(pdedevice, stderr);
	}
      }
      /* Remove song from hash */
      tmpmeta = (metadata_t *) g_hash_table_lookup(songhash, GUINT_TO_POINTER(id));
      /* Sometimes tracks are removed that are not part of any playlist. Weird but happens. */
      if (tmpmeta != NULL) {
	destroy_hash(NULL, tmpmeta, NULL);
	g_hash_table_remove(songhash,
			    GUINT_TO_POINTER(id));
      }
      if (jukebox_songs)
	jukebox_songs--;
    }
    tmplist = tmplist->next;
  }
  flush_usage();
  jukebox_locked = FALSE;
}

gboolean jukebox_begin_metadata_set(void)
{
  /* Begin metadata transaction, return TRUE if transaction
   * is OK to begin */
  jukebox_locked = TRUE;
  return TRUE;
}

void jukebox_set_metadata (metadata_t *meta)
{
  /* Set metadata on the file with ID id */
  u_int32_t id, length;
  metadata_t *tmpmeta;

  id = string_to_guint32(meta->path);
  length = mmss_to_seconds(meta->length);
  if (id == 0) {
    create_error_dialog(_("Track ID was zero! Illegal value!"));
    return;
  }
  if (length < 0) {
    create_error_dialog(_("Song length must be greater than zero!"));
    return;
  }

  if (use_mtp) {
#ifdef HAVE_LIBMTP
    LIBMTP_track_t *trackmeta = LIBMTP_new_track_t();
    int ret;
    const mtp_filetype_description_t *filetype_d;

    // Retrieve filetype infos.
    filetype_d = get_mtp_filetype_description_by_codec(meta->codec);

    trackmeta->item_id = id;
    trackmeta->title = g_strdup(meta->title);
    trackmeta->album = g_strdup(meta->album);
    trackmeta->artist = g_strdup(meta->artist);
    trackmeta->genre = g_strdup(meta->genre);
    trackmeta->date = g_strdup_printf("%4d0101T0000.0", meta->year);
    // This cannot be set this way but send it anyway
    trackmeta->filename = g_strdup(meta->filename);
    trackmeta->tracknumber = meta->trackno;
    // This need to be in milliseconds
    trackmeta->duration = length * 1000;
    // This cannot be set this way but send it anyway
    trackmeta->filesize = (uint64_t) meta->size;
    trackmeta->filetype = filetype_d->filetype;
    // TODO: Discarding return value as for now
    ret = LIBMTP_Update_Track_Metadata(mtpdevice, trackmeta);
    LIBMTP_destroy_track_t(trackmeta);
#endif
  } else {
    njb_songid_t *songid;
    njb_songid_frame_t *frame;

    songid = NJB_Songid_New();
    /* On NJB1 incremental update is not possible! */
    if (pdedevice->device_type == NJB_DEVICE_NJB1) {
      // Sometimes it fails with old codec names.
      gchar *tmpcodec = g_ascii_strup(meta->codec, -1);

      frame = NJB_Songid_Frame_New_Codec(tmpcodec);
      g_free(tmpcodec);
      NJB_Songid_Addframe(songid, frame);
      frame = NJB_Songid_Frame_New_Filesize(meta->size);
      NJB_Songid_Addframe(songid, frame);
    }
    /* Ultimately only send altered fields to NJB series 3 */
    frame = NJB_Songid_Frame_New_Title(meta->title);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Album(meta->album);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Artist(meta->artist);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Genre(meta->genre);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Year(meta->year);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Tracknum(meta->trackno);
    NJB_Songid_Addframe(songid, frame);
    frame = NJB_Songid_Frame_New_Length(length);
    NJB_Songid_Addframe(songid, frame);
    /*
     * These will only be set if the jukebox has retrieved
     * extended metadata so that the user has a chance of modifying
     * an existing tag, not just overwrite what is already there.
     */
    if (get_prefs_extended_metadata()) {
      if (meta->filename != NULL) {
	frame = NJB_Songid_Frame_New_Filename(meta->filename);
	NJB_Songid_Addframe(songid, frame);
      }
      if (meta->folder != NULL) {
	frame = NJB_Songid_Frame_New_Folder(meta->folder);
	NJB_Songid_Addframe(songid, frame);
      }
    }
    if (meta->protect) {
      frame = NJB_Songid_Frame_New_Protected(1);
      NJB_Songid_Addframe(songid, frame);
    }

    if (NJB_Replace_Track_Tag(pdedevice, id, songid) == -1) {
      NJB_Error_Dump(pdedevice, stderr);
    }
  }

  /* Replace the data in the hash table */
  tmpmeta = (metadata_t *) g_hash_table_lookup(songhash,
					       GUINT_TO_POINTER(id));
  /* Make sure we actually find it! Sometimes it is non-existant... */
  if (tmpmeta != NULL) {
    destroy_hash(NULL, tmpmeta, NULL);
    g_hash_table_remove(songhash,
			GUINT_TO_POINTER(id));
  }
  /* Clone metadata */
  tmpmeta = clone_metadata_t(meta);
  /* Insert the new row */
  g_hash_table_insert(songhash,
		      GUINT_TO_POINTER(id),
		      (gpointer) tmpmeta);
}

void jukebox_end_metadata_set(void)
{
  /* End metadata transaction */
  jukebox_locked = FALSE;
  /* Update the window with the new metadata */
  jblist_from_songhash();
}

/* Creates a new playlist and returns its playlist ID UNTHREADED */
guint32 jukebox_create_playlist(gchar *plname, GtkTreeStore *pltreestore)
{
  njb_playlist_t *playlist;
  guint32 plid = 0;

  /* g_print("Called create playlist\n"); */
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    LIBMTP_playlist_t *pl;

    pl = LIBMTP_new_playlist_t();
    pl->name = g_strdup(plname);
    // This will create the new playlist in the default folder
#ifdef HAVE_LIBMTP_030
    pl->parent_id = 0;
    // Hardcode storage to primary
    pl->storage_id = 0;
    if (LIBMTP_Create_New_Playlist(mtpdevice, pl) != 0) {
#else
    if (LIBMTP_Create_New_Playlist(mtpdevice, pl, 0) != 0) {
#endif
      create_error_dialog(_("Could not create playlist"));
      return 0;
    }
    LIBMTP_destroy_playlist_t(pl);
#endif
  } else {
    /* Create the new playlist in memory */
    playlist = NJB_Playlist_New();
    if (playlist == NULL) {
      return 0;
    }
    if (NJB_Playlist_Set_Name(playlist, plname) == -1) {
      NJB_Error_Dump(pdedevice, stderr);
      return 0;
    }
    jukebox_locked = TRUE;
    /* g_print("Calling NJB_Update_Playlist\n"); */
    if (NJB_Update_Playlist(pdedevice, playlist) == -1) {
      create_error_dialog(_("Could not create playlist"));
      NJB_Error_Dump(pdedevice, stderr);
    }
    jukebox_playlists++;
    plid = (guint32) playlist->plid;
    NJB_Playlist_Destroy(playlist);
  }
  build_playlist_tree();
  jukebox_locked = FALSE;
  /* g_print("Created playlist\n"); */
  return plid;
}

/* Delete a playlist with playlist ID plid UNTHREADED */
void jukebox_delete_playlist(guint32 plid)
{
  jukebox_locked = TRUE;
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    if (LIBMTP_Delete_Object(mtpdevice, (uint32_t) plid) != 0) {
      create_error_dialog(_("Could not delete playlist"));
    }
#endif
  } else {
    if (NJB_Delete_Playlist(pdedevice, (u_int32_t) plid) == -1) {
      create_error_dialog(_("Could not delete playlist"));
      NJB_Error_Dump(pdedevice, stderr);
    }
  }
  jukebox_playlists--;
  build_playlist_list();
  jukebox_locked = FALSE;
}

/* Rename the playlist with ID plid */
void jukebox_rename_playlist(guint32 plid, gchar *name, GtkTreeStore *pltreestore)
{
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    // Get metadata for one single playlist, update and write back.
    LIBMTP_playlist_t *pl;

    pl = LIBMTP_Get_Playlist(mtpdevice, (uint32_t) plid);
    if (pl->name != NULL) {
      g_free(pl->name);
    }
    pl->name = g_strdup(name);
    if (LIBMTP_Update_Playlist(mtpdevice, pl) != 0) {
      create_error_dialog(_("Could not rename playlist"));
    }
    LIBMTP_destroy_playlist_t(pl);
#endif
  } else {
    njb_playlist_t *playlist;
    gboolean found = FALSE;

    jukebox_locked = TRUE;
    NJB_Reset_Get_Playlist(pdedevice);
    while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
      /* g_print("Playlist %" PRIguint32 " == Playlist %" PRIguint32 " ... ",playlist->plid, plid); */
      if (playlist->plid == (u_int32_t) plid) {
	/* g_print("Yes.\n"); */
	found = TRUE;
	break;
      }
      /* g_print("No.\n"); */
      NJB_Playlist_Destroy(playlist);
    }
    if (found) {
      if (NJB_Playlist_Set_Name(playlist, name) == -1) {
	NJB_Error_Dump(pdedevice, stderr);
      } else {
	if (NJB_Update_Playlist(pdedevice, playlist) == -1) {
	  create_error_dialog(_("Could not rename playlist"));
	  NJB_Error_Dump(pdedevice, stderr);
	}
      }
      NJB_Playlist_Destroy(playlist);
      build_playlist_tree();
    } else {
      create_error_dialog(_("Could not locate playlist to rename!"));
    }
    jukebox_locked = FALSE;
  }
}

static void add_tracks_to_single_playlist(guint32 playlistid, GList *metalist)
{
  // Sanity check
  if (!metalist)
    return;

  if (use_mtp) {
#ifdef HAVE_LIBMTP
    LIBMTP_playlist_t *pl;
    GList *tmplist;
    guint i;
    guint toadd;
    uint32_t *copy;

    if (gnomad_debug != 0) {
      g_print("Getting playlist %" PRIguint32 "\n", playlistid);
    }
    pl = LIBMTP_Get_Playlist(mtpdevice, (uint32_t) playlistid);
    if (pl == NULL) {
      return;
    }

    // Count the tracks and allocate space for playlist
    tmplist = metalist;
    toadd = 0;
    while(tmplist) {
      toadd++;
      tmplist = g_list_next(tmplist);
    }

    // Allocate the new playlist
    copy = g_malloc((pl->no_tracks + toadd) * sizeof(uint32_t));

    // Copy old tracks
    for (i = 0; i < pl->no_tracks; i++) {
      copy[i] = pl->tracks[i];
    }

    // Remove old contents
    if (pl->tracks != NULL) {
      g_free(pl->tracks);
    }
    pl->tracks = copy;
    pl->no_tracks = pl->no_tracks + toadd;

    // Add each new track
    tmplist = metalist;
    while(tmplist) {
      metadata_t *meta;
      guint32 id;

      meta = (metadata_t *) tmplist->data;
      id = string_to_guint32(meta->path);
      if (gnomad_debug != 0) {
	g_print("Adding track %" PRIguint32 " to playlist %" PRIguint32 "\n", id, playlistid);
      }
      pl->tracks[i] = (uint32_t) id;
      tmplist = g_list_next(tmplist);
      i++;
    }
    if (LIBMTP_Update_Playlist(mtpdevice, pl) != 0) {
      g_print("Failed to add tracks to playlist on MTP device.\n");
    }
#endif
  } else {
    njb_playlist_track_t *pl_track;
    njb_playlist_t *playlist;
    GList *tmplist = metalist;
    gboolean found = FALSE;

    NJB_Reset_Get_Playlist(pdedevice);
    while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
      /* g_print("Is it %lu?\n", playlist->plid */
      if (playlist->plid == playlistid) {
	/* g_print("YES!\n"); */
	found = TRUE;
	break;
      }
      /* g_print("NO.\n"); */
      NJB_Playlist_Destroy(playlist);
    }
    if (!found) {
      g_print(_("Could not find playlist"));
    } else if (tmplist) {
      while(tmplist) {
	metadata_t *meta;
	u_int32_t id;

	meta = (metadata_t *) tmplist->data;
	id = string_to_guint32(meta->path);
	/* g_print("Adding track %lu to playlist %lu\n", id, playlistid); */
	pl_track = NJB_Playlist_Track_New(id);
	NJB_Playlist_Addtrack(playlist, pl_track, NJB_PL_END);
	tmplist = g_list_next(tmplist);
      }
      if (NJB_Update_Playlist(pdedevice, playlist) == -1)
	NJB_Error_Dump(pdedevice, stderr);
      NJB_Playlist_Destroy(playlist);
    }
  }
}

/**
 * Add a list of tracks to a list of playlists.
 */
void add_tracks_to_playlists(GList *playlists, GList *metalist)
{
  GList *templist = g_list_first(playlists);

  if (gnomad_debug != 0) {
    g_print("Called add_tracks_to_playlist()\n");
  }
  jukebox_locked = TRUE;
  cancel_jukebox_operation = FALSE;
  while (templist != NULL) {
    guint32 plid = GPOINTER_TO_GUINT32(templist->data);
    // g_printf("Adding track to playlist 0x%08x\n", plid);
    add_tracks_to_single_playlist(plid, metalist);
    templist = g_list_next(templist);
  }
  jukebox_locked = FALSE;
}

/* Randomize file from playlist UNTHREADED */
guint jukebox_randomize_playlist(guint plist, GtkTreeStore *pltreestore)
{
  guint new_plid = plist;

  if (use_mtp) {
    // FIXME: implement for MTP, not yet done.
    return plist;
  }

  if (plist != 0) {
    njb_playlist_t *playlist;
    gboolean found = FALSE;

    jukebox_locked = TRUE;
    // g_print("randomizing playlist %lu\n", plist);
    NJB_Reset_Get_Playlist(pdedevice);
    while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
      // g_print("Is it %lu?\n", playlist->plid);
      if (playlist->plid == plist) {
	// g_print("YES!");
	found = TRUE;
	break;
      }
      // g_print("NO.");
      NJB_Playlist_Destroy(playlist);
    }
    if (found) {
      GPtrArray *contents = g_ptr_array_sized_new (playlist->ntracks);
      gpointer temp;
      njb_playlist_track_t *current = playlist->first;
      njb_playlist_track_t *prev = NULL;
      GRand* randnum = g_rand_new();
      gint i, j;
      // g_print(" playlist size is %u\n", playlist->ntracks);

      if (playlist->ntracks > 1)
	{
	  //g_print("Old order\n");
	  while(current != NULL)
	    {
	      //g_print("%lu\n", current->trackid);
	      g_ptr_array_add(contents, current);
	      current = current->next;
	    }

	  //For each item, swap with a randommly selected later item
	  for (i = 0; i < playlist->ntracks; ++i)
	    {
	      j = g_rand_int_range(randnum, i, playlist->ntracks);
	      //g_print("Swapping %lu [%i] with %lu [%i]\n",((njb_playlist_track_t *)contents->pdata[i])->trackid,i,((njb_playlist_track_t *)contents->pdata[j])->trackid,j);
	      temp = contents->pdata[i];
	      contents->pdata[i] = contents->pdata[j];
	      contents->pdata[j] = temp;
	    }

	  // Repair the chaining
	  // g_print("New order\n");
	  for (i = 0; i < playlist->ntracks; ++i)
	    {
	      current = (njb_playlist_track_t *)contents->pdata[i];
	      //g_print("%lu\n", current->trackid);
	      current->prev = prev;
	      if (prev != NULL)
		prev->next = current;
	      current->next = NULL;
	      prev = current;
	    }

	  playlist->first  = contents->pdata[0];
	  playlist->last   = prev;

	  // Mark it shuffled
	  playlist->_state = NJB_PL_CHTRACKS;
	  if (NJB_Update_Playlist(pdedevice, playlist) == -1)
	    NJB_Error_Dump(pdedevice, stderr);
	  new_plid = playlist->plid;
	} else {
	  g_print("Playlist is too small to shuffle\n");
	}
      // Clean up
      NJB_Playlist_Destroy(playlist);
      g_ptr_array_free(contents, FALSE);
      g_rand_free(randnum);
      // Then rebuild the playlist list (necessary).
      build_playlist_tree();
    }
  }
  jukebox_locked = FALSE;
  // g_print("Old playlist ID: %lu, new playlist ID: %lu\n", plist, new_plid);
  return new_plid;
}

/* Delete file from playlist UNTHREADED */
guint jukebox_delete_track_from_playlist(guint32 trackid, guint32 plist, GtkTreeStore *pltreestore)
{
  guint new_plid = plist;

  if (trackid != 0 && plist != 0) {


    jukebox_locked = TRUE;
    g_print("Removing track: %" PRIguint32 " from playlist %" PRIguint32 "\n", trackid, plist);

    if (use_mtp) {
#ifdef HAVE_LIBMTP
      LIBMTP_playlist_t *pl;

      pl = LIBMTP_Get_Playlist(mtpdevice, plist);
      if (pl == NULL) {
	g_print("Failed to locate playlist for track removal.\n");
      } else {
	if (!delete_one_track_from_playlist(pl, trackid)) {
	  g_print("Failed to remove track %" PRIguint32 " from playlist %" PRIguint32 "\n",
		  trackid, (guint32) pl->playlist_id);
	}
	LIBMTP_destroy_playlist_t(pl);
      }
#endif
    } else {
      gboolean found = FALSE;
      njb_playlist_t *playlist;
      NJB_Reset_Get_Playlist(pdedevice);
      while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
	// g_print("Is it in %lu? ...", playlist->plid);
	if (playlist->plid == plist) {
	  // g_print("YES!\n");
	  found = TRUE;
	  break;
	}
	// g_print("NO.\n");
	NJB_Playlist_Destroy(playlist);
      }
      /* If the playlist was found, remove the track */
      if (found) {
	njb_playlist_track_t *track;

	// g_print("Found playlist %lu\n", playlist->plid);
	while ((track = NJB_Playlist_Gettrack(playlist)) != NULL) {
	  if (trackid == track->trackid) {
	    /* When the track is located in a playlist, remove it */
	    // g_print("Removing track: %lu from playlist %lu\n", track->trackid, playlist->plid);

	    if (track->prev != NULL)
	      track->prev->next = track->next;
	    else
	      playlist->first = track->next;
	    if (track->next != NULL)
	      track->next->prev = track->prev;
	    NJB_Playlist_Track_Destroy(track);
	    playlist->ntracks--;
	    playlist->_state = NJB_PL_CHTRACKS;
	    break;
	  }
	}
	if (playlist->_state==NJB_PL_CHTRACKS) {
	  /* g_print("Called NJB_Update_Playlist...\n"); */
	  if (NJB_Update_Playlist(pdedevice, playlist) == -1)
	    NJB_Error_Dump(pdedevice, stderr);
	  new_plid = playlist->plid;
	}
	NJB_Playlist_Destroy(playlist);
      } else {
	create_error_dialog(_("Could not find the track in the playlist"));
      }
    }
  }
  jukebox_locked = FALSE;
  // g_print("Old playlist ID: %lu, new playlist ID: %lu\n", plist, new_plid);
  // When replacing playlist ID does not work we must use this
  // build_playlist_tree(pltreestore);
  return new_plid;
}

/* Retrieve ownerstring */
gchar *jukebox_get_ownerstring(void)
{
  return jukebox_ownerstring;
}

/* Set the ownerstring */
void jukebox_set_ownerstring(gchar *owner)
{
  jukebox_locked = TRUE;
  if (use_mtp) {
#ifdef HAVE_LIBMTP
    if (LIBMTP_Set_Friendlyname(mtpdevice, owner) != 0) {
      create_error_dialog(_("Could not set owner string on the device"));
    }
#endif
  } else {
    NJB_Set_Owner_String (pdedevice, owner);
  }
  if (jukebox_ownerstring != NULL)
    g_free(jukebox_ownerstring);
  jukebox_ownerstring = g_strdup(owner);
  /* The ownerstring max length is 64 bytes
   * including the null-terminator according
   * to the protocol specification */
  if (strlen(jukebox_ownerstring) >= OWNER_STRING_LENGTH)
    jukebox_ownerstring[OWNER_STRING_LENGTH-1] = '\0';
  jukebox_locked = FALSE;
}

/* Returns the hh:mm:ss representation from seconds */
static void hhmmss (u_int16_t seconds, u_int16_t *hh, u_int16_t *mm, u_int16_t *ss)
{
  if ( seconds >= 3600 ) {
    *hh= seconds/3600;
    seconds-= 3600*(*hh);
  } else
    *hh = 0;
  if ( seconds >= 60 ) {
    *mm= seconds/60;
    seconds-= 60*(*mm);
  } else
    *mm = 0;
  *ss= seconds;
}

/* Reset EAX retrieval routine */
void jukebox_reset_get_eax(void)
{
  jukebox_locked = TRUE;
  if (!use_mtp) {
    NJB_Reset_Get_EAX_Type(pdedevice);
  }
}


/* Gets an EAX setting struct, returns NULL if
 * something fails. */
njb_eax_t *jukebox_get_eax(void)
{
  if (!use_mtp) {
    njb_eax_t *eax = NJB_Get_EAX_Type(pdedevice);

    if (eax == NULL)
      jukebox_locked = FALSE;
    return eax;
  }
  return NULL;
}

/* Free the memory used by the EAX structure */
void jukebox_destroy_eax(njb_eax_t *eax)
{
  if (!use_mtp) {
    NJB_Destroy_EAX_Type(eax);
  }
}

void jukebox_adjust_eax(guint16 effect, guint16 patch, gint16 value)
{
  if (!use_mtp) {
    if (created_play_mutex) {
      g_mutex_lock(play_thread_mutex);
      NJB_Adjust_EAX(pdedevice, effect, patch, value);
      g_mutex_unlock(play_thread_mutex);
    }
  }
}

/* Returns the playlist in a format suiting for
 * the player thread below */
GList *jukebox_get_playlist_for_play(guint plid)
{
  GList *retlist = NULL;
  njb_playlist_t *playlist;
  gboolean found = FALSE;

  if (use_mtp) {
    // FIXME: not implemented for MTP
    return NULL;
  }

  jukebox_locked = TRUE;
  NJB_Reset_Get_Playlist(pdedevice);
  while ((playlist = NJB_Get_Playlist(pdedevice)) != NULL) {
    if (playlist->plid == plid) {
      found = TRUE;
      break;
    }
    // Dangerous?
    NJB_Playlist_Destroy(playlist);
  }
  /* Get the tracks from the playlist */
  if (found) {
    njb_playlist_track_t *track;
    metadata_t *meta;
    metadata_t *newmeta;

    while ((track = NJB_Playlist_Gettrack(playlist)) != NULL) {
      meta = (metadata_t *) g_hash_table_lookup(songhash,
						GUINT_TO_POINTER(track->trackid));
      // Sometimes it's non-existant
      if (meta != NULL) {
	newmeta = clone_metadata_t(meta);
	retlist = g_list_append(retlist, (gpointer) newmeta);
      }
    }
  }
  jukebox_locked = FALSE;  
  return retlist;
}

/* Transfer playlist to jukebox */
static void send_playlist(GList *list)
{
  gboolean first = TRUE;
  GList *tmplist = list;

  if (use_mtp) {
    // FIXME: not implemented for MTP.
    return;
  }
  
  if (tmplist != NULL)
    NJB_Stop_Play(pdedevice);
  while (tmplist) {
    metadata_t *meta;
    u_int32_t id;

    meta = (metadata_t *) tmplist->data;
    id = string_to_guint32(meta->path);
    if (id) {
      if (first) {
	/* g_print("Playing: %lu\n", id); */
	first = FALSE;
	if (NJB_Play_Track(pdedevice, id) == -1)
	  NJB_Error_Dump(pdedevice, stderr);
      } else {
	/* g_print("Queueing: %lu\n", id); */
	if (NJB_Queue_Track(pdedevice, id) == -1)
	  NJB_Error_Dump(pdedevice, stderr);
      }
    }
    tmplist = tmplist->next;
  }
}

/* Sets the songname in the dialog - you have to
 * lock mutexes before 
 * calling this function */
static void set_songname_label(gchar *artist, gchar *title, gchar *seconds)
{
  draw_label_args_t *da;

  da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
  da->label = songnamelabel;
  da->text = g_strdup_printf("%s - %s, %s", artist, title, seconds);
  g_idle_add(draw_label, da);
}

/**
 * Returns metadata for the currently playing track.
 */
metadata_t *jukebox_get_current_playing_metadata(void)
{
  metadata_t *retval = NULL;
  if (created_play_mutex) {
    g_mutex_lock(play_thread_mutex);
    if (playlistitr != NULL) {
      retval = (metadata_t *) playlistitr->data;
    }
    g_mutex_unlock(play_thread_mutex);
  }
  return retval;
}

/**
 * Skips forward/backward in song (position given
 * as milliseconds).
 */
void jukebox_skip_songposition(guint songpos)
{
  if (created_play_mutex) {
    g_mutex_lock(play_thread_mutex);
    NJB_Seek_Track(pdedevice, (u_int32_t) songpos);
    g_mutex_unlock(play_thread_mutex);
  } 
}

void jukebox_previous(void)
{
  if (created_play_mutex) {
    g_mutex_lock(play_thread_mutex);
    /* Twice, because each read advances three steps */
    if (playlistitr->prev != NULL) {
      metadata_t *meta;

      playlistitr = g_list_previous(playlistitr);
      meta = (metadata_t *) playlistitr->data;
      set_songname_label(meta->artist, meta->title, meta->length);
    }
    send_playlist(playlistitr);
    passed_first_zero = FALSE;
    g_mutex_unlock(play_thread_mutex);
  }
}

/**
 * The parameter tells if the next song is already
 * playing, so that the list does not need to be
 * resent.
 */
void jukebox_next(gboolean already_playing)
{
  if (created_play_mutex) {
    g_mutex_lock(play_thread_mutex);
    if (playlistitr != playlistlast) {
      playlistitr = g_list_next(playlistitr);
      if (playlistitr != NULL) {
	metadata_t *meta;

	meta = (metadata_t *) playlistitr->data;
	set_songname_label(meta->artist, meta->title, meta->length);
	if (!already_playing) {
	  send_playlist(playlistitr);
	  passed_first_zero = FALSE;
	}
      }
    }
    g_mutex_unlock(play_thread_mutex);
  }
}

static void jukebox_current(void)
{
  /* g_print("Called jukebox_current...\n"); */
  if (playlistitr != NULL) {
    metadata_t *meta;
    
    meta = (metadata_t *) playlistitr->data;
    set_songname_label(meta->artist, meta->title, meta->length);
    send_playlist(playlistitr);
  }
}

typedef struct {
  GtkAdjustment *adjustment;
  gdouble position;
} set_adjustment_args_t;

static gboolean call_set_adjustment_value(gpointer data)
{
  set_adjustment_args_t *sa = (set_adjustment_args_t *) data;

  gtk_adjustment_set_value(sa->adjustment, sa->position);
  g_free(sa);
  return FALSE;
}

/* Start playing a bunch of selected files */
gpointer play_thread(gpointer thread_args)
{
  play_thread_arg_t *args = (play_thread_arg_t *) thread_args;
  u_int16_t sec, hh, mm, ss;
  gint change = 0;
  gboolean repeat = TRUE;
  gfloat position;

  /* Just mail out on MTP devices */
  if (use_mtp) {
    return NULL;
  }
  /* This mutex is to avoid collisions with EAX changes */
  if (!created_play_mutex) {
    g_assert (play_thread_mutex == NULL);
    play_thread_mutex = g_mutex_new();
    created_play_mutex = TRUE;
  }
  jukebox_locked = TRUE;

  /* Lock playing mutex and initialize */
  g_mutex_lock(play_thread_mutex);
  playlist = args->metalist;
  songnamelabel = args->songlabel;
  passed_first_zero = FALSE;
  if (!playlist) {
    g_mutex_unlock(play_thread_mutex);
    return NULL;
  }
  /* The iterator is used for walking the playlist */
  playlistitr = playlist;
  /* Set playlistlast to the pointer of the last song */
  playlistlast = g_list_last(playlist);
  /* Setup for the first round */
  jukebox_current();

  g_mutex_unlock(play_thread_mutex);

  while (!cancel_jukebox_operation &&
	 repeat) {
    if (change) {
      if (playlistitr == NULL) {
	/* It would be nice if the jukebox sent a signal
	 * like this at the end of playing all tracks,
	 * but unfortunately it doesn't so this clause
	 * is not executed on any device I've seen. */
	repeat = FALSE;
      } else {
	jukebox_next(TRUE);
      }
      change = 0;
    } else {
      gchar tmp[10];
      metadata_t *meta;
      guint seconds;
      draw_label_args_t *da;
      set_adjustment_args_t *sa;

      g_mutex_lock(play_thread_mutex);
      NJB_Elapsed_Time(pdedevice, &sec, &change);
      g_mutex_unlock(play_thread_mutex);
      hhmmss(sec, &hh, &mm, &ss);
      sprintf(tmp, "%02u:%02u:%02u", hh, mm, ss);
      meta = (metadata_t *) playlistitr->data;
      //dump_metadata_t(meta);
      seconds = mmss_to_seconds(meta->length);
      position = (gfloat) 100 * ((gfloat) sec / (gfloat) seconds);
      /* This is a guard against things that may happen if
       * you have MP3s with variable bitrate -- the second
       * tag may be incorrect */
      if (position < 0)
	position = 100.0;

      da = (draw_label_args_t *) g_malloc(sizeof(draw_label_args_t));
      da->label = args->timelabel;
      da->text = g_strdup(tmp);
      g_idle_add(draw_label, da);

      sa = (set_adjustment_args_t *) g_malloc(sizeof(set_adjustment_args_t));
      sa->adjustment = args->adj;
      sa->position = position;
      g_idle_add(call_set_adjustment_value, sa);
    }
    /* The Jukebox reports zero seconds of time at the
     * beginning of play, and after the song ends. This
     * detects it and uses the situation after the last
     * song ends to kill the player window */
    g_mutex_lock(play_thread_mutex);
    if (playlistitr == playlistlast &&
	sec > 0)
      passed_first_zero = TRUE;
    if (passed_first_zero &&
	sec == 0 &&
	playlistitr == playlistlast) {
      /* g_print("End of last track\n"); */
      g_mutex_unlock(play_thread_mutex);
      break;
    }
    g_mutex_unlock(play_thread_mutex);
    /* Sleep for a second */
    sleep(1);
  }

  // Let the main loop destroy the dialog
  g_idle_add(call_widget_destroy, args->dialog);
  g_mutex_lock(play_thread_mutex);
  // Free the playlist
  destroy_metalist(playlist);
  playlist = NULL;
  NJB_Stop_Play(pdedevice);
  g_mutex_unlock(play_thread_mutex);
  jukebox_locked = FALSE;
  return NULL;
}

/* This routine synchronize the time on the
 * jukebox to that of the host system
 */
void jukebox_synchronize_time(void)
{
  GTimeVal currenttimeval;
  GTime currenttime;

  g_get_current_time(&currenttimeval);
  currenttime = (GTime) currenttimeval.tv_sec;

  if (use_mtp) {
    // FIXME: implement for MTP if device can set time.
  } else {
    njb_time_t *jukeboxtime;
    struct tm *tm;
    const time_t tt_currenttime = currenttime;

    // Get the time from the jukebox, then modify it.
    jukeboxtime = NJB_Get_Time(pdedevice);

    // Uses UNIX library calls, might need to be portablized
    tm = localtime(&tt_currenttime);
    /*
      g_print("Setting: %d-%d-%d (wkd: %d) %d:%d:%d\n", 1900+tm->tm_year,
      1+tm->tm_mon, tm->tm_mday, tm->tm_wday-1,
      tm->tm_hour, tm->tm_min, tm->tm_sec);
    */
    jukeboxtime->year = 1900+tm->tm_year;
    jukeboxtime->month = 1+tm->tm_mon;
    jukeboxtime->day = tm->tm_mday;
    jukeboxtime->weekday = tm->tm_wday-1;
    jukeboxtime->hours = tm->tm_hour;
    jukeboxtime->minutes = tm->tm_min;
    jukeboxtime->seconds = tm->tm_sec;
    
    NJB_Set_Time(pdedevice, jukeboxtime);
    NJB_Destroy_Time(jukeboxtime);
  }
}

/* 
 * This creates a new folder on the device
 */
void jukebox_create_folder(gchar *foldername)
{
  gchar rootdir[] = "\\";
  guint32 id;
  metadata_t *jbmeta;
  gchar *folder;
  
  jbmeta = new_metadata_t();

  folder = (gchar *) gtk_entry_get_text(GTK_ENTRY(data_widgets.jbentry));
  if (folder == NULL) {
    /* This shouldn't happen, but include it anyway. */
    folder = rootdir;
  }
  // Add the new folder name
  jbmeta->folder = g_strdup_printf("%s%s\\", folder, foldername);
  jbmeta->filename = g_strdup(foldername);
  jbmeta->size = 0;

#ifdef HAVE_LIBMTP
  if (use_mtp) {
#ifdef HAVE_LIBMTP_030
    int ret=LIBMTP_Create_Folder(mtpdevice,foldername,mtp_current_filter_id,0);
#else
    int ret=LIBMTP_Create_Folder(mtpdevice,foldername,mtp_current_filter_id);
#endif
    mtp_initialize_folders();
    rebuild_datafile_list(folder);
    return;
  }
#endif

  if (NJB_Create_Folder (pdedevice, jbmeta->folder, (u_int32_t *) &id) == -1) {
    // Error back off
    destroy_metadata_t(jbmeta);
    NJB_Error_Dump(pdedevice, stderr);
    create_error_dialog(_("The folder could not be created.\nThe most typical reason is that you are using a Nomad Jukebox 1\nwhich is too old to support folders."));
    return;
  }
  jbmeta->path = g_strdup_printf("%" PRIguint32, id);
  /* Keep this in the global metadata file list! */
  datafilelist = g_slist_append(datafilelist, (gpointer) jbmeta);
  // Redraw
  rebuild_datafile_list(folder);
}
