/* xfer.c
   Transfer window widgets and callbacks
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you xcan redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "metadata.h"
#include "editmeta.h"
#include "filesystem.h"
#include "filenaming.h"
#include "prefs.h"
#include "jukebox.h"
#include "playlists.h"
#include "util.h"
#include "player.h"
#include "xfer.h"

/* Local variables */
static GtkWidget *new_folder_dialog;
static GtkWidget *new_folder_entry;

/*****************************************************************************
 * Here comes the jukebox transfer functions.
 *****************************************************************************/

/* Move the files off the Jukebox and into the harddisk directory */
static void transfer_from_jukebox_to_hd (GtkButton *button,
		      gpointer data)
{
  jb2hd_thread_arg_t *jb2hd_thread_args;
  GList *metalist = get_all_metadata_from_selection(JB_LIST);

  jb2hd_thread_args = (jb2hd_thread_arg_t *) malloc(sizeof (jb2hd_thread_arg_t));

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    destroy_metalist(metalist);
    return;
  }

  if (metalist != NULL) {
    GtkWidget *label1, *label2, *dialog, *button, *separator;

    dialog=gtk_dialog_new_with_buttons(_("Transferring tracks from jukebox library"),
		    NULL,
		    0,
		    NULL);
    button = gtk_dialog_add_button(GTK_DIALOG(dialog),
		    GTK_STOCK_CANCEL,
		    0);
    g_signal_connect(button,
		    "clicked",
		    G_CALLBACK(cancel_jukebox_operation_click),
		    NULL);
    g_signal_connect_object(GTK_OBJECT(dialog),
			    "delete_event",
			    G_CALLBACK(gtk_widget_destroy),
			    GTK_OBJECT(dialog),
			    0);
    gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
    label1 = gtk_label_new(_("Retrieving tracks from Jukebox..."));
    label2 = gtk_label_new("");
    progress_bar = gtk_progress_bar_new();
    separator = gtk_hseparator_new ();
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), separator, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), progress_bar, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog);

    jb2hd_thread_args->dialog = dialog;
    jb2hd_thread_args->label = label2;
    jb2hd_thread_args->metalist = metalist;
    jukebox_locked = TRUE;
    cancel_jukebox_operation = FALSE;
    g_thread_create(jb2hd_thread,(gpointer) jb2hd_thread_args, FALSE, NULL);
  }
}

/* Create a list of the files to be transferred */
static GList *get_files_to_xfer(GList *metalist, GList *appendum) {
  GList *newlist = appendum;
  GList *templist = g_list_first(metalist);

  while (templist != NULL) {

    metadata_t *meta = (metadata_t *) templist->data;
    gchar *file = filename_fromutf8(meta->path);

    if(is_directory(file)) {
      /* Do not recurse into . and .. */
      if(get_prefs_recurse_dir()
	 && strcmp(meta->artist,"..")
	 && strcmp(meta->artist,".") ) {
	GList *inner_metalist;

	/* g_print("Recursing directory...\n"); */
	/* Get metalist for directory */
	inner_metalist = get_metadata_dir(HD_LIST, meta->path, NULL);
	/* Call myself */
	newlist = get_files_to_xfer(inner_metalist, newlist);
	/* Free the metalist and temppath */
	destroy_metalist(inner_metalist);
      }
    } else {
      newlist = g_list_append(newlist, clone_metadata_t(meta));
    }
    g_free(file);
    templist = templist->next;
  }
  return newlist;
}


/* Move the files off the Harddisk and into the Jukebox */
void transfer_from_hd_to_jukebox(GList *playlists)
{
  GList *metalist = get_all_metadata_from_selection(HD_LIST);
  GList *xferlist;

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    destroy_metalist(metalist);
    return;
  }

  if (metalist != NULL) {
    hd2jb_thread_arg_t *hd2jb_thread_args;
    GtkWidget *label1, *label2, *dialog, *button, *separator;

    hd2jb_thread_args = (hd2jb_thread_arg_t *) malloc(sizeof (hd2jb_thread_arg_t));
     dialog=gtk_dialog_new_with_buttons(_("Transferring tracks to jukebox library"),
		    NULL,
		    0,
		    NULL);
    button = gtk_dialog_add_button(GTK_DIALOG(dialog),
		    GTK_STOCK_CANCEL,
		    0);
    g_signal_connect(button,
		    "clicked",
		    G_CALLBACK(cancel_jukebox_operation_click),
		    NULL);
    g_signal_connect_object(GTK_OBJECT(dialog),
			    "delete_event",
			    G_CALLBACK(gtk_widget_destroy),
			    GTK_OBJECT(dialog),
			    0);
    gtk_window_set_title (GTK_WINDOW (dialog), _("Transferring tracks to jukebox library"));
    gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
    label1 = gtk_label_new(_("Storing tracks in Jukebox library..."));
    label2 = gtk_label_new("");   /* FIXME: Instead add a list to the dialog so progress may be seen better */
    progress_bar = gtk_progress_bar_new();
    separator = gtk_hseparator_new ();
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), separator, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), progress_bar, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog);

    hd2jb_thread_args->dialog = dialog;
    hd2jb_thread_args->label = label2;
    xferlist = get_files_to_xfer(metalist, NULL);
    destroy_metalist(metalist);

    hd2jb_thread_args->metalist = xferlist;
    hd2jb_thread_args->pltreestore = playlist_widgets.pltreestore;
    hd2jb_thread_args->playlists = playlists;

    /* Dumps the xfer list without transferring anything - for debugging */
    /*
    {
      GList *templist = g_list_first(xferlist);

      g_print("Transferring...\n");
      while (templist) {
        metadata_t *meta = (metadata_t *) templist->data;
        g_print("To transfer: %s\n", meta->path);
        templist = g_list_next(templist);
      }
      destroy_metalist(xferlist);
      gtk_widget_destroy(dialog);
    }
    */
    jukebox_locked = TRUE;
    cancel_jukebox_operation = FALSE;
    g_thread_create(hd2jb_thread,(gpointer) hd2jb_thread_args, FALSE, NULL);
  }
}

/*****************************************************************************
 * Other tool functions for popup choices etc.
 *****************************************************************************/

#define MAX_HISTORY 10

static void add_to_history(gchar *string)
{
  /* See if the string is already in the history */
  GList *history = transfer_widgets.history;
  while (history) {
    if (!strcmp(history->data, string))
      return;
    history = g_list_next(history);
  }
  history = transfer_widgets.history;
  /* Else add it */
  transfer_widgets.history
    = g_list_prepend (transfer_widgets.history, string);
  /* Do not remember more than MAX_HISTORY strings */
  while (g_list_length(history) > MAX_HISTORY) {
      GList *elem;

      elem = g_list_last(history);
      transfer_widgets.history
	= g_list_remove(history, elem->data);
  }
}

static void set_popup_history(GtkWidget *combo)
{
  GList *history = transfer_widgets.history;

#if GTK_CHECK_VERSION(3,0,0)
  // Use the new combobox abstractions in 3.0 and later
  gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(combo));
  while (history) {
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), history->data);
    history = g_list_next(history);
  }
#else
  // Dead ugly code for older GTK
  static gboolean firstrun = TRUE;
  int i;

  // Blank out all 5 entries
  if (!firstrun) {
    for (i = 0; i < MAX_HISTORY; i++) {
      gtk_combo_box_remove_text(GTK_COMBO_BOX(combo), 0);
    }
  } else {
    firstrun = FALSE;
  }

  for (i = 0; i < MAX_HISTORY; i++) {
    if (history) {
      gtk_combo_box_append_text(GTK_COMBO_BOX(combo), history->data);
      history = g_list_next(history);
    } else {
      gtk_combo_box_append_text(GTK_COMBO_BOX(combo), "");
    }
    i++;
  }
#endif
}


/* Response to the "rescan" call from the harddisk popup menu */
static GCallback hdmenu_rescan_response(gpointer data)
{
  fill_in_dir(HD_LIST, get_current_dir());
  return NULL;
}


/* Response to the "transfer" call from the harddisk popup menu */
static GCallback hdmenu_transfer_response(gpointer data)
{
  transfer_from_hd_dialog(NULL, NULL);
  return NULL;
}


/* Response to the "delete" call from the harddisk popup menu */
static GCallback hdmenu_delete_response(gpointer data)
{
  if (request_confirmation_dialog(_("Really delete selected files/folders?"))) {
    GList *metalist = get_all_metadata_from_selection(HD_LIST);

    if (!delete_files(metalist)) {
      remove_selected(HD_LIST); // This or fill in directory? This is faster.
    }
    else fill_in_dir(HD_LIST, get_current_dir());
    destroy_metalist(metalist);
  }
  return NULL;
}

static GCallback create_new_folder_ok_button (GtkButton *button,
					      gpointer data)
{
  gchar *foldername;
  gchar *current;
  gchar *fullpath;
  current = get_current_dir();
  foldername = g_strdup(gtk_entry_get_text(GTK_ENTRY(new_folder_entry)));
  gtk_widget_destroy(new_folder_dialog);

  fullpath = g_strconcat(current,"/",foldername,NULL);

#if !GTK_CHECK_VERSION(2,8,0)
  if (g_mkdir(fullpath,0755)) {
	  create_error_dialog(_("Could not create folder"));
  }
#else
  if (g_mkdir_with_parents(fullpath,0755)) {
	  create_error_dialog(_("Could not create folder"));
  }
#endif

  fill_in_dir(HD_LIST, get_current_dir());

  g_free(fullpath);
  g_free(current);
  g_free(foldername);
  return NULL;
}

/* Response to the "new folder..." call from the harddisk popup menu */
static GCallback hdmenu_new_folder_response(gpointer data)
{
  GtkWidget *label, *button;

  new_folder_dialog=gtk_dialog_new_with_buttons(_("New folder"),
					    NULL,
					    0,
					    NULL);

  button = gtk_dialog_add_button(GTK_DIALOG(new_folder_dialog),
				 GTK_STOCK_CANCEL,
				 0);
  g_signal_connect(button,
		   "clicked",
		   G_CALLBACK(dispose_of_dialog_window),
		   GTK_OBJECT(new_folder_dialog));

  button = gtk_dialog_add_button(GTK_DIALOG(new_folder_dialog),
				 GTK_STOCK_OK,
				 1);
  g_signal_connect(button,
		   "clicked",
		   G_CALLBACK(create_new_folder_ok_button),
		   NULL);

  gtk_window_set_position (GTK_WINDOW(new_folder_dialog), GTK_WIN_POS_MOUSE);

  label = gtk_label_new(_("Enter a name for the folder:"));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_folder_dialog)->vbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  new_folder_entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_folder_dialog)->vbox), new_folder_entry, TRUE, TRUE, 0);
  gtk_widget_show(new_folder_entry);

  g_signal_connect(G_OBJECT (new_folder_dialog),
		   "delete_event",
		   G_CALLBACK(gtk_widget_destroy),
		   G_OBJECT (new_folder_dialog));

  gtk_widget_show_all(new_folder_dialog);
  return NULL;
}

/* Handles descending into a directory in the clist */
static void go_down(void)
{
  gchar *path;
  metadata_t *meta;

  meta = get_first_metadata_from_selection(HD_LIST);
  if (meta == NULL) {
    return;
  }

  /*
   * Get the size column. If this is zero,
   * we're dealing with a directory! Only
   * directories of size 0 are allowed, no
   * files of size 0.
   */
  if (meta->size == 0) {
    GtkWidget *combo = transfer_widgets.combo;
    add_to_history (get_current_dir());
    change_directory(meta->path);
    path = get_current_dir();
    // Set the text in the box, then switch directory view
    set_popup_history(combo);
    gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(combo))), path);
    fill_in_dir(HD_LIST, path);
  }
  destroy_metadata_t(meta);
}

/* If we come here, then the user has selected a row in the
 * harddisk file listing */
static GCallback hdlist_mouseevent (GtkWidget      *widget,
				    GdkEventButton *event,
				    gpointer        data )
{
  /* GDK_2BUTTON_PRESS doesn't seem to work OK with GTK+-2.0 */
  if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
    go_down();
    return (gpointer) 1;
  } else if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    GdkEventButton *bevent = (GdkEventButton *) event;
    gtk_menu_popup (GTK_MENU (transfer_widgets.harddiskpopupmenu),
		    NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    return (gpointer) 1;
  }
  return data;
}

static GCallback hdlist_keyevent (GtkWidget    *widget,
				  GdkEventKey    *event,
				  gpointer        data )
{
  if (event->type == GDK_KEY_PRESS &&
      (event->keyval == GDK_Return || event->keyval == GDK_KP_Enter)) {
    go_down();
    return (gpointer) 1;
  } else if (event->keyval == GDK_KP_Delete || event->keyval == GDK_Delete) {
    /* Delete on something */
    return (gpointer) 1;
  }
  return data;
}


/* Response to the "transfer" call from the jukebox popup menu */
static GCallback jbmenu_rescan_response(gpointer data)
{
  scan_jukebox(NULL);
  return NULL;
}


/* Response to the "transfer" call from the jukebox popup menu */
static GCallback jbmenu_transfer_response(gpointer data)
{
  transfer_from_jukebox_to_hd(NULL, NULL);
  return NULL;
}

/* Response to a request to play some music */
static GCallback jbmenu_play_response(gpointer data)
{
  GList *metalist = get_all_metadata_from_selection(JB_LIST);

  if (metalist == NULL)
    return NULL;
  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    destroy_metalist(metalist);
    return NULL;
  }
  create_player_window(metalist);
  return NULL;
}


/* Response to the "delete" call from the jukebox popup menu */
static GCallback jbmenu_delete_response(gpointer data)
{
  if (request_confirmation_dialog(_("Really delete selected tracks?"))) {
    GList *metalist = get_all_metadata_from_selection(JB_LIST);

    jukebox_delete_tracks(metalist, playlist_widgets.pltreestore);
    remove_selected(JB_LIST);
    destroy_metalist(metalist);
  }
  return NULL;
}

static GCallback jblist_mouseevent (GtkWidget      *clist,
				    GdkEventButton *event,
				    gpointer        data )
{
  if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    GdkEventButton *bevent = (GdkEventButton *) event;

    gtk_menu_popup (GTK_MENU (transfer_widgets.jukeboxpopupmenu),
		    NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    return (gpointer) 1;
  }
  return data;
}

static int chdir_edit(GtkWidget * widget, gpointer data)
{
  gchar *text, *tempstr, *path;

#if GTK_CHECK_VERSION(2,24,0)
  text = (gchar *) gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(transfer_widgets.combo));
#else
  text = (gchar *) gtk_combo_box_get_active_text(GTK_COMBO_BOX(transfer_widgets.combo));
#endif

  if ((tempstr = expand_path (text)) == NULL) {
    g_free (tempstr);
  } else
    text = tempstr;
  if (text == NULL)
    return (FALSE);

  /* Add the current directory to the history */
  add_to_history(get_current_dir());
  change_directory(text);
  /* Set the text in the box, then switch directory view */
  path = get_current_dir();
  set_popup_history(transfer_widgets.combo);
  gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(transfer_widgets.combo))), path);
  fill_in_dir(HD_LIST, path);
  return (FALSE);
}


static void cell_data_func (GtkTreeViewColumn *tree_column,
			    GtkCellRenderer *cell,
			    GtkTreeModel *model,
			    GtkTreeIter *iter,
			    gpointer data)
{
  metadata_t *meta = get_metadata_from_model(model, iter);
  if (meta->size == 0) {
    g_object_set(G_OBJECT(cell),
		 "foreground", GNOMAD_DIRECTORY_FOLDER_ENTRY_COLOR,
		 "weight", GNOMAD_DIRECTORY_FOLDER_ENTRY_WEIGHT,
		 "style", GNOMAD_DIRECTORY_FOLDER_ENTRY_STYLE,
		 "underline", PANGO_UNDERLINE_SINGLE,
		 NULL);
  } else {
    g_object_set(G_OBJECT(cell),
		 "foreground", GNOMAD_DIRECTORY_FILE_ENTRY_COLOR,
		 "weight", GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT,
		 "style", GNOMAD_DIRECTORY_FILE_ENTRY_STYLE,
		 "underline", FALSE,
		 NULL);
  }
  destroy_metadata_t(meta);
}

static GtkWidget *create_listview(GtkListStore *liststore)
{
  GtkWidget *listview;
  GtkCellRenderer *dirtextrenderer;
  GtkCellRenderer *textrenderer;
  GtkCellRenderer *timerenderer;
  GtkCellRenderer *togglerenderer;
  GtkTreeViewColumn *view;

  /* For testing to add something to the store.
    GtkTreeIter listiter;

    gtk_list_store_append (GTK_LIST_STORE(liststore), &listiter);
    gtk_list_store_set (GTK_LIST_STORE(liststore), &listiter,
    TRACKLIST_ARTIST_COLUMN, "Artist",
    TRACKLIST_TITLE_COLUMN, "Title",
    TRACKLIST_ALBUM_COLUMN, "Album",
    TRACKLIST_YEAR_COLUMN, 2003,
    TRACKLIST_GENRE_COLUMN, "Genre",
    TRACKLIST_LENGTH_COLUMN, "1:00",
    TRACKLIST_SIZE_COLUMN, 1000000,
    TRACKLIST_CODEC_COLUMN, "MP3",
    TRACKLIST_TRACKNO_COLUMN, 1,
    TRACKLIST_PROTECTED_COLUMN, FALSE,
    TRACKLIST_FILENAME_COLUMN, "Fnord.mp3",
    TRACKLIST_ID_COLUMN, "12345",
    -1);
  */
  listview = gtk_tree_view_new_with_model (GTK_TREE_MODEL(liststore));
  gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(listview), TRUE);

  dirtextrenderer = gtk_cell_renderer_text_new();
  textrenderer = gtk_cell_renderer_text_new();
  /* This renderer render time measures */
  timerenderer = gtk_cell_renderer_text_new();

  g_object_set(G_OBJECT(textrenderer),
	       "foreground", GNOMAD_DIRECTORY_FILE_ENTRY_COLOR,
	       "weight", GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT,
	       "style", GNOMAD_DIRECTORY_FILE_ENTRY_STYLE,
	       "underline", FALSE,
	       NULL);
  g_object_set(G_OBJECT(timerenderer),
	       "foreground", GNOMAD_DIRECTORY_FILE_ENTRY_COLOR,
	       "weight", GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT,
	       "style", GNOMAD_DIRECTORY_FILE_ENTRY_STYLE,
	       "underline", FALSE,
	       NULL);

  togglerenderer = gtk_cell_renderer_toggle_new ();

  view = gtk_tree_view_column_new_with_attributes (_("Artist"),
						   dirtextrenderer,
						   "text", TRACKLIST_ARTIST_COLUMN,
						   NULL);
  gtk_tree_view_column_set_cell_data_func(view,
					  dirtextrenderer,
					  (GtkTreeCellDataFunc) cell_data_func,
					  NULL,
					  NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_ARTIST_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Title"),
						   textrenderer,
						   "text", TRACKLIST_TITLE_COLUMN,
						   NULL);
  // FIXME: color this
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_TITLE_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Album"),
						   textrenderer,
						   "text", TRACKLIST_ALBUM_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_ALBUM_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Year"),
						   textrenderer,
						   "text", TRACKLIST_YEAR_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 40);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_YEAR_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Genre"),
						   textrenderer,
						   "text", TRACKLIST_GENRE_COLUMN,
						   NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 80);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_GENRE_COLUMN);

  view = gtk_tree_view_column_new_with_attributes (_("Length"),
						   timerenderer,
						   "text", TRACKLIST_LENGTH_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 40);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_LENGTH_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Size (bytes)"),
						   textrenderer,
						   "text", TRACKLIST_SIZE_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 80);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_SIZE_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Codec"),
						   textrenderer,
						   "text", TRACKLIST_CODEC_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 48);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_CODEC_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Track#"),
						   textrenderer,
						   "text", TRACKLIST_TRACKNO_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 48);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_TRACKNO_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Protected"),
						   togglerenderer,
						   "active", TRACKLIST_PROTECTED_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 48);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_PROTECTED_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Filename"),
						   textrenderer,
						   "text", TRACKLIST_FILENAME_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  /* Hide it */
  gtk_tree_view_column_set_visible(view, FALSE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_FILENAME_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (/* Track ID number */
						   _("ID"),
						   textrenderer,
						   "text", TRACKLIST_ID_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  /* Hide it */
  gtk_tree_view_column_set_visible(view, FALSE);
  gtk_tree_view_column_set_sort_column_id(view, TRACKLIST_ID_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  return listview;
}


GtkWidget *create_xfer_widgets(void)
{
  GtkWidget *popupmenu, *popupmenu_item;
  GtkWidget *button;
  GtkWidget *leftbox;
  GtkWidget *music_panel;
  GtkWidget *transfer_button_vbox;
  GtkWidget *combo;
  GtkWidget *arrow;
  GtkWidget *left_vbox;
  GtkWidget *right_vbox;
  GtkWidget *scrolled_window;
  /* Fresh widgets */
  GtkWidget *hdlistview;
  GtkWidget *jblistview;
  GtkTreeSelection *hdlistselection;
  GtkTreeSelection *jblistselection;
  GtkWidget *label;

  transfer_widgets.history = NULL;
  /* Just add something to the history... */
  transfer_widgets.history =
    g_list_append (transfer_widgets.history, (gpointer) get_current_dir());

  /* The popup menu for the harddisk files */
  popupmenu = gtk_menu_new ();
  popupmenu_item  = gtk_menu_item_new_with_label (_("Rescan directory metadata"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_rescan_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Transfer selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_transfer_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Edit selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_edit_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Delete selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("New folder..."));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_new_folder_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  transfer_widgets.harddiskpopupmenu = popupmenu;

  /* The popup menu for the jukebox files */
  popupmenu = gtk_menu_new ();
  popupmenu_item  = gtk_menu_item_new_with_label (_("Rescan jukebox contents"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_rescan_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Transfer selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_transfer_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Add selected to playlist"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_add_playlist_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Play selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_play_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Edit selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_edit_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Delete selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  transfer_widgets.jukeboxpopupmenu = popupmenu;

  /* Then the panel (handle in the middle) */
  music_panel = gtk_hpaned_new();
  leftbox = gtk_hbox_new(FALSE, 0);
  left_vbox = gtk_vbox_new(FALSE, 0);

#if GTK_CHECK_VERSION(2,24,0)
  // Use the new combo box type in 2.24 and later
  combo = gtk_combo_box_text_new_with_entry();
#else
  combo = gtk_combo_box_entry_new_text();
#endif

  transfer_widgets.combo = combo;
  set_popup_history(combo);
  gtk_box_pack_start(GTK_BOX(left_vbox), combo, FALSE, FALSE, 0);
  // signal "changed" on the combo directly will not work
  g_signal_connect_object(GTK_OBJECT(gtk_bin_get_child(GTK_BIN(combo))),
			  "activate",
			  G_CALLBACK(chdir_edit),
			  NULL,
			  0);
  gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(combo))), get_current_dir());
  gtk_widget_show(combo);

  /* Create the stores and their views */
  hdlistview = create_listview(transfer_widgets.hdliststore);
  transfer_widgets.hdlistview = hdlistview;
  jblistview = create_listview(transfer_widgets.jbliststore);
  transfer_widgets.jblistview = jblistview;
  hdlistselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(hdlistview));
  gtk_tree_selection_set_mode(hdlistselection, GTK_SELECTION_EXTENDED);
  jblistselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(jblistview));
  gtk_tree_selection_set_mode(jblistselection, GTK_SELECTION_EXTENDED);
  /* Connect signal handlers */
  g_signal_connect_object((gpointer) hdlistview,
			  "button_press_event",
			  G_CALLBACK(hdlist_mouseevent),
			  NULL,
			  0);
  g_signal_connect_object((gpointer) jblistview,
			  "button_press_event",
			  G_CALLBACK(jblist_mouseevent),
			  NULL,
			  0);
  /* FIXME: this doesn't seem to work. Look up code for the list MVCs. */
  g_signal_connect_object((gpointer) hdlistview,
			  "key_press_event",
			  G_CALLBACK(hdlist_keyevent),
			  NULL,
			  0);

  /* Fill in and sort directory */
#if GTK_CHECK_VERSION(2,24,0)
  fill_in_dir(HD_LIST, (gchar *) gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combo)));
#else
  fill_in_dir(HD_LIST, (gchar *) gtk_combo_box_get_active_text(GTK_COMBO_BOX(combo)));
#endif
  view_and_sort_list_store(HD_LIST);

  /* Create a scrolled window for the harddisk files */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  gtk_container_add(GTK_CONTAINER(scrolled_window), hdlistview);
  gtk_widget_show(hdlistview);
  gtk_box_pack_start(GTK_BOX(left_vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show (scrolled_window);
  gtk_widget_show(left_vbox);
  gtk_box_pack_start(GTK_BOX(leftbox), left_vbox, TRUE, TRUE, 0);

  /* A vertical box for the transfer buttons */
  transfer_button_vbox=gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(transfer_button_vbox), 5);

  /* Add buttons to this vertical box */
  button = gtk_button_new();
  arrow = gtk_arrow_new(GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
  gtk_container_add(GTK_CONTAINER(button), arrow);
  gtk_widget_show(arrow);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(transfer_from_hd_dialog),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(transfer_button_vbox), button, TRUE, FALSE, 0);
  gtk_widget_show(button);
  button = gtk_button_new();
  arrow = gtk_arrow_new(GTK_ARROW_LEFT, GTK_SHADOW_NONE);
  gtk_container_add(GTK_CONTAINER(button), arrow);
  gtk_widget_show(arrow);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(jbmenu_transfer_response),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(transfer_button_vbox), button, TRUE, FALSE, 0);
  gtk_widget_show(button);

  gtk_box_pack_start(GTK_BOX(leftbox), transfer_button_vbox, FALSE, FALSE, 0);
  gtk_paned_pack1 (GTK_PANED(music_panel), leftbox, TRUE, TRUE);
  gtk_widget_show(leftbox);
  gtk_widget_show(transfer_button_vbox);


  /* A vertical box for jukebox scroll and label */
  right_vbox=gtk_vbox_new(FALSE, 0);
  /* Create a scrolled window for the jukebox files */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  /* Add the CList widget to the vertical box and show it. */
  gtk_container_add(GTK_CONTAINER(scrolled_window), jblistview);
  gtk_widget_show(jblistview);
  gtk_widget_show (scrolled_window);

  label = gtk_label_new(_("Jukebox library window"));
  gtk_box_pack_start(GTK_BOX(right_vbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  gtk_box_pack_start(GTK_BOX(right_vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_paned_pack2 (GTK_PANED(music_panel), right_vbox, TRUE, TRUE);
  gtk_widget_show (right_vbox);
  gtk_paned_set_position (GTK_PANED(music_panel), 320);
  /* Show the entire music panel */
  gtk_widget_show(music_panel);
  return music_panel;
}
