/* data.c
   Data transferring window widgets and callbacks
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you xcan redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "filesystem.h"
#include "jukebox.h"
#include "data.h"
#include "util.h"

static GtkWidget *new_folder_dialog;
static GtkWidget *new_folder_entry;

/* Move the files off the Jukebox and into the harddisk directory */
static GCallback data_from_jukebox_to_hd (GtkButton *button,
					  gpointer data)
{
  jb2hd_data_thread_arg_t *jb2hd_data_thread_args;
  GList *metalist = get_all_metadata_from_selection(JBDATA_LIST);

  jb2hd_data_thread_args = (jb2hd_data_thread_arg_t *) malloc(sizeof (jb2hd_data_thread_arg_t));

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    destroy_metalist(metalist);
    return NULL;
  }

  if (metalist != NULL) {
    GtkWidget *label1, *label2, *dialog, *button, *separator;

    dialog = gtk_dialog_new_with_buttons(_("Transferring files from jukebox file storage"),
					 NULL,
					 0,
					 NULL);
    button = gtk_dialog_add_button(GTK_DIALOG(dialog),
				   GTK_STOCK_CANCEL,
				   0);
    g_signal_connect(button,
		     "clicked",
		     G_CALLBACK(cancel_jukebox_operation_click),
		     NULL);
    g_signal_connect_object(GTK_OBJECT(dialog),
			    "delete_event",
			    G_CALLBACK(gtk_widget_destroy),
			    GTK_OBJECT(dialog),
			    0);
    gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
    label1 = gtk_label_new(_("Retrieving data files from Jukebox..."));
    label2 = gtk_label_new("");
    progress_bar = gtk_progress_bar_new();
    separator = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), separator, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), progress_bar, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog);

    jb2hd_data_thread_args->dialog = dialog;
    jb2hd_data_thread_args->label = label2;
    jb2hd_data_thread_args->metalist = metalist;
    jukebox_locked = TRUE;
    cancel_jukebox_operation = FALSE;
    g_thread_create(jb2hd_data_thread,(gpointer) jb2hd_data_thread_args, FALSE, NULL);
  }
  return NULL;
}


/* Move the files off the Harddisk and into the Jukebox */
static GCallback data_from_hd_to_jukebox(GtkButton *button/*UNUSED*/, gpointer user_data/*UNUSED*/)
{
  hd2jb_data_thread_arg_t *hd2jb_data_thread_args;
  GList *metalist = get_all_metadata_from_selection(HDDATA_LIST);

  hd2jb_data_thread_args = (hd2jb_data_thread_arg_t *) malloc(sizeof (hd2jb_data_thread_arg_t));

  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    destroy_metalist(metalist);
    return NULL;
  }

  if (metalist != NULL) {
    GtkWidget *label1, *label2, *dialog, *button, *separator;
    dialog=gtk_dialog_new_with_buttons(_("Transferring files to jukebox file storage"),
		    NULL,
		    0,
		    NULL);

    button = gtk_dialog_add_button(GTK_DIALOG(dialog),
		    GTK_STOCK_CANCEL,
		    0);
    g_signal_connect(button,
		    "clicked",
		    G_CALLBACK(cancel_jukebox_operation_click),
		    NULL);
    g_signal_connect_object(GTK_OBJECT(dialog),
			    "delete_event",
			    G_CALLBACK(gtk_widget_destroy),
			    GTK_OBJECT(dialog),
			    0);
    gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
    label1 = gtk_label_new(_("Storing files in Jukebox file storage..."));
    label2 = gtk_label_new("");
    progress_bar = gtk_progress_bar_new();
    separator = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), separator, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), progress_bar, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog);

    hd2jb_data_thread_args->dialog = dialog;
    hd2jb_data_thread_args->label = label2;
    hd2jb_data_thread_args->metalist = metalist;
    jukebox_locked = TRUE;
    cancel_jukebox_operation = FALSE;
    g_thread_create(hd2jb_data_thread,(gpointer) hd2jb_data_thread_args, FALSE, NULL);
  }
  return NULL;
}

#define MAX_HISTORY 10

static void add_to_history (gchar *string)
{
  /* See if the string is already in the history */
  GList *history = data_widgets.history;
  while (history)
    {
      if (!strcmp(history->data, string))
	  return;
      history = g_list_next(history);
    }
  history = data_widgets.history;
  /* Else add it */
  data_widgets.history
    = g_list_prepend (data_widgets.history, string);
  /* Do not remember more than MAX_HISTORY strings */
  while (g_list_length(history) > MAX_HISTORY)
    {
      GList *elem;

      elem = g_list_last(history);
      data_widgets.history
	= g_list_remove(history, elem->data);
    }
}

static void set_popup_history(GtkWidget *combo)
{
  GList *history = data_widgets.history;

#if GTK_CHECK_VERSION(3,0,0)
  // Use the new combobox abstractions in 3.0 and later
  gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(combo));
  while (history) {
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), history->data);
    history = g_list_next(history);
  }
#else
  // Dead ugly code for older GTK
  static gboolean firstrun = TRUE;
  int i;

  // Blank out all 5 entries
  if (!firstrun) {
    for (i = 0; i < MAX_HISTORY; i++) {
      gtk_combo_box_remove_text(GTK_COMBO_BOX(combo), 0);
    }
  } else {
    firstrun = FALSE;
  }

  for (i = 0; i < MAX_HISTORY; i++) {
    if (history) {
      gtk_combo_box_append_text(GTK_COMBO_BOX(combo), history->data);
      history = g_list_next(history);
    } else {
      gtk_combo_box_append_text(GTK_COMBO_BOX(combo), "");
    }
    i++;
  }
#endif
}


/* Response to the "rescan" call from the harddisk popup menu */
static GCallback hdmenu_rescan_response(gpointer data)
{
  fill_in_dir(HDDATA_LIST, get_current_dir());
  return NULL;
}


/* Response to the "transfer" call from the harddisk popup menu */
static GCallback hdmenu_transfer_response(gpointer data)
{
  data_from_hd_to_jukebox(NULL, NULL);
  return NULL;
}

/* Response to the "delete" call from the harddisk popup menu */
static GCallback hdmenu_delete_response(gpointer data)
{
  if (request_confirmation_dialog(_("Really delete selected files?"))) {
    GList *metalist = get_all_metadata_from_selection(HDDATA_LIST);

    delete_files(metalist);
    remove_selected(HDDATA_LIST);
    destroy_metalist(metalist);
    // fill_in_dir(HDDATA_LIST, get_current_dir());
  }
  return NULL;
}

/* Handles descending into a directory */
static void go_down(void)
{
  gchar *path;
  metadata_t *meta;

  meta = get_first_metadata_from_selection(HDDATA_LIST);
  if (meta == NULL) {
    return;
  }

  /*
   * Get the size column. If this is zero,
   * we're dealing with a directory! Only
   * directories of size 0 are allowed, no
   * files of size 0.
   */
  if (meta->size == 0) {
    GtkWidget *combo = data_widgets.combo;
    add_to_history (get_current_dir());
    change_directory(meta->path);
    path = get_current_dir();
    // Set the text in the box, then switch directory view
    set_popup_history(combo);
    gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(combo))), path);
    fill_in_dir(HDDATA_LIST, path);
  }
  destroy_metadata_t(meta);
}

static void go_down_jukebox(void)
{
  metadata_t *meta;

  meta = get_first_metadata_from_selection(JBDATA_LIST);
  if (meta == NULL) {
    return;
  }

  /*
   * Get the size column. If this is zero,
   * we're dealing with a directory! Only
   * directories of size 0 are allowed, no
   * files of size 0.
   */
  if (meta->size == 0) {
    GtkWidget *entry = data_widgets.jbentry;

    // g_print("Changing directory to %s\n", meta->folder);
    rebuild_datafile_list(meta->folder);
    // Set the text in the box
    gtk_entry_set_text (GTK_ENTRY(entry), meta->folder);
  }
  destroy_metadata_t(meta);
}

/*
 * If we come here, then the user has selected a row in the
 * harddisk file listing
 */
static GCallback hdlist_mouseevent(GtkWidget      *widget,
				   GdkEventButton *event,
				   gpointer        data )
{
  if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
    go_down();
    return (gpointer) 1;
  } else if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    GdkEventButton *bevent = (GdkEventButton *) event;

    gtk_menu_popup (GTK_MENU (data_widgets.harddiskpopupmenu),
		    NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    return (gpointer) 1;
  }
  return data;
}

static GCallback hdlist_keyevent (GtkWidget    *widget,
				  GdkEventKey    *event,
				  gpointer        data )
{
  if (event->type == GDK_KEY_PRESS &&
      (event->keyval == GDK_Return || event->keyval == GDK_KP_Enter)) {
    go_down();
    return (gpointer) 1;
  } else if (event->keyval == GDK_KP_Delete || event->keyval == GDK_Delete) {
    /* Delete on something */
    return (gpointer) 1;
  }
  return data;
}


/* Response to the "transfer" call from the jukebox popup menu */
static GCallback jbmenu_rescan_response(gpointer data)
{
  scan_jukebox(NULL);
  return NULL;
}


/* Response to the "transfer" call from the jukebox popup menu */
static GCallback jbmenu_transfer_response(gpointer data)
{
  data_from_jukebox_to_hd(NULL, NULL);
  return NULL;
}


static GCallback create_new_folder_ok_button (GtkButton *button,
						gpointer data)
{
  gchar *foldername;

  foldername = g_strdup(gtk_entry_get_text(GTK_ENTRY(new_folder_entry)));
  gtk_widget_destroy(new_folder_dialog);
  // Create it on the jukebox
  g_print("Creating folder \"%s\"...\n", foldername);
  jukebox_create_folder(foldername);
  g_free(foldername);
  return NULL;
}


/* Response to the "create folder" call from the harddisk popup menu */
static GCallback jbmenu_create_folder_response(gpointer data)
{
  GtkWidget *label, *button;

  new_folder_dialog=gtk_dialog_new_with_buttons(_("Create a new folder"),
		  NULL,
		  0,
		  NULL);

  button = gtk_dialog_add_button(GTK_DIALOG(new_folder_dialog),
		  GTK_STOCK_CANCEL,
		  0);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(dispose_of_dialog_window),
		  GTK_OBJECT(new_folder_dialog));

  button = gtk_dialog_add_button(GTK_DIALOG(new_folder_dialog),
		  GTK_STOCK_OK,
		  1);
  g_signal_connect(button,
		  "clicked",
		  G_CALLBACK(create_new_folder_ok_button),
		  NULL);
  gtk_window_set_position (GTK_WINDOW(new_folder_dialog), GTK_WIN_POS_MOUSE);

  label = gtk_label_new(_("Choose a name for the new folder:"));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_folder_dialog)->vbox), label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  new_folder_entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(new_folder_dialog)->vbox), new_folder_entry, TRUE, TRUE, 0);
  gtk_widget_show(new_folder_entry);

  g_signal_connect(G_OBJECT (new_folder_dialog),
		   "delete_event",
		   G_CALLBACK(gtk_widget_destroy),
		   G_OBJECT (new_folder_dialog));

  gtk_widget_show_all(new_folder_dialog);
  return NULL;
}


/* Response to the "delete" call from the jukebox popup menu */
static GCallback jbmenu_delete_response(gpointer data)
{
  if (request_confirmation_dialog(_("Really delete selected files?"))) {
    GList *metalist = get_all_metadata_from_selection(JBDATA_LIST);

    jukebox_delete_files(metalist);
    remove_selected(JBDATA_LIST);
    destroy_metalist(metalist);
  }
  return NULL;
}


/*
 * If we come here, then the user has selected a row in the
 * jukebox files listing
 */
static GCallback jblist_mouseevent(GtkWidget      *clist,
				   GdkEventButton *event,
				   gpointer        data )
{
  if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
    go_down_jukebox();
    return (gpointer) 1;
  } else if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    GdkEventButton *bevent = (GdkEventButton *) event;

    gtk_menu_popup (GTK_MENU (data_widgets.jukeboxpopupmenu),
		    NULL, NULL, NULL, NULL, bevent->button, bevent->time);
    return (gpointer) 1;
  }
  return data;
}


static int chdir_edit(GtkWidget * widget, gpointer data)
{
  gchar *text, *tempstr, *path;

#if GTK_CHECK_VERSION(2,24,0)
  text = (gchar *) gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(data_widgets.combo));
#else
  text = (gchar *) gtk_combo_box_get_active_text(GTK_COMBO_BOX(data_widgets.combo));
#endif
  if ((tempstr = expand_path (text)) == NULL) {
    g_free (tempstr);
  } else
    text = tempstr;
  if (text == NULL)
    return (FALSE);

  /* Add the current directory to the history */
  add_to_history(get_current_dir());
  change_directory(text);
  /* Set the text in the box, then switch directory view */
  path = get_current_dir();
  set_popup_history(data_widgets.combo);
  gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(data_widgets.combo))), path);
  fill_in_dir(HDDATA_LIST, path);
  return (FALSE);
}

static void cell_data_func (GtkTreeViewColumn *tree_column,
			    GtkCellRenderer *cell,
			    GtkTreeModel *model,
			    GtkTreeIter *iter,
			    gpointer data)
{
  metadata_t *meta = get_data_metadata_from_model(model, iter);
  if (meta->size == 0) {
    g_object_set(G_OBJECT(cell),
		 "foreground", GNOMAD_DIRECTORY_FOLDER_ENTRY_COLOR,
		 "weight", GNOMAD_DIRECTORY_FOLDER_ENTRY_WEIGHT,
		 "style", GNOMAD_DIRECTORY_FOLDER_ENTRY_STYLE,
		 "underline", PANGO_UNDERLINE_SINGLE,
		 NULL);
  } else {
    g_object_set(G_OBJECT(cell),
		 "foreground", GNOMAD_DIRECTORY_FILE_ENTRY_COLOR,
		 "weight", GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT,
		 "style", GNOMAD_DIRECTORY_FILE_ENTRY_STYLE,
		 "underline", FALSE,
		 NULL);
  }
  destroy_metadata_t(meta);
}


static GtkWidget *create_listview(GtkListStore *liststore)
{
  GtkWidget *listview;
  GtkCellRenderer *dirtextrenderer;
  GtkCellRenderer *textrenderer;
  GtkTreeViewColumn *view;

  listview = gtk_tree_view_new_with_model (GTK_TREE_MODEL(liststore));
  gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(listview), TRUE);

  dirtextrenderer = gtk_cell_renderer_text_new ();
  textrenderer = gtk_cell_renderer_text_new ();
  g_object_set(G_OBJECT(textrenderer),
	       "foreground", GNOMAD_DIRECTORY_FILE_ENTRY_COLOR,
	       "weight", GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT,
	       "style", GNOMAD_DIRECTORY_FILE_ENTRY_STYLE,
	       "underline", FALSE,
	       NULL);

  view = gtk_tree_view_column_new_with_attributes (_("Filename"),
						   dirtextrenderer,
						   "text", FILELIST_FILENAME_COLUMN,
						   NULL);
  gtk_tree_view_column_set_cell_data_func(view,
					  dirtextrenderer,
					  (GtkTreeCellDataFunc) cell_data_func,
					  NULL,
					  NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, FILELIST_FILENAME_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (_("Size (bytes)"),
						   textrenderer,
						   "text", FILELIST_SIZE_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 80);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  gtk_tree_view_column_set_sort_column_id(view, FILELIST_SIZE_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  view = gtk_tree_view_column_new_with_attributes (/* file ID number */
						   _("ID"),
						   textrenderer,
						   "text", FILELIST_ID_COLUMN,
						   NULL);
  gtk_tree_view_column_set_sizing (view, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_resizable(view, TRUE);
  gtk_tree_view_column_set_fixed_width(view, 150);
  gtk_tree_view_column_set_reorderable(view, TRUE);
  /* Hide it */
  gtk_tree_view_column_set_visible(view, FALSE);
  gtk_tree_view_column_set_sort_column_id(view, FILELIST_ID_COLUMN);
  gtk_tree_view_append_column (GTK_TREE_VIEW(listview), view);

  return listview;
}

GtkWidget *create_data_widgets(void)
{
  GtkWidget *popupmenu, *popupmenu_item;
  GtkWidget *button;
  GtkWidget *leftbox;
  GtkWidget *panel;
  GtkWidget *transfer_button_vbox;
  GtkWidget *combo;
  GtkWidget *jbentry;
  GtkWidget *arrow;
  GtkWidget *left_vbox;
  GtkWidget *right_vbox;
  GtkWidget *scrolled_window;
  GtkWidget *label;
  /* Fresh widgets */
  GtkWidget *hdlistview;
  GtkWidget *jblistview;
  GtkTreeSelection *hdlistselection;
  GtkTreeSelection *jblistselection;

  data_widgets.history = NULL;
  /* Just add something to the history... */
  data_widgets.history =
    g_list_append (data_widgets.history, (gpointer) get_current_dir());

  /* The popup menu for the harddisk files */
  popupmenu = gtk_menu_new ();
  popupmenu_item  = gtk_menu_item_new_with_label (_("Rescan jukebox"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_rescan_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Transfer selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_transfer_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Delete selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(hdmenu_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  data_widgets.harddiskpopupmenu = popupmenu;

  /* The popup menu for the jukebox files */
  popupmenu = gtk_menu_new ();
  popupmenu_item  = gtk_menu_item_new_with_label (_("Rescan contents"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_rescan_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Create folder"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_create_folder_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Transfer selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_transfer_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  popupmenu_item  = gtk_menu_item_new_with_label (_("Delete selected"));
  gtk_menu_shell_append (GTK_MENU_SHELL(popupmenu), popupmenu_item);
  g_signal_connect_object(GTK_OBJECT(popupmenu_item),
			  "activate",
			  G_CALLBACK(jbmenu_delete_response),
			  NULL,
			  0);
  gtk_widget_show (popupmenu_item);
  data_widgets.jukeboxpopupmenu = popupmenu;

  /* Then the panel (handle in the middle) */
  panel = gtk_hpaned_new();
  leftbox = gtk_hbox_new(FALSE, 0);
  left_vbox = gtk_vbox_new(FALSE, 0);

#if GTK_CHECK_VERSION(2,24,0)
  // Use the new combo box type in 2.24 and later
  combo = gtk_combo_box_text_new_with_entry();
#else
  combo = gtk_combo_box_entry_new_text();
#endif

  data_widgets.combo = combo;
  set_popup_history(combo);
  gtk_box_pack_start(GTK_BOX(left_vbox), combo, FALSE, FALSE, 0);
  // signal "changed" on the combo directly will not work
  g_signal_connect_object(GTK_OBJECT(gtk_bin_get_child(GTK_BIN(combo))),
			  "activate",
			  G_CALLBACK(chdir_edit),
			  NULL,
			  0);
  gtk_entry_set_text(GTK_ENTRY(gtk_bin_get_child(GTK_BIN(combo))), get_current_dir());
  gtk_widget_show(combo);

  /* Create the stores and their views */
  hdlistview = create_listview(data_widgets.hdliststore);
  data_widgets.hdlistview = hdlistview;
  jblistview = create_listview(data_widgets.jbliststore);
  data_widgets.jblistview = jblistview;
  hdlistselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(hdlistview));
  gtk_tree_selection_set_mode(hdlistselection, GTK_SELECTION_EXTENDED);
  jblistselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(jblistview));
  gtk_tree_selection_set_mode(jblistselection, GTK_SELECTION_EXTENDED);
  /* Connect signal handlers */
  g_signal_connect_object((gpointer) hdlistview,
			  "button_press_event",
			  G_CALLBACK(hdlist_mouseevent),
			  NULL,
			  0);
  g_signal_connect_object((gpointer) jblistview,
			  "button_press_event",
			  G_CALLBACK(jblist_mouseevent),
			  NULL,
			  0);
  /* FIXME: this doesn't seem to work. Look up code for the list MVCs. */
  g_signal_connect_object((gpointer) hdlistview,
			  "key_press_event",
			  G_CALLBACK(hdlist_keyevent),
			  NULL,
			  0);

  /* Fill in and sort directory */
#if GTK_CHECK_VERSION(2,24,0)
  fill_in_dir(HDDATA_LIST, (gchar *) gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combo)));
#else
  fill_in_dir(HDDATA_LIST, (gchar *) gtk_combo_box_get_active_text(GTK_COMBO_BOX(combo)));
#endif
  view_and_sort_list_store(HDDATA_LIST);

  /* Create a scrolled window for the harddisk files */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  /* Add the CList widget to the vertical box and show it. */
  gtk_container_add(GTK_CONTAINER(scrolled_window), hdlistview);
  gtk_widget_show(hdlistview);
  gtk_box_pack_start(GTK_BOX(left_vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show (scrolled_window);
  gtk_widget_show(left_vbox);
  gtk_box_pack_start(GTK_BOX(leftbox), left_vbox, TRUE, TRUE, 0);

  /* A vertical box for the transfer buttons */
  transfer_button_vbox=gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(transfer_button_vbox), 5);

  /* Add buttons to this vertical box */
  button = gtk_button_new();
  arrow = gtk_arrow_new(GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
  gtk_container_add(GTK_CONTAINER(button), arrow);
  gtk_widget_show(arrow);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(data_from_hd_to_jukebox),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(transfer_button_vbox), button, TRUE, FALSE, 0);
  gtk_widget_show(button);
  button = gtk_button_new();
  arrow = gtk_arrow_new(GTK_ARROW_LEFT, GTK_SHADOW_NONE);
  gtk_container_add(GTK_CONTAINER(button), arrow);
  gtk_widget_show(arrow);
  g_signal_connect_object(GTK_OBJECT(button),
			  "clicked",
			  G_CALLBACK(data_from_jukebox_to_hd),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(transfer_button_vbox), button, TRUE, FALSE, 0);
  gtk_widget_show(button);

  gtk_box_pack_start(GTK_BOX(leftbox), transfer_button_vbox, FALSE, FALSE, 0);
  gtk_paned_pack1 (GTK_PANED(panel), leftbox, TRUE, TRUE);
  gtk_widget_show(leftbox);
  gtk_widget_show(transfer_button_vbox);

  /* A vertical box for jukebox scroll and label */
  right_vbox=gtk_vbox_new(FALSE, 0);
  /* Create a scrolled window for the jukebox files */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  /* Add the ListView widget to the vertical box and show it. */
  gtk_container_add(GTK_CONTAINER(scrolled_window), jblistview);
  gtk_widget_show(jblistview);
  gtk_widget_show (scrolled_window);

  label = gtk_label_new(_("Jukebox data file window"));
  gtk_box_pack_start(GTK_BOX(right_vbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  /* An entry for showing jukebox directory */
  jbentry = gtk_entry_new();
  data_widgets.jbentry = jbentry;
  gtk_box_pack_start(GTK_BOX(right_vbox), jbentry, FALSE, FALSE, 0);
  gtk_entry_set_text (GTK_ENTRY(jbentry), "\\");
  gtk_widget_show (jbentry);
  /* Then the rest of the stuff... */
  gtk_box_pack_start(GTK_BOX(right_vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_paned_pack2 (GTK_PANED(panel), right_vbox, TRUE, TRUE);
  gtk_widget_show (right_vbox);
  gtk_paned_set_position (GTK_PANED(panel), 320);
  /* Show the entire data file panel */
  gtk_widget_show(panel);
  return panel;
}
