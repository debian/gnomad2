/* common.h
   Shared header file for the GNOMAD package
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with Yelah xmltools; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#ifndef COMMON_INCLUDED
#define COMMON_INCLUDED

#include <stdio.h>
#if HAVE_STRING_H
# include <string.h>
#endif
#if HAVE_STDLIB_H
# include <stdlib.h>
#endif

/* Internationalization */
#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext(String)
#ifdef gettext_noop
#define N_(String) gettext_noop(String)
#else
#define N_(String) (String)
#endif
#else /* NLS is disabled */
#define _(String) (String)
#define N_(String) (String)
#define textdomain(String) (String)
#define gettext(String) (String)
#define dgettext(Domain,String) (String)
#define dcgettext(Domain,String,Type) (String)
#define bindtextdomain(Domain,Directory) (Domain)
#define bind_textdomain_codeset(Domain,Codeset) (Codeset)
#endif /* ENABLE_NLS */

/* The GTK library */
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>

/* Macros lacking in GTK+ */
#ifdef G_GUINT32_FORMAT
#define PRIguint32 G_GUINT32_FORMAT
#define PRIguint64 G_GUINT64_FORMAT
#else
/* Retrieve C99 format macros */
#if HAVE_INTTYPES_H
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#define PRIguint32 PRIu32
#define PRIguint64 PRIu64
#else
#define PRIguint32 "u"
#define PRIguint64 "Lu"
#endif
#endif

#define GUINT32_TO_GPOINTER GUINT_TO_POINTER
#define GPOINTER_TO_GUINT32 GPOINTER_TO_UINT

/* Directory colors and styles */
#define GNOMAD_DIRECTORY_FILE_ENTRY_COLOR    "black"
#define GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT   NULL
#define GNOMAD_DIRECTORY_FILE_ENTRY_STYLE    NULL
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_COLOR  "blue"
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_WEIGHT "bold"
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_STYLE  NULL
/*
 * Example high contrast colors and style.
 * comment these in and comment out the lines above
 * to activate a high-visibility style.
 */
/*
#define GNOMAD_DIRECTORY_FILE_ENTRY_COLOR    "green"
#define GNOMAD_DIRECTORY_FILE_ENTRY_WEIGHT   PANGO_WEIGHT_BOLD
#define GNOMAD_DIRECTORY_FILE_ENTRY_STYLE    NULL
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_COLOR  "green"
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_WEIGHT PANGO_WEIGHT_BOLD
#define GNOMAD_DIRECTORY_FOLDER_ENTRY_STYLE  PANGO_STYLE_ITALIC
*/

/* Globally useful structures */

typedef struct
{
  GtkListStore *hdliststore;
  GtkListStore *jbliststore;
  GtkWidget *hdlistview;
  GtkWidget *jblistview;
  GtkWidget *harddiskpopupmenu;
  GtkWidget *jukeboxpopupmenu;
  GtkWidget *combo;
  GList *history;
} transfer_widgets_t;

typedef struct
{
  GtkListStore *hdliststore;
  GtkListStore *jbliststore;
  GtkWidget *hdlistview;
  GtkWidget *jblistview;
  GtkWidget *harddiskpopupmenu;
  GtkWidget *jukeboxpopupmenu;
  GtkWidget *combo;
  GtkWidget *jbentry;
  GList *history;
} data_widgets_t;

typedef struct
{
  GtkTreeStore *pltreestore;
  GtkWidget *pltreeview;
  GtkWidget *playlistpopupmenu;
  GtkWidget *nodepopupmenu;
  GtkWidget *blankpopupmenu;
} playlist_widgets_t;

/* Globally known widgets */
transfer_widgets_t transfer_widgets;
data_widgets_t data_widgets;
playlist_widgets_t playlist_widgets;

/* Global progress bar - not so good but... */
GtkWidget *progress_bar;

/* Global playlist selection for the popup, not good either ... */
GList *jukebox_playlist;
GList *selected_target_playlists;

/* Global lock variable for the jukebox */
gboolean volatile jukebox_locked;

/* Global cancellation variable for jukebox operations */
gboolean volatile cancel_jukebox_operation;

/* Global debug level variable (standard = 7) */
gint gnomad_debug;

/* A proc for hiding dialog windows */
GCallback dispose_of_dialog_window(GtkButton * button, gpointer data);

/* THIS PROBABLY BELONGS IN A DIFFERENT HEADER.
 * THIS IS THE ONLY EXTERNAL FUNCTION DEFINED IN gnomad2.c (other than main).
 */
extern void scan_jukebox(gpointer data);

#endif
