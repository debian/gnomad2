/* metadata.h
   Metadata list stores, header file
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#ifndef METADATAH_INCLUDED
#define METADATAH_INCLUDED 1

typedef struct 
{
  gchar *artist;
  gchar *title;
  gchar *album;
  guint32 year;
  gchar *genre;
  gchar *length;
  gulong size;
  gchar *codec;
  guint32 trackno;
  gboolean protect;
  gchar *filename;
  gchar *path;
  gchar *folder;
} metadata_t;

enum
  {
    TRACKLIST_ARTIST_COLUMN,
    TRACKLIST_TITLE_COLUMN,
    TRACKLIST_ALBUM_COLUMN,
    TRACKLIST_YEAR_COLUMN,
    TRACKLIST_GENRE_COLUMN,
    TRACKLIST_LENGTH_COLUMN,
    TRACKLIST_SIZE_COLUMN,
    TRACKLIST_CODEC_COLUMN,
    TRACKLIST_TRACKNO_COLUMN,
    TRACKLIST_PROTECTED_COLUMN,
    TRACKLIST_FILENAME_COLUMN,
    TRACKLIST_ID_COLUMN, /* Hidden */
    TRACKLIST_FOLDER_COLUMN, /* Hidden */
    TRACKLIST_N_COLUMNS
  };

enum
  {
    PLIST_PLAYLISTNAME_COLUMN,
    PLIST_ARTIST_COLUMN,
    PLIST_TITLE_COLUMN,
    PLIST_LENGTH_COLUMN,
    PLIST_PLID_COLUMN, /* Hidden */
    PLIST_SONGID_COLUMN, /* Hidden */
    PLIST_N_COLUMNS
  };

enum
  {
    FILELIST_FILENAME_COLUMN,
    FILELIST_SIZE_COLUMN,
    FILELIST_ID_COLUMN, /* Hidden */
    FILELIST_FOLDER_COLUMN, /* Hidden */
    FILELIST_EXTENSION_COLUMN, /* Hidden */
    FILELIST_N_COLUMNS
  };

typedef enum
  {
    HD_LIST,
    JB_LIST,
    HDDATA_LIST,
    JBDATA_LIST,
    PL_TREE
  } listtype_t;

metadata_t *new_metadata_t(void);
void destroy_metadata_t(metadata_t *meta);
metadata_t *clone_metadata_t(metadata_t const * const meta);
void dump_metadata_t(metadata_t const * const meta);
gchar *compose_metadata_string(metadata_t const * const meta, 
			       gchar const * const template, 
			       gboolean const filesystemsafe);
metadata_t *get_metadata_from_model(GtkTreeModel *model, 
				    GtkTreeIter *iter);
metadata_t *get_data_metadata_from_model(GtkTreeModel *model, 
					 GtkTreeIter *iter);
void add_metadata_to_model(metadata_t const * const meta, 
			   listtype_t const listtype);
GtkTreeSelection *get_metadata_selection(listtype_t listtype);
GList *get_all_metadata_from_selection(listtype_t listtype);
void destroy_metalist(GList *metalist);
metadata_t *get_first_metadata_from_selection(listtype_t listtype);
void remove_selected(listtype_t listtype);
void view_and_sort_list_store(listtype_t listtype);
void recreate_list_store(listtype_t listtype);
void create_list_stores(void);
void clear_list_store(listtype_t listtype);

#endif
