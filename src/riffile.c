/* riffile.c
   WAV and AVI interface
   Copyright (C) 2004-2011 Linus Walleij
   Copyright (C) 2008 Kees van Veen

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Much of the code in this file was derived from the getid3() code,
written in PHP. The C implementation here is however made from
scratch.

*/

#include "common.h"
#include "metadata.h"
#include "filesystem.h"
#include "util.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>

#define b2len(b) ( ((b)[0])+((b)[1]<<8)+((b)[2]<<16)+((b)[3]<<24) )

static guint32 le_to_guint(guchar *data)
{
  return data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0];
}

/* -------------------------------------- */
/* EXPORTED FUNCTIONS                     */
/* -------------------------------------- */

void
get_tag_for_wavfile (metadata_t *meta)
{
  gint fd;
  guchar header[46];
  gchar *tmppath = filename_fromutf8(meta->path);
  gint n;

  // g_print("Getting WAV tag info for %s...\n", meta->path);
  fd = (gint) g_open(tmppath, READONLY_FLAGS, 0);
  if (fd < 0) {
    g_free(tmppath);
    return;
  }
  // g_print("Opened file\n");
  g_free(tmppath);

  // Read in some stuff...
  n = read(fd,header,46);
  if (n == 46) {
    // Hardcoded RIFF header
    if ((header[0] == 'R' || header[0] == 'r') &&
	(header[1] == 'I' || header[1] == 'i') &&
	(header[2] == 'F' || header[2] == 'f') &&
	(header[3] == 'F' || header[3] == 'f') &&
	(header[8] == 'W' || header[8] == 'w') &&
	(header[9] == 'A' || header[9] == 'a') &&
	(header[10] == 'V' || header[10] == 'v') &&
	(header[11] == 'E' || header[11] == 'e') &&
	(header[12] == 'f' || header[12] == 'F') &&
	(header[13] == 'm' || header[13] == 'M') &&
	(header[14] == 't' || header[14] == 'T') &&
	header[15] == ' ') {
      // This is indeed a RIFF/WAVE file
      guint32 chunksize = le_to_guint(&header[4]);
      guint32 fmtchunksize = le_to_guint(&header[16]);
      //guint32 samplerate = le_to_guint(&header[24]);
      guint32 byterate = le_to_guint(&header[28]);
      guint32 calctime;

      // Calculate the run time. Remove the format chunk size,
      // the four remaining bytes of the RIFF header and the
      // data header (8 bytes).
      calctime = (chunksize - fmtchunksize - 8 - 4) / byterate;
      if (calctime == 0) {
	// Atleast one second, please.
	calctime = 1;
      }

      //g_print("RIFF/WAVE chunksize: %d bytes\n", chunksize);
      //g_print("Sample rate: %d samples/s\n", samplerate);
      //g_print("Byte rate: %d bytes/s\n", byterate);
      //g_print("Calculated time: %d seconds\n", calctime);
      if (meta->length != NULL) {
	g_free(meta->length);
      }
      meta->length = seconds_to_mmss(calctime);
    }
  }

  close(fd);
}

void
get_tag_for_avifile (metadata_t *meta)
{
	gint fd;
	unsigned char header[256+1], buffer[256+1], type[256+1];
	gchar *tmppath = filename_fromutf8(meta->path);
	size_t len;
	int length = -1;

	// g_print("Getting WAV tag info for %s...\n", meta->path);
	fd = (gint) g_open(tmppath, READONLY_FLAGS, 0);
	if (fd < 0) {
		g_free(tmppath);
		return;
	}
	g_free(tmppath);

	if (read(fd, header, 12)!=12)
		goto BAD;

	len = b2len(header+4);
	if (! (strncmp(header, "RIFF", 4)==0 && strncmp(header+8, "AVI ", 4)==0) )
		goto BAD;	/* Bad AVI file */

	for (;;) {
		if (read(fd, header, 12)!=12)
			goto BAD;

		if (strncmp(header, "LIST", 4)==0) {
			int max = b2len(header+4);
			if (strncmp(header+8, "hdrl", 4)==0) {
				for (; max>0; ) {
					if (read(fd,header, 12)!=12)
						goto BAD;

					max -= 12;
					len = b2len(header+4);
					if (strncmp(header, "LIST", 4)==0) {
						if (len>max) break;
						else continue;
					} else if (strncmp(header, "strh", 4)==0) {
						int newlength;
						double scale, rate;
						strncpy(type, header+8, 4); type[4]='\0';
						if (read(fd, buffer, 36)!=36)
							goto BAD;
						len -= 36;
						scale = (double) b2len(buffer+16);
						rate = (double) b2len(buffer+20);
						newlength = b2len(buffer+28);
						if (scale==0.0) scale = 0.0000001;
						newlength /= rate/scale;
						if (newlength>length)		/* Previous length may be audio/video - choose largest */
							length = newlength;

//					} else if (strncmp(header, "strf", 4)==0) {
//						if (strcmp(type, "vids")==0)
//							printf("Video length: %d\n", length);
//						else
//							printf("Audio length: %d\n", length);
					}
					if (lseek(fd, len-4, SEEK_CUR)<0)
						goto BAD;
					max -= len-4;
				}
				break;
			} else {
				if (lseek(fd, len-4, SEEK_CUR)<0)
					goto BAD;
			}
		} else {
			goto BAD;
		}
	}
//	if (length>=0) {
//		int h=length/3600, m=(length%3600)/60, s=length-h*3600-m*60;
//		printf("length: %02d:%02d:%02d\n", h,m,s);
//	}
BAD:

//	if (meta->length != NULL) {
//		g_free(meta->length);
//	}
	meta->length = seconds_to_mmss(length);
	close(fd);
}
