/* util.h
   General utility functions, eg for string formatting
   header definitions.
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#ifndef UTILH_INCLUDED
#define UTILH_INCLUDED 1

/* Exported functions */
gint vectorlength(gchar **vector);
gchar *stringcat(gchar *org, gchar *add);
void replacechar(gchar *string, gchar find, gchar replace);
gchar *replacestring(gchar *string, gchar *find, gchar *replace);
gchar *seconds_to_mmss(guint32 seconds);
guint32 mmss_to_seconds(gchar *mmss);
guint32 string_to_guint32(gchar *string);
gboolean is_a_number(gchar *string);
void add_empty_hbox (GtkWidget *tobox);
void create_error_dialog(gchar *errorstring);
gboolean request_confirmation_dialog(gchar *confirmstring);
void hexdump(unsigned char *data, guint len);

#endif
