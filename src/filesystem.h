/* filesystem.h
   Interface to the posix (well...) file system
   header definitions
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#ifndef FILESYSTEMH_INCLUDED
#define FILESYSTEMH_INCLUDED 1

#include <gtk/gtk.h>
#include "metadata.h"

/* Mode flags for files and dirs */
#ifdef G_OS_WIN32
#define READONLY_FOPEN_STRING ("rb")
#define READPLUS_FOPEN_STRING ("rb+")
#define READONLY_FLAGS (O_RDONLY | O_BINARY)
#define WRITEONLY_FLAGS (O_WRONLY | O_BINARY)
#define CREATE_FILEMODE (S_IRUSR | S_IWUSR)
#define CREATE_DIRMODE (S_IRUSR | S_IWUSR | S_IXUSR)
#else
#define READONLY_FOPEN_STRING ("r")
#define READPLUS_FOPEN_STRING ("rb")
#define READONLY_FLAGS (O_RDONLY)
#define WRITEONLY_FLAGS (O_WRONLY)
#define CREATE_FILEMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define CREATE_DIRMODE (S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
#endif

/* Local type for the metadata read thread */
typedef struct
{
  GtkWidget *dialog;
  GtkWidget *label;
  GtkWidget *progress;
  listtype_t listtype;
  gchar *path;
  gboolean cancelled;
} dirfill_thread_arg_t;

/* Exported functions */
guint64 get_64bit_file_size(const gchar * const path);
gchar *filename_toutf8(const gchar * const instr);
gchar *filename_fromutf8(const gchar * const instr);
GList *get_metadata_dir(listtype_t listtype,
			gchar *path,
			dirfill_thread_arg_t *args);
void fill_in_dir(listtype_t listtype, gchar *path);

gchar *get_current_dir(void);
gboolean is_directory(gchar *dir);
void change_directory(gchar *dir);
gint delete_dir_contents(gchar *dir);
gint delete_files(GList *filelist);
gboolean clone_and_strip_tag(const gchar *const source,
			     const gchar * const dest,
			     const gchar * const codec);
gint get_prefsfile_fd(gboolean write);
void close_prefsfile(void);
void rewrite_prefsfile(gchar *prefs);
char *expand_path (char *src);
gboolean test_writeable(gchar *path);
gboolean create_directory(gchar *path);
void create_dirs(gchar *filename);
gboolean copy_file(gchar *source, gchar *dest);
void export_playlistfile(gchar *filename, gchar *formatstring, GList *playlist, gboolean m3ufile, gboolean plsfile);

#endif
