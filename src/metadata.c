/* metadata.c
   Metadata list stores and handler functions
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "metadata.h"
#include "util.h"

metadata_t *new_metadata_t(void)
{
  metadata_t *meta;

  meta = (metadata_t *) g_malloc(sizeof(metadata_t));
  meta->artist = NULL;
  meta->title = NULL;
  meta->album = NULL;
  meta->year = 0;
  meta->genre = NULL;
  meta->length = NULL;
  meta->size = 0;
  meta->codec = NULL;
  meta->trackno = 0;
  meta->protect = FALSE;
  meta->filename = NULL;
  meta->path = NULL;
  meta->folder = NULL;
  return meta;
}

void destroy_metadata_t(metadata_t *meta)
{
  if (meta->artist != NULL)
    g_free(meta->artist);
  if (meta->title != NULL)
    g_free(meta->title);
  if (meta->album != NULL)
    g_free(meta->album);
  if (meta->genre != NULL)
    g_free(meta->genre);
  if (meta->codec != NULL)
    g_free(meta->codec);
  if (meta->filename != NULL)
    g_free(meta->filename);
  if (meta->path != NULL)
    g_free(meta->path);
  if (meta->folder != NULL)
    g_free(meta->folder);
  g_free(meta);
}

metadata_t *clone_metadata_t(metadata_t const * const meta)
{
  metadata_t *clone = new_metadata_t();

  if (meta->artist != NULL)
    clone->artist = g_strdup(meta->artist);
  if (meta->title != NULL)
    clone->title = g_strdup(meta->title);
  if (meta->album != NULL)
    clone->album = g_strdup(meta->album);
  clone->year = meta->year;
  if (meta->genre != NULL)
    clone->genre = g_strdup(meta->genre);
  clone->length = meta->length;
  clone->size = meta->size;
  if (meta->codec != NULL)
    clone->codec = g_strdup(meta->codec);
  clone->trackno = meta->trackno;
  clone->protect = meta->protect;
  if (meta->filename != NULL)
    clone->filename = g_strdup(meta->filename);
  if (meta->path != NULL)
    clone->path = g_strdup(meta->path);
  if (meta->folder != NULL)
    clone->folder = g_strdup(meta->folder);
  return clone;
}

static gchar *metadata_string_escape(gchar *str)
{
  static gchar null_value[] = "(null)";

  if (str == NULL) {
    return null_value;
  }
  return str;
}

void dump_metadata_t(metadata_t const * const meta)
{
  g_print("\n---- Song metadata -----\n");
  g_print("Artist:    %s\n", metadata_string_escape(meta->artist));
  g_print("Title:     %s\n", metadata_string_escape(meta->title));
  g_print("Album:     %s\n", metadata_string_escape(meta->album));
  g_print("Year:      %" PRIguint32 "\n", meta->year);
  g_print("Genre:     %s\n", metadata_string_escape(meta->genre));
  g_print("Length:    %s\n", metadata_string_escape(meta->length));
  g_print("Size:      %lu bytes\n", meta->size);
  g_print("Codec:     %s\n", metadata_string_escape(meta->codec));
  g_print("Track#:    %" PRIguint32 "\n", meta->trackno);
  g_print("Protected: %s\n", meta->protect ? "YES" : "NO");
  g_print("Filename:  %s\n", metadata_string_escape(meta->filename));
  g_print("Path:      %s\n", metadata_string_escape(meta->path));
  g_print("Folder:    %s\n", metadata_string_escape(meta->folder));
}

static void strip_filename_illegal_chars(gchar * const str)
{
  guint i;

  for (i = 0; i < strlen(str); i++) {
    if (str[i] == G_DIR_SEPARATOR || str[i] == G_SEARCHPATH_SEPARATOR) {
      str[i] = '_';
    }
  }
}

/**
 * This function creates a metadata string to be used for
 * e.g. filenames using a template.
 * @param meta the metadata to use for composing the string
 * @param template a template string giving the metadata arrangement
 * @param filesystemsafe whether some illegal characters that cannot
 *        be used in the underlying filesystem should be stripped from
 *        the string.
 */
gchar *compose_metadata_string(metadata_t const * const meta, 
			       gchar const * const template, 
			       gboolean const filesystemsafe)
{
  gchar *tmp;
  gchar *tmpyear;
  gchar *tmptrackno;
  gchar *returnname;

  if (meta == NULL) {
    return NULL;
  }

  returnname = g_strdup(template);

  if (meta->artist) {
    tmp = g_strdup(meta->artist);
    if (filesystemsafe) {
      strip_filename_illegal_chars(tmp);
    }
    returnname = replacestring(returnname, "%a", tmp);
    g_free(tmp);
  } else {
    returnname = replacestring(returnname, "%a", "<Unknown>");
  }
  if (meta->title) {
    tmp = g_strdup(meta->title);
    if (filesystemsafe) {
      strip_filename_illegal_chars(tmp);
    }
    returnname = replacestring(returnname, "%t", tmp);
    g_free(tmp);
  } else {
    returnname = replacestring(returnname, "%t", "<Unknown>");
  }
  if (meta->album) {
    tmp = g_strdup(meta->album);
    if (filesystemsafe) {
      strip_filename_illegal_chars(tmp);
    }
    returnname = replacestring(returnname, "%b", tmp);
    g_free(tmp);
  } else {
    returnname = replacestring(returnname, "%b", "<Unknown>");
  }
  if (meta->genre) {
    tmp = g_strdup(meta->genre);
    if (filesystemsafe) {
      strip_filename_illegal_chars(tmp);
    }
    returnname = replacestring(returnname, "%g", tmp);
    g_free(tmp);
  } else {
    returnname = replacestring(returnname, "%g", "<Unknown>");
  }
  // The following does not contain filesystem-illegal characters
  tmptrackno = g_strdup_printf("%02u", meta->trackno);
  if (tmptrackno) {
    returnname = replacestring(returnname, "%n", tmptrackno);
  } else {
    returnname = replacestring(returnname, "%n", "00");
  }
  g_free(tmptrackno);
  tmpyear = g_strdup_printf("%" PRIguint32, meta->year);
  if (tmpyear)
    returnname = replacestring(returnname, "%y", tmpyear);
  else
    returnname = replacestring(returnname, "%y", "1900");
  g_free(tmpyear);

  return returnname;
}

/*****************************************************************************
 * These functions retrieve data from selections in metadata trees
 *****************************************************************************/

metadata_t *get_metadata_from_model(GtkTreeModel *model, 
				    GtkTreeIter *iter)
{
  metadata_t *meta = new_metadata_t();
  gtk_tree_model_get(model, iter,
		     TRACKLIST_ARTIST_COLUMN, &meta->artist,
		     TRACKLIST_TITLE_COLUMN, &meta->title,
		     TRACKLIST_ALBUM_COLUMN, &meta->album,
		     TRACKLIST_YEAR_COLUMN, &meta->year,
		     TRACKLIST_GENRE_COLUMN, &meta->genre,
		     TRACKLIST_LENGTH_COLUMN, &meta->length,
		     TRACKLIST_SIZE_COLUMN, &meta->size,
		     TRACKLIST_CODEC_COLUMN, &meta->codec,
		     TRACKLIST_TRACKNO_COLUMN, &meta->trackno,
		     TRACKLIST_PROTECTED_COLUMN, &meta->protect,
		     TRACKLIST_FILENAME_COLUMN, &meta->filename,
		     TRACKLIST_ID_COLUMN, &meta->path,
		     TRACKLIST_FOLDER_COLUMN, &meta->folder,
		     -1);
  return meta;
}

metadata_t *get_data_metadata_from_model(GtkTreeModel *model, 
					 GtkTreeIter *iter)
{
  metadata_t *meta = new_metadata_t();
  gtk_tree_model_get(model, iter,
		     FILELIST_FILENAME_COLUMN, &meta->filename,
		     FILELIST_SIZE_COLUMN, &meta->size,
		     FILELIST_ID_COLUMN, &meta->path,
		     FILELIST_FOLDER_COLUMN, &meta->folder,
		     FILELIST_EXTENSION_COLUMN, &meta->codec,
		     -1);
  return meta;
}


void add_metadata_to_model(metadata_t const * const meta, 
			   listtype_t const listtype)
{
  GtkListStore *liststore = NULL;  /* initialize to shut up GCC */
  GtkTreeIter listiter;

  if (listtype == HD_LIST || listtype == JB_LIST) {
    if (listtype == HD_LIST)
      liststore = transfer_widgets.hdliststore;
    else if (listtype == JB_LIST)
      liststore = transfer_widgets.jbliststore;
  
    gtk_list_store_append (liststore, &listiter);
    gtk_list_store_set (liststore, &listiter,
			TRACKLIST_ARTIST_COLUMN, meta->artist,
			TRACKLIST_TITLE_COLUMN, meta->title,
			TRACKLIST_ALBUM_COLUMN, meta->album,
			TRACKLIST_YEAR_COLUMN, meta->year,
			TRACKLIST_GENRE_COLUMN, meta->genre,
			TRACKLIST_LENGTH_COLUMN, meta->length,
			TRACKLIST_SIZE_COLUMN, meta->size,
			TRACKLIST_CODEC_COLUMN, meta->codec,
			TRACKLIST_TRACKNO_COLUMN, meta->trackno,
			TRACKLIST_PROTECTED_COLUMN, meta->protect,
			TRACKLIST_FILENAME_COLUMN, meta->filename,
			TRACKLIST_ID_COLUMN, meta->path,
			TRACKLIST_FOLDER_COLUMN, meta->folder,
			-1);
  }

  else if (listtype == HDDATA_LIST || listtype == JBDATA_LIST) {
    if (listtype == HDDATA_LIST)
      liststore = data_widgets.hdliststore;
    else if (listtype == JBDATA_LIST)
      liststore = data_widgets.jbliststore;
  
    gtk_list_store_append (liststore, &listiter);
    gtk_list_store_set (liststore, &listiter,
			FILELIST_FILENAME_COLUMN, meta->filename,
			FILELIST_SIZE_COLUMN, meta->size,
			FILELIST_ID_COLUMN, meta->path,
			FILELIST_FOLDER_COLUMN, meta->folder,
			FILELIST_EXTENSION_COLUMN, meta->codec,
			-1);
  }
}

static void
select_foreach_xfer(GtkTreeModel *model,
		    GtkTreePath *path,
		    GtkTreeIter *iter,
		    GList **list)
{
  metadata_t *meta = get_metadata_from_model(model, iter);
  *list = g_list_append (*list, meta);
}

static void
select_foreach_data(GtkTreeModel *model,
		    GtkTreePath *path,
		    GtkTreeIter *iter,
		    GList **list)
{
  metadata_t *meta = get_data_metadata_from_model(model, iter);
  *list = g_list_append (*list, meta);
}


static GtkTreeView*
get_view(listtype_t listtype)
{
  if (listtype == HD_LIST)
    return GTK_TREE_VIEW(transfer_widgets.hdlistview);
  else if (listtype == JB_LIST)
    return GTK_TREE_VIEW(transfer_widgets.jblistview);
  else if (listtype == HDDATA_LIST)
    return GTK_TREE_VIEW(data_widgets.hdlistview);
  else if (listtype == JBDATA_LIST)
    return GTK_TREE_VIEW(data_widgets.jblistview);
  else
    return NULL;
}

static GtkListStore*
get_store(listtype_t listtype)
{
  if (listtype == HD_LIST)
    return transfer_widgets.hdliststore;
  else if (listtype == JB_LIST)
    return transfer_widgets.jbliststore;
  else if (listtype == HDDATA_LIST)
    return data_widgets.hdliststore;
  else if (listtype == JBDATA_LIST)
    return data_widgets.jbliststore;
  else
    return NULL;
}


GtkTreeSelection *get_metadata_selection(listtype_t listtype)
{
  GtkTreeSelection *select = NULL;

  select = gtk_tree_view_get_selection(get_view(listtype));
  return select;
}

/* Returns the paths for all selected rows so they may be processed */
static GList *get_metadata_selection_paths(listtype_t listtype)
{
  GtkTreeSelection *select = get_metadata_selection(listtype);
  GtkTreeModel *model = GTK_TREE_MODEL(get_store(listtype));
  GList *selectlist = gtk_tree_selection_get_selected_rows(select, &model);

  return selectlist;
}

GList *get_all_metadata_from_selection(listtype_t listtype)
{
  GtkTreeSelection *select;
  GList *metadatas = NULL;

  select = get_metadata_selection(listtype);
  if (select == NULL)
    return NULL;
  if (listtype == HD_LIST || listtype == JB_LIST) {
    gtk_tree_selection_selected_foreach(select,
					(GtkTreeSelectionForeachFunc) select_foreach_xfer,
					(gpointer) &metadatas);
  } else if (listtype == HDDATA_LIST || listtype == JBDATA_LIST) {
    gtk_tree_selection_selected_foreach(select,
					(GtkTreeSelectionForeachFunc) select_foreach_data,
					(gpointer) &metadatas);
  } else if (listtype == PL_TREE) {
    g_print("Illegal call to retrieve playlist metadata!");
  }
  return metadatas;
}

void destroy_metalist(GList *metalist)
{
  GList *tmplist = g_list_first(metalist);
  while(tmplist != NULL) {
    metadata_t *meta = (metadata_t *) tmplist->data;
    destroy_metadata_t(meta);
    tmplist = g_list_next(tmplist);
  }
  g_list_free(metalist);
}

metadata_t *get_first_metadata_from_selection(listtype_t listtype)
{
  GList *metadatas = get_all_metadata_from_selection(listtype);
  GList *tmplist;
  metadata_t *retmeta;

  if (metadatas == NULL)
    return NULL;
  tmplist = g_list_first(metadatas);
  retmeta = (metadata_t *) tmplist->data;
  tmplist = g_list_next(tmplist);
  while(tmplist != NULL) {
    metadata_t *meta = (metadata_t *) tmplist->data;
    destroy_metadata_t(meta);
    tmplist = g_list_next(tmplist);
  }
  g_list_free(metadatas);
  return retmeta;
}

/*****************************************************************************
 * These functions destroy data from selections in metadata trees
 *****************************************************************************/

void remove_selected(listtype_t listtype)
{
  GList *selection;

  selection = get_metadata_selection_paths(listtype);
  while (selection != NULL) {
    GList *tmp;
    GtkTreeIter iter;

    tmp = g_list_first(selection);
    /* Get the iterator for the first row and remove it */
    if (gtk_tree_model_get_iter(GTK_TREE_MODEL(get_store(listtype)), &iter, 
				(GtkTreePath *) tmp->data)) {
      gtk_list_store_remove(get_store(listtype), &iter);
    }
    /*
     * We have to free this selection list and get the
     * paths again, because when we removed the line
     * all path references changed and are now invalid.
     */
    g_list_foreach (selection, (GFunc) gtk_tree_path_free, NULL);
    g_list_free(selection);
    selection = get_metadata_selection_paths(listtype);
  }
}

gint sort_iter_compare_func(GtkTreeModel *model,
     GtkTreeIter  *a,
     GtkTreeIter  *b,
     gpointer      userdata)
{
    gint sortcol = GPOINTER_TO_INT(userdata);
    gint ret = 0;
    gchar *name1 = NULL, *name2 = NULL;
    gchar *subname1 = NULL, *subname2 = NULL;
    guint  subint1 = 0, subint2 = 0;
    gtk_tree_model_get(model, a, sortcol, &name1, -1);
    gtk_tree_model_get(model, b, sortcol, &name2, -1);

    switch (sortcol)
    {
      case TRACKLIST_ARTIST_COLUMN:
      {
        gtk_tree_model_get(model, a, TRACKLIST_TITLE_COLUMN, &subname1, -1);
        gtk_tree_model_get(model, b, TRACKLIST_TITLE_COLUMN, &subname2, -1);
	break;
      }
      case TRACKLIST_TITLE_COLUMN:
      {
        gtk_tree_model_get(model, a, TRACKLIST_ARTIST_COLUMN, &subname1, -1);
        gtk_tree_model_get(model, b, TRACKLIST_ARTIST_COLUMN, &subname2, -1);
	break;
      }
      case TRACKLIST_ALBUM_COLUMN:
      {
        gtk_tree_model_get(model, a, TRACKLIST_TRACKNO_COLUMN, &subint1, -1);
        gtk_tree_model_get(model, b, TRACKLIST_TRACKNO_COLUMN, &subint2, -1);
        gtk_tree_model_get(model, a, TRACKLIST_TITLE_COLUMN, &subname1, -1);
        gtk_tree_model_get(model, b, TRACKLIST_TITLE_COLUMN, &subname2, -1);
      }
    }
    if (name1 != NULL || name2 != NULL)
       ret = g_utf8_collate(name1,name2);
    else if (!(name1 == NULL && name2 == NULL))
       ret = (name1 == NULL ? -1 : 1);

    if (ret == 0)
       ret = subint1 - subint2;

    if (ret == 0)
    {
      if (subname1 != NULL || subname2 != NULL)
         ret = g_utf8_collate(subname1,subname2);
      else if (!(name1 == NULL && name2 == NULL))
         ret = (subname1 == NULL ? -1 : 1);
    }
    if (name1 != NULL) g_free(name1);
    if (name2 != NULL) g_free(name2);
    if (subname1 != NULL) g_free(subname1);
    if (subname2 != NULL) g_free(subname2);
    return ret;
}

static GtkListStore *create_hdliststore(void)
{
  GtkListStore *hdliststore;
  GtkTreeSortable *sortable;

  hdliststore = gtk_list_store_new (TRACKLIST_N_COLUMNS,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_BOOLEAN,
				    G_TYPE_STRING,
				    G_TYPE_STRING, /*ID*/
				    G_TYPE_STRING /* Folder */
				    );

  sortable = GTK_TREE_SORTABLE(hdliststore);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_ARTIST_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_ARTIST_COLUMN), NULL);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_TITLE_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_TITLE_COLUMN), NULL);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_ALBUM_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_ALBUM_COLUMN), NULL);

  return hdliststore;
}

static GtkListStore *create_jbliststore(void)
{
  GtkListStore *jbliststore;
  GtkTreeSortable *sortable;

  jbliststore = gtk_list_store_new (TRACKLIST_N_COLUMNS,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_BOOLEAN,
				    G_TYPE_STRING,
				    G_TYPE_STRING, /*ID*/
				    G_TYPE_STRING /* Folder */
				    );

  sortable = GTK_TREE_SORTABLE(jbliststore);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_ARTIST_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_ARTIST_COLUMN), NULL);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_TITLE_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_TITLE_COLUMN), NULL);

  gtk_tree_sortable_set_sort_func(sortable, 
	TRACKLIST_ALBUM_COLUMN, sort_iter_compare_func,
        GINT_TO_POINTER(TRACKLIST_ALBUM_COLUMN), NULL);

  return jbliststore;
}

static GtkListStore *create_hddataliststore(void)
{
  GtkListStore *hddataliststore;

  hddataliststore = gtk_list_store_new (FILELIST_N_COLUMNS,
					G_TYPE_STRING, /*Filename */
					G_TYPE_INT, /* Size */
					G_TYPE_STRING, /*ID*/
					G_TYPE_STRING, /* Folder */
					G_TYPE_STRING /* Codec */
				    );
  return hddataliststore;
}

static GtkListStore *create_jbdataliststore(void)
{
  GtkListStore *jbdataliststore;

  jbdataliststore = gtk_list_store_new (FILELIST_N_COLUMNS,
					G_TYPE_STRING, /*Filename */
					G_TYPE_INT, /* Size */
					G_TYPE_STRING, /*ID*/
					G_TYPE_STRING, /* Folder */
					G_TYPE_STRING /* Codec */
				    );
  return jbdataliststore;
}

static gint compare_lengths(gchar * length1, gchar * length2)
{
  gint retval;
  if (!length2)
    retval = (length1 != NULL);
  else if (!length1)
    retval = -1;
  else
  {
    gchar * sep;
    gchar ** cmp1;
    gchar ** cmp2;
    gint min1, sec1, min2, sec2;
    cmp1 = g_strsplit(length1, ":", 2);
    cmp2 = g_strsplit(length2, ":", 2);
    min1 = g_ascii_strtoll(cmp1[0], 0, 10);
    sec1 = g_ascii_strtoll(cmp1[1], 0, 10);
    min2 = g_ascii_strtoll(cmp2[0], 0, 10);
    sec2 = g_ascii_strtoll(cmp2[1], 0, 10);
    if (min1 < min2)
      retval = -1;
    else if (min1 > min2)
      retval = 1;
    else
    {
      if (sec1 < sec2)
        retval = -1;
      else if (sec1 > sec2)
        retval = 1;
      else
        retval = 0;
    }
    g_strfreev(cmp1);
    g_strfreev(cmp2);
  }
  return retval;
}


/* This intercompare function sorts the directories on top of
 * the files in the harddisk list view. (It is not used at all
 * for the jukebox view.)
 */
static gint hdlist_compare_func (GtkTreeModel *model,
				 GtkTreeIter *a,
				 GtkTreeIter *b,
				 gpointer user_data)
{
  GType coltype;
  gint retval = 0;
  gint sort_column_id;
  GtkSortType order;
  gboolean sort;
  guint size1;
  guint size2;

  /* Check validity of iterators */
  if (a == NULL || b == NULL ||
      a->user_data == NULL || b->user_data == NULL ||
      a->stamp == 0 ||
      b->stamp == 0) {
    g_print("Bad iterator!\n");
    return 0;
  }

  sort = gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(model),
						&sort_column_id,
						&order);
  if (!sort) {
    return 0;
  }
  gtk_tree_model_get(model, a, TRACKLIST_SIZE_COLUMN, &size1, -1);
  gtk_tree_model_get(model, b, TRACKLIST_SIZE_COLUMN, &size2, -1);
  coltype = gtk_tree_model_get_column_type(model,
					   sort_column_id);
  /* First, if the size is zero, it should be on top
   * (directory!) */
  if (size1 == 0 && size2 != 0) {
    if (order == GTK_SORT_ASCENDING)
      return -1;
    else
      return 1;
  } else if (size2 == 0 && size1 != 0) {
    if (order == GTK_SORT_ASCENDING)
      return 1;
    else
      return -1;
  }
  /* And directories are ALWAYS internally sorted on directory name! */
  else if(size1 == 0 && size2 == 0) {
    gchar *text1;
    gchar *text2;

    gtk_tree_model_get(model, a, TRACKLIST_ARTIST_COLUMN, &text1, -1);
    gtk_tree_model_get(model, b, TRACKLIST_ARTIST_COLUMN, &text2, -1);
    if (!text2)
      retval = (text1 != NULL);
    else if (!text1)
      retval = -1;
    else {
      retval = g_strcasecmp(text1, text2);
    }
    g_free(text1);
    g_free(text2);
    if (order == GTK_SORT_DESCENDING)
      retval = -retval;
  } else {
    switch(sort_column_id)
    {
      case TRACKLIST_ALBUM_COLUMN:
      case TRACKLIST_ARTIST_COLUMN:
      case TRACKLIST_TITLE_COLUMN:
      {
        retval = sort_iter_compare_func(model, a, b, GINT_TO_POINTER(sort_column_id));
      }
      break;
      case TRACKLIST_LENGTH_COLUMN:
      {
           gchar *text1;
           gchar *text2;
           gtk_tree_model_get(model, a, sort_column_id, &text1, -1);
           gtk_tree_model_get(model, b, sort_column_id, &text2, -1);
           retval = compare_lengths(text1, text2);
           g_free(text1);
           g_free(text2);
      }
      break;
      default:
      {
        /* Dynamically decide how to handle different text types */
        if (coltype == G_TYPE_STRING) {
           gchar *text1;
           gchar *text2;
      
           gtk_tree_model_get(model, a, sort_column_id, &text1, -1);
           gtk_tree_model_get(model, b, sort_column_id, &text2, -1);
           if (!text2)
	      retval = (text1 != NULL);
           else if (!text1)
	      retval = -1;
           else 
	      retval = g_strcasecmp(text1, text2);
           g_free(text1);
           g_free(text2);
        } else if (coltype == G_TYPE_INT) {
          guint int1;
          guint int2;

          gtk_tree_model_get(model, a, sort_column_id, &int1, -1);
          gtk_tree_model_get(model, b, sort_column_id, &int2, -1);
          if (int1 < int2)
	    retval = -1;
          else if (int1 > int2)
	    retval = 1;
          else
	    retval = 0;
	}
      }
    }
  }
  return retval;
}

/* This intercompare function sorts the directories on top of
 * the files in the harddisk list view. (It is not used at all
 * for the jukebox view.)
 */
static gint datalist_compare_func (GtkTreeModel *model,
				   GtkTreeIter *a,
				   GtkTreeIter *b,
				   gpointer user_data)
{
  GType coltype;
  gint retval = 0;
  gint sort_column_id;
  GtkSortType order;
  gboolean sort;
  guint size1;
  guint size2;

  /* Check validity of iterators */
  if (a == NULL || b == NULL ||
      a->user_data == NULL || b->user_data == NULL ||
      a->stamp == 0 ||
      b->stamp == 0) {
    g_print("Bad iterator!\n");
    return 0;
  }

  sort = gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(model),
						&sort_column_id,
						&order);
  if (!sort) {
    return 0;
  }
  gtk_tree_model_get(model, a, FILELIST_SIZE_COLUMN, &size1, -1);
  gtk_tree_model_get(model, b, FILELIST_SIZE_COLUMN, &size2, -1);
  coltype = gtk_tree_model_get_column_type(model,
					   sort_column_id);
  /* First, if the size is zero, it should be on top
   * (directory!) */
  if (size1 == 0 && size2 != 0) {
    if (order == GTK_SORT_ASCENDING)
      return -1;
    else
      return 1;
  } else if (size2 == 0 && size1 != 0) {
    if (order == GTK_SORT_ASCENDING)
      return 1;
    else
      return -1;
  }
  /* And directories are ALWAYS internally sorted on directory name! */
  else if(size1 == 0 && size2 == 0) {
    gchar *text1;
    gchar *text2;

    gtk_tree_model_get(model, a, FILELIST_FILENAME_COLUMN, &text1, -1);
    gtk_tree_model_get(model, b, FILELIST_FILENAME_COLUMN, &text2, -1);
    if (!text2)
      retval = (text1 != NULL);
    else if (!text1)
      retval = -1;
    else {
      retval = g_strcasecmp(text1, text2);
    }
    g_free(text1);
    g_free(text2);
    if (order == GTK_SORT_DESCENDING)
      retval = -retval;
  } else {
    /* Dynamically decide how to handle different text types */
    if (coltype == G_TYPE_STRING) {
      gchar *text1;
      gchar *text2;
      
      gtk_tree_model_get(model, a, sort_column_id, &text1, -1);
      gtk_tree_model_get(model, b, sort_column_id, &text2, -1);
      if (!text2)
	retval = (text1 != NULL);
      else if (!text1)
	retval = -1;
      else 
	retval = g_strcasecmp(text1, text2);
      g_free(text1);
      g_free(text2);
    } else if (coltype == G_TYPE_INT) {
      guint int1;
      guint int2;

      gtk_tree_model_get(model, a, sort_column_id, &int1, -1);
      gtk_tree_model_get(model, b, sort_column_id, &int2, -1);
      if (int1 < int2)
	retval = -1;
      else if (int1 > int2)
	retval = 1;
      else
	retval = 0;
    }
  }
  return retval;
}


void view_and_sort_list_store(listtype_t listtype)
{
  gint i;

  if (gnomad_debug != 0) {
    g_print("Enter view_and_sort_list_store(%d)...\n", (gint) listtype);
  }

  /* All should be found now */
  gtk_tree_view_set_model (GTK_TREE_VIEW (get_view(listtype)),
                           GTK_TREE_MODEL (get_store(listtype)));

  if (listtype == HD_LIST) {
    for (i = TRACKLIST_ARTIST_COLUMN; i < TRACKLIST_ARTIST_COLUMN + TRACKLIST_N_COLUMNS; i++) {
      gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(transfer_widgets.hdliststore),
				      i,
				      hdlist_compare_func,
				      NULL,
				      NULL);
    }
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(transfer_widgets.hdliststore),
					 TRACKLIST_ARTIST_COLUMN,
					 GTK_SORT_ASCENDING);
  } else if (listtype == JB_LIST) {
    for (i = TRACKLIST_ARTIST_COLUMN; i < TRACKLIST_ARTIST_COLUMN + TRACKLIST_N_COLUMNS; i++) {
      gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(transfer_widgets.jbliststore),
				      i,
				      hdlist_compare_func,
				      NULL,
				      NULL);
    }
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(transfer_widgets.jbliststore),
					 TRACKLIST_ARTIST_COLUMN,
					 GTK_SORT_ASCENDING);
  } else if (listtype == HDDATA_LIST) {
    for (i = FILELIST_FILENAME_COLUMN; i < FILELIST_FILENAME_COLUMN + FILELIST_N_COLUMNS; i++) {
      gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(data_widgets.hdliststore),
				      i,
				      datalist_compare_func,
				      NULL,
				      NULL);
    }
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(data_widgets.hdliststore),
					 FILELIST_FILENAME_COLUMN,
					 GTK_SORT_ASCENDING);
  } else if (listtype == JBDATA_LIST) {
    for (i = FILELIST_FILENAME_COLUMN; i < FILELIST_FILENAME_COLUMN + FILELIST_N_COLUMNS; i++) {
      gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(data_widgets.jbliststore),
				      i,
				      datalist_compare_func,
				      NULL,
				      NULL);
    }
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(data_widgets.jbliststore),
					 FILELIST_FILENAME_COLUMN,
					 GTK_SORT_ASCENDING);
  }
}

void recreate_list_store(listtype_t listtype)
{
  clear_list_store(listtype);
  g_object_unref(get_store(listtype));
  if (listtype == HD_LIST)
    transfer_widgets.hdliststore = create_hdliststore();
  else if (listtype == JB_LIST)
    transfer_widgets.jbliststore = create_jbliststore();
  else if (listtype == HDDATA_LIST)
    data_widgets.hdliststore = create_hddataliststore();
  else if (listtype == JBDATA_LIST)
    data_widgets.jbliststore = create_jbdataliststore();  
}

void create_list_stores(void)
{
  /* The list stores */
  GtkTreeStore *pltreestore;

  pltreestore = gtk_tree_store_new(PLIST_N_COLUMNS,
				   G_TYPE_STRING, /* name */
				   G_TYPE_STRING, /* artist */
				   G_TYPE_STRING, /* title */
				   G_TYPE_STRING, /* length */
				   G_TYPE_STRING, /* Playlist ID */
				   G_TYPE_STRING  /* Song ID */
				   );

  transfer_widgets.hdliststore = create_hdliststore();
  transfer_widgets.jbliststore = create_jbliststore();
  playlist_widgets.pltreestore = pltreestore;
  data_widgets.hdliststore = create_hddataliststore();
  data_widgets.jbliststore = create_jbdataliststore();
}

void clear_list_store(listtype_t listtype)
{
  if (listtype == HD_LIST)
    gtk_list_store_clear(GTK_LIST_STORE(transfer_widgets.hdliststore));
  else if (listtype == JB_LIST)
    gtk_list_store_clear(GTK_LIST_STORE(transfer_widgets.jbliststore));
  else if (listtype == HDDATA_LIST)
    gtk_list_store_clear(GTK_LIST_STORE(data_widgets.hdliststore));
  else if (listtype == JBDATA_LIST)
    gtk_list_store_clear(GTK_LIST_STORE(data_widgets.jbliststore));
  else if (listtype == PL_TREE)
    gtk_tree_store_clear(GTK_TREE_STORE(playlist_widgets.pltreestore));
}
