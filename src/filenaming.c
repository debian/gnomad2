/* filenaming.c
   Naming of files stored to and read from harddisk
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

/*
 * I have put all functions that deal with filename formatting
 * and tagging of (eg) mp3 files into this file. If you are
 * displeased with how GNOMAD handles tagging or filenaming, this
 * is where you start hacking. Don't forget to send in any smart
 * pathces to me. /Linus
 */

#include "common.h"
#include "filenaming.h"
#include "prefs.h"
#include "util.h"
#include "metadata.h"
#include <ctype.h>

gchar *chomp_strings[] =
{
  /* What a decoder is it that puts !s at the end of every filename
   * and a N01Songtitle thing at the beginning of the file??? */
  "!s",
  /* Beatforge and friends - put your SIGs in the ID3 tag plz */
  " bf",
  " bftop",
  " xtd bftop",
  " nbd",
  " 12i",
  " 12inch",
  " oz",
  " wax",
  " atm",
  " we",
  " ii",
  /* From some stuid man that signifies "disks" with d1 for disc 1 etc */
  " d1",
  " d2",
  " d3",
  " d4",
  " d5",
  /* Remove trailing dash */
  "-"
};

gchar *replacement_strings[] =
{
  /* Some obvious mistakes I've found... 
   * add all things you find erroneously named in here. */
  "Dj ","DJ ",
  "Djs ","DJs ",
  "DJ - ","DJ ",
  "Ac - Dc ","AC-DC ",
  "Acdc ","AC-DC ",
  "Ebtg ","Everything But the Girl ",
  "Bjrk ","Bj�rk ",
  "Bjrn ","Bj�rn ",
  "Beastieboys ","Beastie Boys ",
  "C - Tec","C-Tec",
  "Ccr ","CCR ",
  "Atr ","ATR ",
  "Abba ","ABBA ",
  "Rmb ","RMB ",
  "Zz Top ","ZZ Top ",
  "A - Ha ","A-Ha ",
  "B - Charme ","B-Charme ",
  "C - Block ","C-Block ",
  "D - Tune ","D-Tune ",
  "E - Type ","E-Type ",
  "X - Perience ","X-Perience ",
  "Dee - Lite ","Dee-Lite ",
  "Run - Dmc ","Run-DMC ",
  "Run Dmc ","Run-DMC ",
  "Ann - Lie ","Ann-Lie ",
  "Cajsa - Stina ","Cajsa-Stina ",
  " - Rmx"," RMX",
  " Rmx"," RMX",
  " - Remix"," RMX",
  " Remix"," RMX",
  " Ft. "," ft ",
  " Feat "," ft ",
  " Feat. "," ft ",
};

const gint chomp_string_count = 
((gint)(sizeof(chomp_strings)/sizeof(gchar*))-1);
const gint replacement_string_count = 
((gint)(sizeof(replacement_strings)/sizeof(gchar*))-1);

/* Adds an underscore before the file extension. */
gchar *add_underscore(gchar *str)
{
  gchar *res;
  gchar *res2;
  gchar *ext;
  guint c = strlen(str);
  while (str[c] != '.') c--;
  res = g_strndup(str,c);
  ext = g_strdup(str+c);
  res2 = g_strconcat(res, "_", ext, NULL);
  g_free(str);
  g_free(ext);
  g_free(res);
  return res2;
}

/**
 * This routine composes a suitable filename for a track file
 * based on user configuration.
 * @param meta the metadata to use for constructing the filename
 * @return a UTF-8 string with the desired filename
 */
gchar *compose_filename(metadata_t *meta)
{
  gchar *returnname;
  gchar *template;
  gchar *tmpcodec;

  if (get_prefs_use_origname()) {
    if (meta->filename != NULL) {
      if (strlen(meta->filename) > 1) {
	return g_strdup(meta->filename);
      }
    }
  }

  /* This is also the default if no filename exists in the metadata */
  template = get_prefs_filenameformat();
  returnname = compose_metadata_string(meta, template, TRUE);
  g_free(template);
  returnname = stringcat(returnname, ".");
  tmpcodec = g_utf8_strdown(meta->codec,-1);
  returnname = stringcat(returnname, tmpcodec);
  g_free(tmpcodec);
  /* g_print("Returnname: %s\n", returnname); */
  return returnname;
}

static gint number_of_dashes(const gchar *string)
{
  const gchar *tmp;
  gint no = 0;
  
  tmp = string;
  while (*tmp)
    {
      if (*tmp == '-')
	no ++;
      tmp ++;
    }
  return no;
}

static gchar *kill_tracknumber(const gchar *string)
{
  /* If something beginning with two figures is followed
   * by something that is not a figure, we assume it is
   * a track number and NUKE IT. */
  gchar *tmp;

  if (strlen(string) < 4)
    return g_strdup(string);
  if (g_ascii_isdigit(string[0]) &&
      g_ascii_isdigit(string[1]) &&
      !g_ascii_isdigit(string[2]) &&
      string[2] != '\'') { // This last condition is for year albums like "70's".
    tmp = g_strdup(string+2);
    g_strstrip(tmp);
    return tmp;
  }
  return g_strdup(string);
}

static void stripdblspaces(gchar *string)
{
  /* Remove any double spaces in the string 
   * (does not deallocate memory) */
  gchar *tmp;
  gchar *tmp2;
  
  if (strlen(string) < 2)
    return;
  tmp = string + 1;
  while (*tmp)
    {
      if (*tmp == ' ' && *(tmp-1) == ' ') {
	tmp2 = tmp + 1;
	while (*tmp2)
	  {
	    *(tmp2 - 1) = *tmp2;
	    tmp2 ++;
	  }
	*(tmp2 - 1) = '\0';
      }
      tmp ++;
    }
  return;
}

static void capitalize(gchar *string)
{
  /* Markov process that capitalizes every letter 
   * in the beginning of a word - hm if you're a
   * communist, perhaps you don't like the name
   * of this function? */
  gchar *tmp;
  
  if (strlen(string) < 2)
    return;
  string[0] = toupper(string[0]);
  tmp = string + 1;
  while (*tmp)
    {
      if (*(tmp - 1) == ' ')
	*tmp = toupper(*tmp);
      tmp ++;
    }
}

static gchar hex_to_char(gchar c)
{
  if (c >= '0' && c <= '9')
    return (c - '0');
  if (c >= 'A' && c <= 'F')
    return (c - 'A' + 10);
  if (c >= 'a' && c <= 'f')
    return (c - 'a' + 10);
  return '\0';
}

/* Performs on-the-fly URL decoding */
static void url_decode(gchar *string)
{
  gchar *sp;

  if (!string)
    return;
  sp = string;
  while(*sp)
    {
      if (*sp == '%') {
	gchar *tmp;
	gchar tmpchar;

	tmp = sp;
	sp ++;
	tmpchar = 16 * hex_to_char(*sp);
	sp ++;
	tmpchar += hex_to_char(*sp);
	sp = tmp;
	/* Only deURLencode stuff that makes sense */
	if (tmpchar) {
	  *sp = tmpchar;
	  tmp += 3;
	  while (*tmp)
	    {
	      *(tmp-2) = *tmp;
	      tmp ++;
	    }
	  *(tmp-2) = '\0';
	}
      }
      sp ++;
    }
}

static gchar *remove_crap_from_filename(gchar *instring)
{
  gint i;
  gchar *string;

  /* Return on empty string */
  if (!instring)
    return instring;
  /* Make a lowercase working copy */
  string = g_utf8_strdown(instring,-1);
  g_free(instring);
  /* Remove any underscores */
  replacechar(string, '_', ' ');
  /* Remove URL encoding present in some MP3 file
   * I've seen (on my CDs :) */
  url_decode(string);
  /* Find some clear candidates of junk at the
   * end of the filename and kill 'em */
  for (i = 0; i < chomp_string_count; i ++) {
    if (!strcmp(chomp_strings[i],
		string+strlen(string)-strlen(chomp_strings[i]))) {
      string[strlen(string)-strlen(chomp_strings[i])] = '\0';
    }
  }
  /* Another fix for the stupid encoder that names all files
   * insanely and puts !s at the end of then filename */
  if (string[0] == 'n' && strlen(string) > 4) {
    static gchar tmp[3];
    gchar *tmp2;

    tmp[0] = string[1];
    tmp[1] = string[2];
    tmp[2] = '\0';
    if (is_a_number(tmp)) {
      tmp2 = g_strdup(string+3);
      g_free(string);
      string = tmp2;
    }
  }
  /* Finally trim and return it */
  g_strstrip(string);
  return string;
}

static gchar *remove_crap_from_dirname(gchar *string)
{
  /* Remove any underscores */
  replacechar(string, '_', ' ');  
  return string;
}

static gchar *kill_squarebrackets(gchar *string)
{
  /* Some people insist on naming their files:
   * [Artist name] Trackname - remove such crap
   * and replace with the more common
   * Artist name - Trackname */
  if (*string == '[') {
    gchar *tmp;
    gchar *tmp2;
    
    tmp = string;
    while(*tmp && *tmp != ']')
      tmp ++;
    if (!tmp)
      return string+1;	// no matching ']': just strip '['
    *tmp = '\0';
    tmp2 = g_strdup(string+1);
    tmp2 = stringcat(tmp2, " - ");
    tmp2 = stringcat(tmp2, tmp+1);
    g_free(string);
    return tmp2;
  } else {
    return string;
  }
}

static gchar *beautify_string(gchar *instring)
{
  gint i;
  gchar *string = instring;

  /* See comment in this function for details */
  string = kill_squarebrackets(string);
  /* Obvious misuse of dashes */
  if (number_of_dashes(string) > 3)
    replacechar(string, '-', ' ');
  /* Kill that stoopid paranthesis indenting */
  string = replacestring(string,"( ","(");
  string = replacestring(string," )",")");
  string = replacestring(string,"[ ","[");
  string = replacestring(string," ]","]");
  /* Lighten crunched parantheses */
  string = replacestring(string,")[",") [");
  string = replacestring(string,"](","] (");
  /* Fix up spacing (OK this is crude too) */
  string = replacestring(string,"("," (");
  string = replacestring(string,")",") ");
  string = replacestring(string,"["," [");
  string = replacestring(string,"]","] ");
  /* This is a case of insanely genious programming */
  string = replacestring(string,"--","-");
  string = replacestring(string,"-"," - ");
  /* So it corrects its own mistakes (even cruder) */
  string = replacestring(string,"  ("," (");
  string = replacestring(string,")  ",") ");
  string = replacestring(string,"  ["," [");
  string = replacestring(string,"]  ","] ");
  string = replacestring(string,"] .","].");
  string = replacestring(string,") .",").");
  /* Replace all kind of double spaces */
  stripdblspaces(string);
  /* Remove space in the end or beginning of string */
  g_strstrip(string);
  /* We want to capitalize the first letter in every word as
   * is common in titles, and we're dealing with titles
   * most definately */
  capitalize(string);
  /* OK so things might become a 'lil bit TOO proper, and we
   * fix it by hardwiring a bit of English grammar. As every kid
   * knows, in titles all words EXCEPT prepositions, especially
   * those with one syllable, are NOT written with capital letters.
   * The words "and" and "but" are also exceptions. I didnt include
   * the word "on" because it can have different meanings... */
  string = replacestring(string," The "," the ");
  string = replacestring(string," To "," to ");
  string = replacestring(string," At "," at ");
  string = replacestring(string," For "," for ");
  string = replacestring(string," Of "," of ");
  string = replacestring(string," In "," in ");
  string = replacestring(string," On Top Of "," On top of ");
  string = replacestring(string," By "," by ");
  string = replacestring(string," From "," from ");
  string = replacestring(string," With "," with ");
  string = replacestring(string," And "," and ");
  string = replacestring(string," But "," but ");
  for (i = 0; i < replacement_string_count; i+=2) {
    string = replacestring(string, 
			   replacement_strings[i],
			   replacement_strings[i+1]);
  }
  return string;
}

static gchar *get_artist (gchar *dir, gchar *file)
{
  gchar **tmp;
  gchar *artist = NULL;

  if (gnomad_debug != 0) {
    g_print("get_artist()\n");
  }
  tmp = g_strsplit(dir, " - ", 0);
  /* If the directory is named "Foo - Bar", we presume
   * Foo is the artist, and Bar is the album title */
  if (vectorlength(tmp) > 1) {
    artist = g_strdup(*tmp);
    g_strfreev(tmp);
  } else {
    gchar *file2 = kill_tracknumber(file);
    g_strfreev(tmp);
    tmp = g_strsplit(file2, " - ", 0);
    /* Else if the file is named "0x - Foo - Bar", we assume
     * Foo is the artist name */
    if (vectorlength(tmp) > 2) {
      if (is_a_number(tmp[0])) {
        artist = g_strdup(tmp[1]);
      } else {
    /* Else if the file is named "Foo - 0x - Bar", we assume
     * Foo is the artist name */
        artist = g_strdup(tmp[0]);
      }
    /* Else if the file is named "Foo - Bar", we assume
     * Foo is the artist name */
    } else if (vectorlength(tmp) > 1) {
      artist = g_strdup(*tmp);
    } else {
      /* Otherwise we will use the directory name as it is,
       * assuming this is the artist is the best we can do... */
      artist = g_strdup(dir);
    }
    g_strfreev(tmp);
    g_free(file2);
  }  
  return artist;
}

static gchar *get_title (gchar *file)
{
  gchar **tmp;
  gchar *title = NULL;
  gchar *file2;

  if (gnomad_debug != 0) {
    g_print("get_title()\n");
  }

  file2 = kill_tracknumber(file);
  if (file2 == NULL) {
    return NULL;
  }
  tmp = g_strsplit(file, " - ", 0);
  g_free(file2);

  if (vectorlength(tmp) > 1) {
    gchar **tmp2 = tmp;
    gchar *tmp3;

    while(*tmp2 != NULL) {
      tmp3 = *tmp2;
      tmp2++;
    }
  }
  g_strfreev(tmp);
  return g_strdup(file);

  tmp = g_strsplit(file2, " - ", 0);
  g_free(file2);
  if (vectorlength(tmp) > 1) {
    gchar **tmp2 = tmp;
    gchar *tmp3;

    while(*tmp2 != NULL) {
      tmp3 = *tmp2;
      tmp2++;
    }
    title = g_strdup(tmp3);
  } else {
    /* Otherwise we will use the file name as it is,
     * assuming this is the title is the best we can do... */
    title = g_strdup(file);
  }
  // This is the culprit
  g_strfreev(tmp);
  if (gnomad_debug != 0) {
    if (title != NULL) {
      g_print("Found title \"%s\"...\n", title);
    } else {
      g_print("Didn't find any title.\n");
    }
  }
  return title;
}

static gchar *get_album (gchar *dir)
{
  gchar **tmp;
  gchar *album = NULL;

  if (gnomad_debug != 0) {
    g_print("get_album()\n");
  }
  tmp = g_strsplit(dir, " - ", 0);
  /* If the directory is named "Foo - Bar", we presume
   * Foo is the artist, and Bar is the album title */
  if (vectorlength(tmp) > 1)
    album = g_strdup(*(tmp+1));
  g_strfreev(tmp);
  return album;
}

static guint32 get_trackno(gchar *file)
{
  /* If something beginning with two figures is followed
   * by something that is not a figure, we assume it is
   * a track number. */
  gchar test[3];
  guint i;

  if (gnomad_debug != 0) {
    g_print("get_trackno()\n");
  }

  if (strlen(file) < 3) {
    return 0;
  }
  for (i = 0; i < (strlen(file) - 2); i++) {
    if (g_ascii_isdigit(file[i]) &&
	g_ascii_isdigit(file[i+1]) &&
	!g_ascii_isdigit(file[i+2])) {
      test[0] = file[i];
      test[1] = file[i+1];
      test[2] = '\0';
      if (gnomad_debug != 0) {
	g_print("Found track number: %s\n", test);
      }
      return string_to_guint32(test);
    }
  }
  return 0;
}

void info_from_path(metadata_t *meta, gboolean be_clever)
{
  gchar *path;
  gchar **tmppath;
  gchar *tmpdir;
  gchar *tmpfile;
  guint track = 0;
  int i;

  if (gnomad_debug != 0) {
    g_print("Getting info from path...\n");
  }
  /* If ID3lib has already set seriously acceptable tags,
   * use them and return */
  if (meta->artist != NULL &&
      meta->title != NULL &&
      meta->album != NULL &&
      meta->trackno > 0) {
    if (gnomad_debug != 0) {
      g_print("All metadata already set.\n");
    }
    return;
  }
  /* It's not much use trying to determine something
   * with too little input information.
   */
  if (strlen(meta->path) <= 4) {
    return;
  }
  /* OK now we will use some artificial intelligence to
   * cook up Artist / Title / Album fields from the path.
   */
  path = g_strdup(meta->path);
  if (gnomad_debug != 0) {
    g_print("Processing path: %s\n", path);
  }
  /* Chop off the file extension */
  if (path[strlen(path)-3] == '.')
    path[strlen(path)-3] = '\0';
  else if (path[strlen(path)-4] == '.')
    path[strlen(path)-4] = '\0';
  /* We are interested in the last directory name
   * and the filename from the path */
  tmppath = g_strsplit(path, G_DIR_SEPARATOR_S, 0);
  g_free(path);
  for (i = 0; tmppath[i] != NULL; i++) {};
  if (i >= 2)
    tmpdir = g_strdup(tmppath[i-2]);
  else
    tmpdir = NULL;
  if (i >= 1)
    tmpfile = g_strdup(tmppath[i-1]);
  else
    tmpfile = NULL;
  g_strfreev(tmppath);
  if (be_clever) {
    tmpdir = remove_crap_from_dirname(tmpdir);
    tmpfile = remove_crap_from_filename(tmpfile);
    tmpdir = beautify_string(tmpdir);
    tmpfile = beautify_string(tmpfile);
  }
  if (gnomad_debug != 0) {
    g_print("Dir: %s\n", tmpdir);
    g_print("File: %s\n", tmpfile);
  }
  // If we're not being too clever, do as mediasource do.
  if (!be_clever) {
    if ((meta->title == NULL) && (tmpfile != NULL)) {
      meta->title = tmpfile;
      g_free(tmpdir);
    }
    return;
  }
  if (meta->trackno <=0 && track > 0)
    meta->trackno = track;
  /* Now guess the artist, title and album from
   * looking at the file- and directoryname */
  if (meta->artist == NULL)
    meta->artist = get_artist(tmpdir, tmpfile);
  if (meta->title == NULL)
    meta->title = get_title(tmpfile);
  if (meta->album == NULL)
    meta->album = get_album(tmpdir);
  if (meta->trackno == 0)
    meta->trackno = get_trackno(tmpfile);
  g_free(tmpdir);
  g_free(tmpfile);
  if (gnomad_debug != 0) {
    g_print("Got all info from path...\n");
    dump_metadata_t(meta);
  }
}
