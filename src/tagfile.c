/* tagfile.c
   Interface to the taglib library for Ogg/FLAC file tags.
   Copyright (C) 2007  Peter Randeu

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/
#include "common.h"
#include "metadata.h"
#include "util.h"
#include "filesystem.h"
#include <taglib/tag_c.h>

/*****************************************************************************
 * EXPORTED FUNCTIONS
 *****************************************************************************/

void
remove_tag_from_file(gchar *path)
{
  gchar *tmppath = filename_fromutf8(path);
  TagLib_File *file = taglib_file_new(tmppath);
  // FIXME: taglib doesn't know how to strip all tags. That would be good
  // because on our devices, metadata is sent with PDE/MTP commands, not
  // stored in the tags.

  if(file == NULL) {
    g_printf("could not open file %s", tmppath);
    g_free(tmppath);
    return;      
  }
  g_free(tmppath);
  // I introduced this to taglib by patch. Let's see if they accept it.
  // taglib_mpeg_strip_tag(file);
  taglib_tag_free_strings();
  taglib_file_free(file);
}

void
get_tag_for_file (metadata_t *meta)
{
  gchar *tmppath = filename_fromutf8(meta->path);
  TagLib_Tag *tag;
  const TagLib_AudioProperties *properties;
  TagLib_File *file = taglib_file_new(tmppath);
  
  if(file == NULL) {
    g_printf("could not open file %s", tmppath);
    g_free(tmppath);
    return;      
  }
  g_free(tmppath);
  
  tag = taglib_file_tag(file);
  if (tag == NULL) {
    g_print("tag was NULL in file %s", tmppath);
    return;
  }
  properties = taglib_file_audioproperties(file);
  
  gchar* artist = taglib_tag_artist(tag);
  meta->artist = strlen(artist) ? g_strdup(artist) : NULL;
  
  gchar* title = taglib_tag_title(tag);
  meta->title = strlen(title) ? g_strdup(title) : NULL;
  
  gchar* album = taglib_tag_album(tag);
  meta->album = strlen(album) ? g_strdup(album) : NULL;
  
  gchar* genre = taglib_tag_genre(tag);
  meta->genre = strlen(genre) ? g_strdup(genre) : NULL;

  meta->year = taglib_tag_year(tag);
  meta->length = seconds_to_mmss(taglib_audioproperties_length(properties));
  meta->trackno = taglib_tag_track(tag);
  meta->filename = NULL;
  
  taglib_tag_free_strings();
  taglib_file_free(file);
}



void
set_tag_for_file (metadata_t *meta, gboolean override)
{
  gchar *tmppath = filename_fromutf8(meta->path);
  TagLib_Tag *tag;
  const TagLib_AudioProperties *properties;
  TagLib_File *file = taglib_file_new(tmppath);
  
  if(file == NULL) {
    g_printf("could not open file %s", tmppath);
    g_free(tmppath);
    return;      
  }
  g_free(tmppath);

  // If taglib >= 1.5
  // taglib_id3v2_set_default_text_encoding(TagLib_ID3v2_UTF16);

  tag = taglib_file_tag(file);
  if (tag == NULL) {
    g_print("tag was NULL in file %s", tmppath);
    return;
  }
  
  if (meta->artist != NULL &&
      strcmp(meta->artist, "<Unknown>")) {
      taglib_tag_set_artist(tag, meta->artist);
  }
  if (meta->title != NULL &&
      strcmp(meta->title, "<Unknown>")) {
      taglib_tag_set_title(tag, meta->title);
  }
  if (meta->album != NULL &&
      strcmp(meta->album, "<Unknown>")) {
    taglib_tag_set_album(tag, meta->album);
  }
  if (meta->year != 0) {
    taglib_tag_set_year(tag, meta->year);
  }
  if (meta->genre != NULL &&
      strcmp(meta->genre, "<Unknown>")) {
    taglib_tag_set_genre(tag, meta->genre);
  }
  taglib_tag_set_track(tag, meta->trackno);
  
  taglib_file_save(file);
  taglib_tag_free_strings();
  taglib_file_free(file);
}
