/* prefs.c
   Graphical widgets and callbacks for the preferences frame
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#include "common.h"
#include "prefs.h"
#include "util.h"
#include "filesystem.h"

static GtkWidget *tagoverride = NULL;
static GtkWidget *tagremove = NULL;
static GtkWidget *usetag = NULL;
static GtkWidget *useid3_unicode = NULL;
static GtkWidget *display_dotdirs = NULL;
#ifndef G_OS_WIN32
static GtkWidget *force_8859 = NULL;
#endif
static GtkWidget *filenameformat = NULL;
static GtkWidget *use_origname = NULL;
static GtkWidget *ask_playlist = NULL;
static GtkWidget *recurse_dir = NULL;
static GtkWidget *detect_metadata = NULL;
static GtkWidget *extended_metadata = NULL;
static GtkWidget *autoscan = NULL;
static GtkWidget *set_time = NULL;
static GtkWidget *remember_dir = NULL;
static GtkWidget *turbo_mode = NULL;

static gboolean get_prefs_remember_dir(void);


static void read_prefs_file(void)
{
  const gchar scannername[] = PACKAGE " lexical scanner";
  GScanner *scanner;
  GTokenType token = G_TOKEN_NONE;
  gint preffd;
  /* This indicates that said option _must_ be selected */
  gboolean must_read_extended_metadata = FALSE;
  gchar *name;

  /* These shall be TRUE by default */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tagremove), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(usetag), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useid3_unicode), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ask_playlist), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(detect_metadata), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(autoscan), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(set_time), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(remember_dir), TRUE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(turbo_mode), TRUE);

  /* g_print("Called read prefs\n"); */
  scanner = g_scanner_new(NULL);
  scanner->input_name = scannername;
  preffd = get_prefsfile_fd(FALSE);
  if (preffd <= 0)
    return;
  g_scanner_input_file(scanner, preffd);
  while (!g_scanner_eof(scanner) &&
	 token != G_TOKEN_LAST) {
    token = g_scanner_get_next_token(scanner);
    if (token == G_TOKEN_IDENTIFIER) {
      name = g_strdup(g_scanner_cur_value(scanner).v_identifier);
      /* g_print("Found identifier %s\n", name); */
      /* We found an identifier! Get the value! 
       * only accept strings and bits as of now */
      while (token != G_TOKEN_STRING &&
	     token != G_TOKEN_INT &&
	     !g_scanner_eof(scanner))
	token = g_scanner_get_next_token(scanner);
      if (!strcmp(name, "FILENAME_FORMAT_STRING")) {
	gtk_entry_set_text(GTK_ENTRY(filenameformat),
			   g_scanner_cur_value(scanner).v_string);
      } else if (!strcmp(name, "USE_ORIGNAME")) {
	if (g_scanner_cur_value(scanner).v_int) {
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_origname), TRUE);
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(extended_metadata), TRUE);
	  must_read_extended_metadata = TRUE;
	} else {
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_origname), FALSE);
	}
      } else if (!strcmp(name, "DISPLAY_DOTDIRS")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_dotdirs), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_dotdirs), FALSE);
      } else if (!strcmp(name, "ID3_REMOVE") || !strcmp(name, "TAG_REMOVE")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tagremove), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tagremove), FALSE);
      } else if (!strcmp(name, "ID3_OVERRIDE") || !strcmp(name, "TAG_OVERRIDE")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tagoverride), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tagoverride), FALSE);
      } else if (!strcmp(name, "USE_ID3V1") || !strcmp(name, "USE_ID3V2") || 
		 !strcmp(name, "USE_ID3") || !strcmp(name, "USE_TAG")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(usetag), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(usetag), FALSE);
      } else if (!strcmp(name, "USE_ID3_UNICODE")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useid3_unicode), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useid3_unicode), FALSE);
      } else if (!strcmp(name, "ASK_PLAYLIST")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ask_playlist), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ask_playlist), FALSE);
      } else if (!strcmp(name, "RECURSE_DIR")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(recurse_dir), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(recurse_dir), FALSE);
#ifndef G_OS_WIN32
      } else if (!strcmp(name, "FORCE_8859")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(force_8859), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(force_8859), FALSE);
#endif
      } else if (!strcmp(name, "DETECT_METADATA")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(detect_metadata), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(detect_metadata), FALSE);
      } else if (!strcmp(name, "EXTENDED_METADATA")) {
	if (g_scanner_cur_value(scanner).v_int || must_read_extended_metadata)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(extended_metadata), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(extended_metadata), FALSE);
      } else if (!strcmp(name, "AUTOSCAN")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(autoscan), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(autoscan), FALSE);
      } else if (!strcmp(name, "SET_TIME")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(set_time), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(set_time), FALSE);
      } else if (!strcmp(name, "REMEMBER_DIR")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(remember_dir), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(remember_dir), FALSE);
      } else if (!strcmp(name, "TURBO_MODE")) {
	if (g_scanner_cur_value(scanner).v_int)
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(turbo_mode), TRUE);
	else
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(turbo_mode), FALSE);
      } else if (!strcmp(name, "CURRENT_DIRECTORY")) {
	if (get_prefs_remember_dir() && is_directory(g_scanner_cur_value(scanner).v_string)) {
	  change_directory(g_scanner_cur_value(scanner).v_string);
	}
      }

      // Then also change dir
      g_free(name);
    }
  }
  g_scanner_destroy(scanner);
  close_prefsfile();
}

void write_prefs_file(void)
{
  gchar *prefs = NULL;
  gchar *fnameformat = NULL;
  gchar *tmp;

  /* g_print("Called write prefs\n"); */
  prefs = stringcat(prefs, "# " PACKAGE " PREFERENCES\n\n");
  prefs = stringcat(prefs, "FILENAME_FORMAT_STRING='");
  fnameformat = get_prefs_filenameformat();
  prefs = stringcat(prefs, fnameformat);
  g_free(fnameformat);
  prefs = stringcat(prefs, "'\n");
  prefs = stringcat(prefs, "USE_ORIGNAME=");
  if (get_prefs_use_origname())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "DISPLAY_DOTDIRS=");
  if (get_prefs_display_dotdirs())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "TAG_REMOVE=");
  if (get_prefs_tagremove())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "TAG_OVERRIDE=");
  if (get_prefs_tagoverride())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "USE_TAG=");
  if (get_prefs_usetag())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "USE_ID3_UNICODE=");
  if (get_prefs_useid3_unicode())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "ASK_PLAYLIST=");
  if (get_prefs_ask_playlist())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "RECURSE_DIR=");
  if (get_prefs_recurse_dir())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
#ifndef G_OS_WIN32
  prefs = stringcat(prefs, "FORCE_8859=");
  if (get_prefs_force_8859())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
#endif
  prefs = stringcat(prefs, "DETECT_METADATA=");
  if (get_prefs_detect_metadata())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "EXTENDED_METADATA=");
  if (get_prefs_extended_metadata() || get_prefs_use_origname())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "AUTOSCAN=");
  if (get_prefs_autoscan())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "SET_TIME=");
  if (get_prefs_set_time())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "REMEMBER_DIR=");
  if (get_prefs_remember_dir())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");
  prefs = stringcat(prefs, "TURBO_MODE=");
  if (get_prefs_turbo_mode())
    prefs = stringcat(prefs, "1");
  else
    prefs = stringcat(prefs, "0");
  prefs = stringcat(prefs, "\n");

  prefs = stringcat(prefs, "CURRENT_DIRECTORY='");
  tmp = get_current_dir();
  prefs = stringcat(prefs, tmp);
  g_free(tmp);
  prefs = stringcat(prefs, "'\n");

  rewrite_prefsfile(prefs);
}

gboolean get_prefs_use_origname(void)
{
  if (use_origname == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_origname));
}

gboolean get_prefs_tagoverride(void)
{
  if (tagoverride == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tagoverride));
}

gboolean get_prefs_tagremove(void)
{
  if (tagremove == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tagremove));
}

gboolean get_prefs_usetag(void)
{
  if (usetag == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(usetag));
}

gboolean get_prefs_useid3_unicode(void)
{
  if (useid3_unicode == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(useid3_unicode));
}

gboolean get_prefs_display_dotdirs(void)
{
  if (display_dotdirs == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(display_dotdirs));
}

gboolean get_prefs_ask_playlist(void)
{
  if (ask_playlist == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ask_playlist));
}

gboolean get_prefs_recurse_dir(void)
{
  if (recurse_dir == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(recurse_dir));
}

#ifndef G_OS_WIN32
gboolean get_prefs_force_8859(void)
{
  if (force_8859 == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(force_8859));
}
#endif

gboolean get_prefs_detect_metadata(void)
{
  if (detect_metadata == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(detect_metadata));
}

gboolean get_prefs_extended_metadata(void)
{
  if (extended_metadata == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(extended_metadata));
}

gboolean get_prefs_autoscan(void)
{
  if (autoscan == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(autoscan));
}

gboolean get_prefs_set_time(void)
{
  if (set_time == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(set_time));
}

static gboolean get_prefs_remember_dir(void)
{
  if (remember_dir == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(remember_dir));
}

gboolean get_prefs_turbo_mode(void)
{
  if (turbo_mode == NULL)
    return FALSE;
  else
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(turbo_mode));
}


gchar *get_prefs_filenameformat(void)
{
  static gchar defaultformat[] = "%a - %t";
  G_CONST_RETURN gchar *returnformat;
  
  if (tagremove == NULL)
    returnformat = defaultformat;
  returnformat = gtk_entry_get_text(GTK_ENTRY(filenameformat));
  if (!strlen(returnformat))
    returnformat = defaultformat;
  return g_strdup(returnformat);
}

static void toggle_origname (GtkCheckButton *toggle,
		       gpointer data)
{
  if (get_prefs_use_origname()) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(extended_metadata), TRUE);
  }
}

static void toggle_extended_metadata (GtkCheckButton *toggle,
				      gpointer data)
{
  if (!get_prefs_extended_metadata()) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_origname), FALSE);
  }
}

void create_prefs_widgets(GtkWidget *box)
{
  GtkWidget *hbox, *vbox, *vbox_naming,
    *frame, *label;

  /* Pack all prefs into a vbox */
  vbox = gtk_vbox_new(FALSE, 0);

  /* Preference for filenaming */
  frame = gtk_frame_new(_("Filenaming"));
  gtk_widget_show (frame);
  vbox_naming = gtk_vbox_new(FALSE, 0);
  use_origname = 
    gtk_check_button_new_with_label(_("Use original filename if present in metadata for the track"));
  g_signal_connect_object(GTK_OBJECT(use_origname),
			  "toggled",
			  G_CALLBACK(toggle_origname),
			  NULL,
			  0);
  gtk_box_pack_start(GTK_BOX(vbox_naming), use_origname, FALSE, FALSE, 0);
  gtk_widget_show(use_origname);
  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new(_("This string is used when naming files copied " \
			"from the jukebox library to the filesystem.") );
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox_naming), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);
  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new(_("Formatting string:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  /* Add text entry */
  filenameformat = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(filenameformat), "%a - %t");
  gtk_box_pack_start(GTK_BOX(hbox), filenameformat, TRUE, TRUE, 0);
  gtk_widget_show(filenameformat);
  gtk_box_pack_start(GTK_BOX(vbox_naming), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);
  /* Don't translate the magic %-things! */
  label = gtk_label_new(_("%a = Artist, %t = Title, %b = Album, %g = Genre, %n = Track number, %y = Year"));
  gtk_box_pack_start(GTK_BOX(vbox_naming), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  gtk_widget_show(vbox_naming);
  gtk_container_add(GTK_CONTAINER (frame), vbox_naming);
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

  /* Checkbutton preferences */
  tagoverride = 
    gtk_check_button_new_with_label(_("Metadata from the jukebox overrides " \
				      "present tags on transfered files"));
  tagremove = 
    gtk_check_button_new_with_label(_("Remove all metadata tags from files "	\
				      "transferred to jukebox"));
  usetag =
    gtk_check_button_new_with_label(_("Write tags to files transferred to " \
				      "harddisk"));
  useid3_unicode =
    gtk_check_button_new_with_label(_("Force use of Unicode charset for ID3 tags"));
  display_dotdirs = 
    gtk_check_button_new_with_label(_("Display hidden (.foo) directories " \
				      "in filesystem"));
  ask_playlist =
    gtk_check_button_new_with_label(_("Ask to add files to a playlist"));
  recurse_dir =
    gtk_check_button_new_with_label(_("Recurse into any selected directories when transferring"));
#ifndef G_OS_WIN32
  force_8859 =
    gtk_check_button_new_with_label(_("Force local filesystem to ISO-8859-1 charset"));
#endif
  detect_metadata =
    gtk_check_button_new_with_label(_("Try to extract sound file metadata from path and filename"));
  extended_metadata =
    gtk_check_button_new_with_label(_("Read extended metadata from jukebox (e.g. original filename - slow on newer devices)"));
  g_signal_connect_object(GTK_OBJECT(extended_metadata),
			  "toggled",
			  G_CALLBACK(toggle_extended_metadata),
			  NULL,
			  0);
  autoscan =
    gtk_check_button_new_with_label(_("Automatically read in jukebox contents on startup"));
  set_time =
    gtk_check_button_new_with_label(_("Automatically set the time on the Jukebox to that of your computer on startup"));
  remember_dir =
    gtk_check_button_new_with_label(_("On startup, return to the place in the filesystem where you were last time"));
  turbo_mode =
    gtk_check_button_new_with_label(_("Enable turbo mode - uncheck this if you have problems"));
  gtk_box_pack_start(GTK_BOX(vbox), tagoverride, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), tagremove, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), usetag, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), useid3_unicode, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), display_dotdirs, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), ask_playlist, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), recurse_dir, FALSE, FALSE, 0);
#ifndef G_OS_WIN32
  gtk_box_pack_start(GTK_BOX(vbox), force_8859, FALSE, FALSE, 0);
#endif
  gtk_box_pack_start(GTK_BOX(vbox), detect_metadata, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), extended_metadata, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), autoscan, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), set_time, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), remember_dir, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), turbo_mode, FALSE, FALSE, 0);
  gtk_widget_show(tagoverride);
  gtk_widget_show(tagremove);
  gtk_widget_show(usetag);
  gtk_widget_show(useid3_unicode);
  gtk_widget_show(display_dotdirs);
  gtk_widget_show(ask_playlist);
  gtk_widget_show(recurse_dir);
#ifndef G_OS_WIN32
  gtk_widget_show(force_8859);
#endif
  gtk_widget_show(detect_metadata);
  gtk_widget_show(extended_metadata);
  gtk_widget_show(autoscan);
  gtk_widget_show(set_time);
  gtk_widget_show(remember_dir);
  gtk_widget_show(turbo_mode);

  gtk_widget_show(vbox);
  gtk_box_pack_start(GTK_BOX(box), vbox, TRUE, TRUE, 0);
  /* Read in preferences */
  read_prefs_file();
}
