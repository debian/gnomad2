/* editmeta.c
   Edit metadata for an mp3- or similar file
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "filesystem.h"
#include "jukebox.h"
#include "xfer.h"
#include "util.h"
#include "id3read.h"
#include "prefs.h"
#ifdef HAVE_TAGLIB
#include "tagfile.h"
#endif

static listtype_t target;
static GtkWidget *edit_dialog;
static GtkWidget *edit_dialog_msglabel;
static GtkTreeSelection *selection;
static GList *metalist = NULL;
static gchar *artist;
static gchar *title;
static gchar *album;
static gchar *year;
static gchar *genre;
static gchar *length;
static gchar *trackno;
static gchar *origfname;
static gchar *folder;
static gchar *protect;

static gchar *ret_nonull(gchar *retval)
{
  if (retval == NULL) {
    return g_strdup("");
  } else {
    return g_strdup(retval);
  }
}

static gchar *metalist_get_column(GList *metalist, gint column)
{
  if (g_list_length(metalist) == 0)
    return NULL;
  else {
    /* We all assume file data */
    metadata_t *meta = (metadata_t *) metalist->data;
    switch (column) {
    case TRACKLIST_ARTIST_COLUMN:
      return ret_nonull(meta->artist);
    case TRACKLIST_TITLE_COLUMN:
      return ret_nonull(meta->title);
    case TRACKLIST_ALBUM_COLUMN:
      return ret_nonull(meta->album);
    case TRACKLIST_YEAR_COLUMN:
      /* Convert to string and return */
      return g_strdup_printf("%" PRIguint32,meta->year);
    case TRACKLIST_GENRE_COLUMN:
      return ret_nonull(meta->genre);
    case TRACKLIST_LENGTH_COLUMN:
      return g_strdup(meta->length);
    case TRACKLIST_SIZE_COLUMN:
      /* Convert to string and return */
      return g_strdup_printf("%lu", meta->size);
    case TRACKLIST_CODEC_COLUMN:
      return ret_nonull(meta->codec);
    case TRACKLIST_TRACKNO_COLUMN:
      /* Convert to string and return */
      return g_strdup_printf("%" PRIguint32,meta->trackno);
    case TRACKLIST_PROTECTED_COLUMN:
      return g_strdup_printf("%s", meta->protect ? _("YES") : _("NO"));
    case TRACKLIST_FILENAME_COLUMN:
      return ret_nonull(meta->filename);
    case TRACKLIST_ID_COLUMN:
      /* Only for TRACKLIST */
      return ret_nonull(meta->path);
    case TRACKLIST_FOLDER_COLUMN:
      return ret_nonull(meta->folder);
    default:
      return NULL;
    }
  }
}

static int nullcmp(gchar *one, gchar *two)
{
  if (one == NULL || two == NULL)
    return 1;
  else
    return strcmp(one, two);
}

static void edit_ok_click ( GtkButton *button,
			    gpointer data )
{
  if (target == JB_LIST) {
    if (jukebox_locked) {
      create_error_dialog(_("Jukebox is busy.\nCannot edit metadata now."));
      gtk_grab_remove(edit_dialog);
      gtk_widget_destroy(edit_dialog);
      if (metalist != NULL) {
	destroy_metalist(metalist);
      }
      metalist = NULL;
      return;
    }
    if (!jukebox_begin_metadata_set()) {
      gtk_grab_remove(edit_dialog);
      gtk_widget_destroy(edit_dialog);
      if (metalist != NULL) {
	destroy_metalist(metalist);
      }
      metalist = NULL;
      return;
    }
  }
  /* Remove all selected entries from the list store */
  remove_selected(target);

  while (metalist) {
    metadata_t *meta;
    metadata_t *newmeta;
    gboolean changed;

    meta = (metadata_t *) metalist->data;
    newmeta = clone_metadata_t(meta);
    changed = FALSE;
    if (artist != NULL &&
	strlen(artist) &&
	nullcmp(artist, meta->artist)) {
      changed = TRUE;
      g_free(newmeta->artist);
      newmeta->artist = g_strdup(artist);
    }
    if (title != NULL &&
	strlen(title) &&
	nullcmp(title, meta->title)) {
      changed = TRUE;
      g_free(newmeta->title);
      newmeta->title = g_strdup(title);
    }
    if (album != NULL &&
	strlen(album) &&
	nullcmp(album, meta->album)) {
      changed = TRUE;
      g_free(newmeta->album);
      newmeta->album = g_strdup(album);
    }
    if (genre != NULL &&
	strlen(genre) &&
	nullcmp(genre, meta->genre)) {
      changed = TRUE;
      g_free(newmeta->genre);
      newmeta->genre = g_strdup(genre);
    }
    if (length != NULL &&
	strlen(length) &&
	nullcmp(length, meta->length)) {
      changed = TRUE;
      g_free(newmeta->length);
      newmeta->length = g_strdup(length);
    }
    if (trackno != NULL &&
	strlen(trackno) &&
	string_to_guint32(trackno) != meta->trackno) {
      changed = TRUE;
      newmeta->trackno = string_to_guint32(trackno);
    }
    if (origfname != NULL &&
	strlen(origfname) &&
	nullcmp(origfname, meta->filename)) {
      changed = TRUE;
      g_free(newmeta->filename);
      newmeta->filename = g_strdup(origfname);
    }
    if (folder != NULL &&
	strlen(folder) &&
	nullcmp(folder, meta->folder)) {
      changed = TRUE;
      g_free(newmeta->folder);
      newmeta->folder = g_strdup(folder);
    }
    if (year != NULL &&
	strlen(year) &&
	string_to_guint32(year) != meta->year) {
      changed = TRUE;
      newmeta->year = string_to_guint32(year);
    }
    if (changed) {
      /* Write the new tag */
      gchar tmp[512];

      /* First is artist, second is title */
      sprintf(tmp, _("Updating metadata on %s - %s"), newmeta->artist, newmeta->title);
      gtk_label_set_text(GTK_LABEL(edit_dialog_msglabel), tmp);
      /* Make sure the dialog is updated */
      while (gtk_events_pending())
	gtk_main_iteration();
      if (target == HD_LIST) {
         /*
          * g_print("Updating: %s\n", newmeta->path);
          * dump_metadata_t(newmeta);
          */
         if(!strcmp(newmeta->codec, "MP3"))
#if defined(HAVE_TAGLIB) && defined(TAGLIB_ONLY)
	   set_tag_for_file(newmeta, TRUE);
#else
           set_tag_for_mp3file(newmeta, TRUE);
#endif

#ifdef HAVE_TAGLIB
         else if(!strcmp(newmeta->codec, "OGG") || !strcmp(newmeta->codec, "FLAC"))
            set_tag_for_file(newmeta, TRUE);
#endif
      } else {
	/*
	 * g_print("Updating: %s\n", newmeta->path);
	 * dump_metadata_t(newmeta);
	 */
	jukebox_set_metadata(newmeta);
      }
    }
    add_metadata_to_model(newmeta, target);
    destroy_metadata_t(newmeta);
    metalist = metalist->next;
  }

  gtk_label_set_text(GTK_LABEL(edit_dialog_msglabel), "");
  if (target == JB_LIST) {
    jukebox_end_metadata_set();
  }
  /*
    There is strictly no reason ro re-read the directory, so why
    are we doing it? In the past because we could not trust
    id3lib, so this is to be certain the correct metadata is
    displayed.

    if (target == HD_LIST) {
       fill_in_dir(HD_LIST, get_current_dir());
    }
  */
  gtk_grab_remove(edit_dialog);
  gtk_widget_destroy(edit_dialog);
  if (metalist != NULL) {
    destroy_metalist(metalist);
  }
  metalist = NULL;
}

/* This is needed to free up some memory used by the edit dialog */
static void edit_cancel_click ( GtkButton *button,
				gpointer data )
{
  gtk_grab_remove(edit_dialog);
  gtk_widget_destroy(edit_dialog);
  if (metalist != NULL) {
    destroy_metalist(metalist);
  }
  metalist = NULL;
}

static void entry_edit_callback(GtkWidget *entry,
				gpointer data)
{
  gchar **string;

  string = (gchar **) data;
  *string = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
  /* printf("Entry changed to: %s\n", *string); */
}

static gboolean all_strings_same(GList *metalist, gint column)
{
  GList *tmplist = metalist;
  gchar *firstval;

  if (g_list_length(metalist) < 2)
    return TRUE;
  firstval = metalist_get_column(metalist, column);
  tmplist = tmplist->next;
  while (tmplist != NULL) {
    gchar *secondval = metalist_get_column(tmplist, column);
    if (nullcmp(firstval, secondval)) {
      g_free(firstval);
      g_free(secondval);
      return FALSE;
    }
    g_free(secondval);
    tmplist = tmplist->next;
  }
  g_free(firstval);
  return TRUE;
}

static void add_to_dialog(GtkWidget *dialog, GtkWidget *thing)
{
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), thing, TRUE,
		      TRUE, 0);
}
/* Editing the metadata... */
static void edit_dialog_create(void)
{
  GtkWidget *hbox, *label, *dialog, *tempwid, *entry;

  if (target == JB_LIST) {
    dialog = gtk_dialog_new_with_buttons(_("Edit jukebox file metadata"),
					 NULL, 0, NULL);
  } else {
    dialog = gtk_dialog_new_with_buttons(_("Edit track metadata"),
					 NULL, 0, NULL);
  }

  tempwid = gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_CANCEL, 0);
  g_signal_connect (tempwid, "clicked",
		    G_CALLBACK (edit_cancel_click), NULL);

  tempwid = gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_OK, 1);
  g_signal_connect (tempwid, "clicked",
		    G_CALLBACK (edit_ok_click), NULL);
  edit_dialog = dialog;

  /* Add the metadata edit field for the dialog box */
  hbox = gtk_hbox_new (FALSE, 0);
  label = gtk_label_new(_("Artist:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox(hbox);
  entry = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(entry), 45);
  if (all_strings_same(metalist, TRACKLIST_ARTIST_COLUMN)) {
    artist = metalist_get_column(metalist, TRACKLIST_ARTIST_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), artist);
    g_free(artist);
  }
  artist = (gchar*) gtk_entry_get_text(GTK_ENTRY(entry));
  g_signal_connect(GTK_OBJECT(entry),
		   "changed",
		   G_CALLBACK(entry_edit_callback),
		   (gpointer) &artist);
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
  gtk_widget_show(entry);
  add_to_dialog(dialog , hbox);
  gtk_widget_show (hbox);

  /* We may want to edit the title only if one file is selected... */
  if (g_list_length(metalist) == 1) {
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Title:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox(hbox);
    entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 45);
    title = metalist_get_column(metalist, TRACKLIST_TITLE_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), title);
    g_free(title);
    title = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
    g_signal_connect(GTK_OBJECT(entry),
		     "changed",
		     G_CALLBACK(entry_edit_callback),
		     (gpointer) &title);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
    gtk_widget_show(entry);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }

  hbox = gtk_hbox_new (FALSE, 0);
  label = gtk_label_new(_("Album:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox(hbox);
  entry = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(entry), 45);
  if (all_strings_same(metalist, TRACKLIST_ALBUM_COLUMN)) {
    album = metalist_get_column(metalist, TRACKLIST_ALBUM_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), album);
    g_free(album);
  }
  album = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
  g_signal_connect(GTK_OBJECT(entry),
		   "changed",
		   G_CALLBACK(entry_edit_callback),
		   (gpointer) &album);
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
  gtk_widget_show(entry);
  add_to_dialog(dialog , hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new (FALSE, 0);
  label = gtk_label_new(_("Year:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox(hbox);
  entry = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(entry), 45); /* 5 but does not work... */
  if (all_strings_same(metalist, TRACKLIST_YEAR_COLUMN)) {
    year = metalist_get_column(metalist, TRACKLIST_YEAR_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), year);
    g_free(year);
  }
  year = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
  g_signal_connect(GTK_OBJECT(entry),
		   "changed",
		   G_CALLBACK(entry_edit_callback),
		   (gpointer) &year);
  /* Alignment widget for left alignments */
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
  add_to_dialog(dialog , hbox);
  gtk_widget_show (hbox);

  hbox = gtk_hbox_new (FALSE, 0);
  label = gtk_label_new(_("Genre:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);
  add_empty_hbox(hbox);
  entry = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(entry), 45); /* 30 but does not work... */
  if (all_strings_same(metalist, TRACKLIST_GENRE_COLUMN)) {
    genre = metalist_get_column(metalist, TRACKLIST_GENRE_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), genre);
    g_free(genre);
  }
  genre = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
  g_signal_connect(GTK_OBJECT(entry), 
		   "changed",
		   G_CALLBACK(entry_edit_callback),
		   (gpointer) &genre);
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
  gtk_widget_show(entry);
  add_to_dialog(dialog , hbox);
  gtk_widget_show (hbox);

  /* If there was only one file selected we may edit the play length */
  if (g_list_length(metalist) == 1) {
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Length:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox(hbox);
    entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 45); /* 6 but does not work... */
    if (all_strings_same(metalist, TRACKLIST_LENGTH_COLUMN)) {
      length = metalist_get_column(metalist, TRACKLIST_LENGTH_COLUMN);
      gtk_entry_set_text(GTK_ENTRY(entry), length);
      g_free(length);
    }
    length = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
    g_signal_connect(GTK_OBJECT(entry), 
		     "changed",
		     G_CALLBACK(entry_edit_callback),
		     (gpointer) &length);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
    gtk_widget_show(entry);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }

  /* If there was only one file selected we may show the track number */
  if (g_list_length(metalist) == 1) {
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Track number:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox(hbox);
    entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 45); /* 3 but does not work... */
    trackno = metalist_get_column(metalist, TRACKLIST_TRACKNO_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), trackno);
    g_free(trackno);
    trackno = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
    g_signal_connect(GTK_OBJECT(entry), 
		     "changed",
		     G_CALLBACK(entry_edit_callback),
		     (gpointer) &trackno);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
    gtk_widget_show(entry);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }

  /* If there was only one file selected we may show the original filename */
  if (g_list_length(metalist) == 1 && get_prefs_extended_metadata()) {
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Original filename:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox(hbox);
    entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 45);
    origfname = metalist_get_column(metalist, TRACKLIST_FILENAME_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), origfname);
    g_free(origfname);
    origfname = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
    g_signal_connect(GTK_OBJECT(entry), 
		     "changed",
		     G_CALLBACK(entry_edit_callback),
		     (gpointer) &origfname);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
    gtk_widget_show(entry);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }

  /* If there was only one file selected we may show the folder name */
  if (g_list_length(metalist) == 1 && get_prefs_extended_metadata()) {
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Folder:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_empty_hbox(hbox);
    entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry), 45);
    folder = metalist_get_column(metalist, TRACKLIST_FOLDER_COLUMN);
    gtk_entry_set_text(GTK_ENTRY(entry), folder);
    g_free(folder);
    folder = (gchar *) gtk_entry_get_text(GTK_ENTRY(entry));
    g_signal_connect(GTK_OBJECT(entry), 
		     "changed",
		     G_CALLBACK(entry_edit_callback),
		     (gpointer) &folder);
    gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);
    gtk_widget_show(entry);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }

  /* Protection */
  if (g_list_length(metalist) == 1) {
    GtkWidget *checkbox = gtk_check_button_new_with_label(_("Protected"));
    gtk_widget_set_sensitive(GTK_WIDGET(checkbox), FALSE);
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), checkbox, FALSE, FALSE, 0);
    protect = metalist_get_column(metalist, TRACKLIST_PROTECTED_COLUMN);
    if (!strcmp(protect,_("YES"))) {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), TRUE);
    }
    g_free(protect);
    gtk_widget_show(checkbox);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
  }


  /* Show codec type if it is the same for all files */
  if (all_strings_same(metalist, TRACKLIST_CODEC_COLUMN)) {
    gchar *tmpcodec = metalist_get_column(metalist, TRACKLIST_CODEC_COLUMN);
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("Codec:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    label = gtk_label_new(tmpcodec);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
    g_free(tmpcodec);
  }

  /* Show size if only one file is selected */
  if (g_list_length(metalist) == 1) {
    gchar *tmpfilesize = metalist_get_column(metalist, TRACKLIST_SIZE_COLUMN);
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new(_("File size:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);    
    label = gtk_label_new(tmpfilesize);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
    g_free(tmpfilesize);
  }

  /* If there was only one file selected we may show the path/trackid */
  if (g_list_length(metalist) == 1) {
    gchar *tmpid = metalist_get_column(metalist, TRACKLIST_ID_COLUMN);
    hbox = gtk_hbox_new (FALSE, 0);
    if (target == JB_LIST)
      label = gtk_label_new(_("File ID:"));
    else
      label = gtk_label_new(_("Path:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    label = gtk_label_new(tmpid);
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);
    add_to_dialog(dialog , hbox);
    gtk_widget_show (hbox);
    g_free(tmpid);
  }

  /* Label for displaying tagging messages */
  label = gtk_label_new("");
  gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
  edit_dialog_msglabel = label;
  add_to_dialog(dialog, label);
  gtk_widget_show (label);
  
  gtk_widget_show (dialog);
  /* grab focus */
  gtk_grab_add (dialog);
}

/* Removes any directories from a selection */
static GList *filter_for_editing(GList *metadatas, guint listtype)
{
  GList *tmp = metadatas;
  GList *newmetas = NULL;

  if (metadatas == NULL) {
    return NULL;
  }

  while (tmp) {
    metadata_t *meta = (metadata_t  *) tmp->data;
    if (meta->size == 0) {
      /* Throw this away */
       destroy_metadata_t(meta);
    } else if ( (listtype == HD_LIST)
		&& strcmp(meta->codec, "MP3")
#ifdef HAVE_TAGLIB
                && strcmp(meta->codec, "OGG")
		&& strcmp(meta->codec, "FLAC")
#endif
		) {
      // If on the host side, we can only edit MP3 ID3v1/v2 plug OGG/FLAC
      // metadata, so filter out anything else.
      destroy_metadata_t(meta);
    } else {
      /* Keep this */
      newmetas = g_list_append (newmetas, meta);
    }
    tmp = g_list_next(tmp);
  }
  g_list_free(metadatas);

  return newmetas;
}

static void prepare_dialog(guint listtype)
{
  /* This is for the harddisk list only */
  GList *metadatas = get_all_metadata_from_selection(listtype);

  metadatas = filter_for_editing(metadatas, listtype);
  selection = get_metadata_selection(listtype);
  artist = NULL;
  title = NULL;
  album = NULL;
  year = NULL;
  genre = NULL;
  length = NULL;
  trackno = NULL;
  origfname = NULL;
  folder = NULL;
  protect = NULL;
  if (metadatas == NULL)
    return;
  metalist = metadatas;
  // Create the editing dialog
  if (g_list_length(metalist) > 0)
    edit_dialog_create();
}

/* Response to the "edit" call from the harddisk popup menu */
void hdmenu_edit_response(gpointer data)
{
  target = HD_LIST;
  prepare_dialog(target);
}

/* Response to the "edit" call from the jukebox popup menu */
void jbmenu_edit_response(gpointer data)
{
  target = JB_LIST;
  prepare_dialog(target);
}
