/* util.c
   General utility functions, eg for string formatting
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"

extern GtkWidget *main_window;

/* Find the length of a string vector */
gint vectorlength(gchar **vector)
{
  gchar **tmp = vector;
  gint size = 0;

  if (!tmp)
    return 0;

  while (*tmp)
    {
      size ++;
      tmp ++;
    }
  return size;
}

/* Concatenate two dynamic strings */
gchar *stringcat(gchar *org, gchar *add)
{
  /* This can be improved under GTK+ 2.0
   * new GLib functions -> 2.0 */
  gchar *tmpstring;

  if (add == NULL)
    return org;
  if (org == NULL)
    return g_strdup(add);

  tmpstring = (gchar *) g_malloc(strlen(org) + strlen(add) + 1);
  tmpstring = strcpy(tmpstring, org);
  tmpstring = strcat(tmpstring, add);
  g_free(org);
  return tmpstring;
}

void replacechar(gchar *string, gchar find, gchar replace)
{
  /* Replaces character find with character replace
   * in string */
  gchar *tmp = string;

  if (!string || !find || !replace)
    return;

  while (*tmp)
    {
      if (*tmp == find)
	*tmp = replace;
      tmp ++;
    }
}

gchar *replacestring(gchar *string, gchar *find, gchar *replace)
{
  /* Replaces the substring find with substring replace
   * in string */
  static gchar stringbuffer[512];
  gchar *returnstring;
  gchar **tmp2;
  gchar **tmp3;

  if (!string || !find || !replace)
    return string;

  tmp2 = g_strsplit(string, find, 0);

  /* This is the case when the replacement is in the
   * last characters of the string */
  if (vectorlength(tmp2) == 1 &&
      strlen(string) > strlen(*tmp2)) {
    stringbuffer[0] = '\0';
    strcat(stringbuffer, *tmp2);
    strcat(stringbuffer, replace);
    g_free(string);
    returnstring = g_strdup(stringbuffer);
  }
  /* If there were no matches, return original string */
  else if (vectorlength(tmp2) > 1) {
    stringbuffer[0] = '\0';
    tmp3 = tmp2;
    while (*tmp3)
      {
	strcat(stringbuffer, *tmp3);
	/* If there are more strings in the list, insert the replacement */
	if (*(tmp3+1))
	  strcat(stringbuffer, replace);
	tmp3 ++;
      }
    /* Free old string */
    g_free(string);
    returnstring = g_strdup(stringbuffer);
  } else {
    returnstring = string;
  }
  g_strfreev(tmp2);
  return returnstring;
}

/* Converts a string into guint representation (if possible) */
guint32 string_to_guint32(gchar *string)
{
  gchar *dummy;

  if (!string)
    return 0;

  return (guint32) strtoul(string, &dummy, 10);
}

/* Converts a figure representing a number of seconds to
 * a string in mm:ss notation */
gchar *seconds_to_mmss(guint32 seconds)
{
  gchar tmp2[10];
  gchar tmp[10];
  guint32 secfrac = seconds % 60;
  guint32 minfrac = seconds / 60;

  if (seconds == 0)
    return g_strdup("0:00");

  snprintf(tmp2, 10, "0%" PRIguint32, secfrac);
  while (strlen(tmp2)>2) {
    tmp2[0]=tmp2[1];
    tmp2[1]=tmp2[2];
    tmp2[2]='\0';
  }
  snprintf(tmp, 10, "%" PRIguint32 ":%s", minfrac, tmp2);
  return g_strdup(tmp);
}

/* Converts a string in mm:ss notation to a figure
 * representing seconds */
guint32 mmss_to_seconds(gchar *mmss)
{
  gchar **tmp;
  guint32 seconds = 0;

  if (!mmss)
    return seconds;

  tmp = g_strsplit(mmss, ":", 0);
  if (vectorlength(tmp) == 2) {
    seconds = 60 * string_to_guint32(tmp[0]);
    seconds += string_to_guint32(tmp[1]);
  }
  if (tmp != NULL)
    g_strfreev(tmp);
  return seconds;
}

/* are there only numbers in this string? */
gboolean is_a_number(gchar *string)
{
  gchar *tmp;

  if (string == NULL)
    return FALSE;
  tmp = string;
  while (*tmp)
    {
      /* See if it is not a number, skip spaces */
      if ((*tmp < '0' ||
	   *tmp > '9') &&
	  *tmp != ' ' &&
	  *tmp != '.')
	return FALSE;
      tmp ++;
    }
  return TRUE;
}

/* Good for mocking up correctly adjusted dialogs */
void add_empty_hbox (GtkWidget *tobox)
{
  GtkWidget *thing = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (thing);
  gtk_box_pack_start (GTK_BOX (tobox), thing, TRUE, FALSE, 0);
}

/* Create an error dialog */
void create_error_dialog(gchar *errorstring)
{
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
				   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_CLOSE,
				   errorstring);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "delete_event",
			  G_CALLBACK(gtk_widget_destroy),
			  NULL,
			  0);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "response",
			  G_CALLBACK(gtk_widget_destroy),
			  NULL,
			  0);
  gtk_widget_show(dialog);
}

gboolean request_confirmation_dialog(gchar *confirmstring)
{
  GtkWidget *dialog;
  gint i;
  gboolean retval = FALSE;

  dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
				   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_QUESTION,
				   GTK_BUTTONS_YES_NO,
				   confirmstring);
  i = gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
  if (i == GTK_RESPONSE_YES) {
    retval = TRUE;
  }

  return retval;
}

void hexdump(unsigned char *data, guint len)
{
  guint i,j,k;

  for (i=0; i<len; i++) {
    if (i % 16 == 0 && i != 0) {
      g_print("  ");

      for (j = i-16; j < i; j++) {
	gchar ch = data[j];
	g_print("%c", ( ch >= 0x20 && ch <= 0x7e ) ? ch : '.');
      }
      g_print("\n");
    }
    g_print("%02x ", (guchar) data[i]);
  }
  k = i % 16;
  if (k != 0) {
    for (j = 0; j < k+2; j++) {
      g_print("   ");
    }
  } else {
    k = 16;
  }
  g_print("  ");
  for (j = i-k; j < i; j++) {
    gchar ch = data[j];
    g_print("%c", ( ch >= 0x20 && ch <= 0x7e ) ? ch : '.');
  }
  g_print("\n\n");
}

